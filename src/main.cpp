#include "io_lib.h"
#include "slab.h"
#include "global.h"
#include "memconf.h"
#include <string.h>
#include "task_lib.h"
#include "tasks.h"
#include "interrupt.h"
#include "timer.h"
#include "ts7200.h"

int main()
{
	// get redboot return address
	// stored in lr by default
	asm volatile ("MOV %[result], lr"
		: [result] "=r" (redboot_return_addr)
	);
	void* tmp_lr = redboot_return_addr; // redboot_return_addr will be overwritten by ctor init

	enableCache();			// increase efficiency
	// setup UARTs
	k_setfifo(COM2, 1); // enable uart 2 buffer
	k_setspeed(COM1, 2400);
	k_setfifo(COM1, 0); // disable uart1 fifo
    volatile int *uart1_flags_reg = (int *)( UART1_BASE + UART_FLAG_OFFSET );
    volatile int *uart1_data_reg = (int *)( UART1_BASE + UART_DATA_OFFSET );
    while (!(*uart1_flags_reg & RXFE_MASK)) { // clear uart 1 receive
        __attribute__((unused))unsigned char c = *uart1_data_reg;    // suppress unused warning
    }
    timerInit();
	interrupt_init();
	enable_halt();

	// memory init
	memset(&__bss_start, 0, &__bss_end - &__bss_start); // clear bss
	for (funcvoid0_t *ctr = (funcvoid0_t *)&__init_array_start; ctr < (funcvoid0_t *)&__init_array_end; ctr += 1){
		(*ctr)(); // run ctors
    }

	// reset 0x8 instruction
	volatile u32 *instr = (u32 *)0x8;
	*instr = 0xe59ff018;

	// reset 0x18 instruction
	instr = (u32 *)0x18;
	*instr = 0xe59ff018;

	// register software interruption handler
	volatile u32 *vector = (u32 *)0x28;
	*vector = (u32)svc_handler;

	// register hardware interruption handler
	vector = (u32 *)0x38;
	*vector = (u32)svc_handler; // use the same handler

	// global vars init
	// see https://piazza.com/class/l2g73ho4t8f61q?cid=44
	// need to do this here since "Starting your program does not reinitialize the data section. Loading does."
	tid_counter = 1;
	g_task_pool_size = 0;
	g_cur_task_td = nullptr;
	g_kernel_sp = nullptr;
    g_ticks = 0;
	redboot_return_addr = tmp_lr;

	// save kernel sp
	asm volatile("MOV %0, sp": "=r"(g_kernel_sp));

    // ---- deliverable startup code start ----
    // uncomment the desired parts to execute

    // -- k1 --
    // k_Create(ETP_Medium, task_first_task_k1);


	// -- k2 --
    // RPS game - uncomment this part to start the rps game
    // k_Create(ETP_Medium, task_first_task_k2);

    // measurements - need the receiver to have tid of 1

    // enable/disable cache (uncomment/comment) be careful, we have enabled cache before this line
    // enableCache();

    // measurement - receiver first - uncomment this part to start this measurement
    // assert(1 == k_Create(ETP_Highest, task_measurement_receiver));
    // k_Create(ETP_Medium, task_measurement_sender);

    // measurement - sender first - uncomment this part to start this measurement
    // assert(1 == k_Create(ETP_Medium, task_measurement_receiver));
    // k_Create(ETP_Highest, task_measurement_sender);

    // documentation for how to do other measurements
    // opt or noopt - optimization enabled or not   - add/remove the optimization compile flag in Makefile
    // cache or nocache - caches enabled or not     - uncomment the function of enableCache or not
    // R or S - receiver first or sender first      - changing the priority of the tasks or not
    // 4, 64, or 256 - message size                 - changing the values of MEASUREMENT_BUFFER_SIZE in tasks.h to different values


    // -- k3 --
    // g_idle_task_tid = k_Create(ETP_Idle, task_idle);
    // k_Create(ETP_Initializer, task_first_task_k3);


	// -- k4 --
    g_idle_task_tid = k_Create(ETP_Idle, task_idle);
    k_Create(ETP_Initializer, task_first_task_k4);

    // ---- deliverables startup code end ----

	/**
	 * TODO:
	 * Trivial/later
	 * 1. make notifier a task factory, so notifiers can be created by passing a parameter for the event to wait
	 * 2. refactor file/code structure
	 * 3. implement Destroy and use Destroy instead of Exit
	 * 		- reuse slabs
	 * 
	 * 
	 * User needs to make sure:
	 * 	train initially heading positive direction
	 */


    // start the main (kernel) loop
	k_main_loop(ESN_Interrupt, 0, true);

	bwprintf(COM2, (char *)"[Exception] Should never reach here!");
	return 0;
}
