/*
 * io_lib.cpp - I/O with mediate interrupt
 * This file needs the uart server to be created first, so it can be functional
 *
 * Specific to the TS-7200 ARM evaluation board
 *
 */

#include <io_lib.h>
#include <ts7200.h>
#include <task_lib.h>
#include <interrupt.h>
#include <global.h>
#include <string.h>

/*
 * The UARTs are initialized by RedBoot to the following state
 * 	115,200 bps
 * 	8 bits
 * 	no parity
 * 	fifos enabled
 */
int k_setfifo( int channel, int state ) {
	volatile int *line, buf;
	switch( channel ) {
	case COM1:
		line = (int *)( UART1_BASE + UART_LCRH_OFFSET );
	        break;
	case COM2:
		line = (int *)( UART2_BASE + UART_LCRH_OFFSET );
		break;
	default:
		return -1;
		break;
	}
	buf = *line;
	buf = state ? buf | FEN_MASK : buf & ~FEN_MASK;
	if (channel == COM1) buf = buf | STP2_MASK;
	*line = buf;
	return 0;
}

int k_setspeed( int channel, int speed ) {
	volatile int *high, *low, *mid;
	switch( channel ) {
	case COM1:
		// high = (int *)( UART1_BASE + UART_LCRH_OFFSET );
		mid = (int *)( UART1_BASE + UART_LCRM_OFFSET );
		low = (int *)( UART1_BASE + UART_LCRL_OFFSET );
		break;
	case COM2:
		high = (int *)( UART2_BASE + UART_LCRM_OFFSET );
		low = (int *)( UART2_BASE + UART_LCRL_OFFSET );
		break;
	default:
		return -1;
		break;
	}
	switch( speed ) {
	case 115200:
		*high = 0x0;
		*low = 0x3;
		return 0;
	case 2400:
		*mid = 0x0;
		*low = 0xc0;
		return 0;
	default:
		return -1;
	}
}


int a2d( char ch ) {
	if( ch >= '0' && ch <= '9' ) return ch - '0';
	if( ch >= 'a' && ch <= 'f' ) return ch - 'a' + 10;
	if( ch >= 'A' && ch <= 'F' ) return ch - 'A' + 10;
	return -1;
}

char a2i( char ch, char **src, int base, int *nump ) {
	int num, digit;
	char *p;

	p = *src; num = 0;
	while( ( digit = a2d( ch ) ) >= 0 ) {
		if ( digit > base ) break;
		num = num*base + digit;
		ch = *p++;
	}
	*src = p; *nump = num;
	return ch;
}

void ui2a( unsigned int num, unsigned int base, char *bf ) {
	int n = 0;
	int dgt;
	unsigned int d = 1;
	
	while( (num / d) >= base ) d *= base;
	while( d != 0 ) {
		dgt = num / d;
		num %= d;
		d /= base;
		if( n || dgt > 0 || d == 0 ) {
			*bf++ = dgt + ( dgt < 10 ? '0' : 'a' - 10 );
			++n;
		}
	}
	*bf = 0;
}

void i2a( int num, char *bf ) {
	if( num < 0 ) {
		num = -num;
		*bf++ = '-';
	}
	ui2a( num, 10, bf );
}

void putw(int n, char fc, char *bf , int *index, volatile char *str) {
	char ch;
	char *p = bf;

	while( *p++ && n > 0 ) n--;
	while( n-- > 0 ) str[(*index)++] = fc;
	while( ( ch = *bf++ ) ) str[(*index)++] = ch;
}

void format (char *fmt, va_list va, volatile char *str) {
	char bf[12];
    int index = 0;
	char ch, lz;
	int w;

	while ( ( ch = *(fmt++) ) ) {
		if ( ch != '%' )
            *(str + index++) = ch;
		else {
			lz = 0; w = 0;
			ch = *(fmt++);
			switch ( ch ) {
			case '0':
				lz = 1; ch = *(fmt++);
				break;
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				ch = a2i( ch, &fmt, 10, &w );
				break;
			}
			switch( ch ) {
			case 0: return;
			case 'c':
                *(str + index++) = va_arg( va, char );
				break;
			case 's':
				putw(w, 0, va_arg( va, char* ), &index, str);
				break;
			case 'u':
				ui2a( va_arg( va, unsigned int ), 10, bf );
				putw(w, lz, bf , &index, str);
				break;
			case 'd':
				i2a( va_arg( va, int ), bf );
				putw(w, lz, bf, &index, str);
				break;
			case 'x':
				ui2a( va_arg( va, unsigned int ), 16, bf );
				putw(w, lz, bf, &index, str);
				break;
			case '%':
                *(str + index++) = ch;
				break;
			}
		}
	}
    str[index] = '\0';
}

void printf( int channel, char *fmt, ... ) {
	volatile va_list va;
    char str[UART2_MSG_BUFFER_CAPACITY + 1];
    int tid = WhoIs(UART_SERVER_NAME);

	va_start(va,fmt);
    format(fmt, va, str);
	va_end(va);
	Putstr(tid, channel, str);
}

void va_printf( int channel, char *fmt, va_list args){
    char str[UART2_MSG_BUFFER_CAPACITY + 1];
    int tid = WhoIs(UART_SERVER_NAME);

    format(fmt, args, str);
    Putstr(tid, channel, str, strlen(str));
}



// This is the other implementation, which uses Putc only, does not support multiple user tasks

void cputw(int tid, int channel, int n, char fc, char *bf , int *index) {
	char ch;
	char *p = bf;

	while( *p++ && n > 0 ) n--;
	while( n-- > 0 ) Putc(tid, channel, fc);
	while( ( ch = *bf++ ) ) Putc(tid, channel, ch);
}

void cformat (int tid, int channel, char *fmt, va_list va) {
	char bf[12];
    int index = 0;
	char ch, lz;
	int w;

	while ( ( ch = *(fmt++) ) ) {
		if ( ch != '%' )
            Putc(tid, channel, ch);
		else {
			lz = 0; w = 0;
			ch = *(fmt++);
			switch ( ch ) {
			case '0':
				lz = 1; ch = *(fmt++);
				break;
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				ch = a2i( ch, &fmt, 10, &w );
				break;
			}
			switch( ch ) {
			case 0: return;
			case 'c':
                Putc(tid, channel, va_arg( va, char ));
				break;
			case 's':
				cputw(tid, channel, w, 0, va_arg( va, char* ), &index);
				break;
			case 'u':
				ui2a( va_arg( va, unsigned int ), 10, bf );
				cputw(tid, channel, w, lz, bf , &index);
				break;
			case 'd':
				i2a( va_arg( va, int ), bf );
				cputw(tid, channel, w, lz, bf, &index);
				break;
			case 'x':
				ui2a( va_arg( va, unsigned int ), 16, bf );
				cputw(tid, channel, w, lz, bf, &index);
				break;
			case '%':
                Putc(tid, channel, ch);
				break;
			}
		}
	}
}

void cprintf( int uart_tid, int channel, char *fmt, ... ){
	volatile va_list va;

	va_start(va, fmt);
    cformat(uart_tid, channel, fmt, va);
	va_end(va);
}

void cprintf( int channel, char *fmt, ... ){
	volatile va_list va;
    int tid = WhoIs(UART_SERVER_NAME);

	va_start(va, fmt);
    cformat(tid, channel, fmt, va);
	va_end(va);
}

void va_cprintf( int channel, char *fmt, va_list args){
    int tid = WhoIs(UART_SERVER_NAME);
    cformat(tid, channel, fmt, args);
}
