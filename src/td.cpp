#include "td.h"

TD::TD(int tid, TD* parent, int priority, u32* sp) : 
    state{ES_Ready}, tid{tid}, parent{parent}, priority(priority), sp(sp) {
    next = nullptr;
}
