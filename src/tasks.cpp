#include "io_lib.h"
#include "task_lib.h"
#include <string.h>
#include "global.h"
#include "tasks.h"
#include "ts7200.h"
#include "timer.h"
#include "interrupt.h"
#include "trainControl.h"

// ---- user defined tasks ----

// -- test tasks --
// test:
// k_Create(ETP_High, test_task_cs);
// test for context switching, making sure variables do not change values
void test_task_cs(){
    // creates the idle task
    g_idle_task_tid = Create(ETP_Idle, task_idle);

    // creates the name server
    name_server_tid = Create(ETP_Highest, task_name_server);

    // creates the clock server
    Create(ETP_Highest, task_clock_server);     // tid: 4

    int a = -1, b = 0, c = 10;
    char d = 'e';
    MyTid();
    if ( -1 == a && 0 == b && 10 == c && 'e' == d){
        bwprintf(COM2, (char*) "Mytid test Passed!. \n\r");
    }else{
        bwprintf(COM2, (char*) "Mytid test failed: a: %d vs -1, b: %d vs. 0, c: %d vs. 10, d: %c vs. e.\n\r", a,b,c,d);
    }

    a = -1, b = 0, c = 10;
    d = 'e';
    MyParentTid();
    if ( -1 == a && 0 == b && 10 == c && 'e' == d){
        bwprintf(COM2, (char*) "MyParentTid test Passed!. \n\r");
    }else{
        bwprintf(COM2, (char*) "MyParentTid test failed: a: %d vs -1, b: %d vs. 0, c: %d vs. 10, d: %c vs. e.\n\r", a,b,c,d);
    }

    a = -1, b = 0, c = 10;
    d = 'e';
    Yield();
    if ( -1 == a && 0 == b && 10 == c && 'e' == d){
        bwprintf(COM2, (char*) "Yield test Passed!. \n\r");
    }else{
        bwprintf(COM2, (char*) "Yield test failed: a: %d vs -1, b: %d vs. 0, c: %d vs. 10, d: %c vs. e.\n\r", a,b,c,d);
    }

    a = -1, b = 0, c = 10;
    d = 'e';
    ReadTimerUS();
    if ( -1 == a && 0 == b && 10 == c && 'e' == d){
        bwprintf(COM2, (char*) "ReadTimerUS test Passed!. \n\r");
    }else{
        bwprintf(COM2, (char*) "ReadTimerUS test failed: a: %d vs -1, b: %d vs. 0, c: %d vs. 10, d: %c vs. e.\n\r", a,b,c,d);
    }

    a = -1, b = 0, c = 10;
    d = 'e';
    rand(1,2);
    if ( -1 == a && 0 == b && 10 == c && 'e' == d){
        bwprintf(COM2, (char*) "rand test Passed!. \n\r");
    }else{
        bwprintf(COM2, (char*) "rand test failed: a: %d vs -1, b: %d vs. 0, c: %d vs. 10, d: %c vs. e.\n\r", a,b,c,d);
    }

    a = -1, b = 0, c = 10;
    d = 'e';
    ReadTicks();
    if ( -1 == a && 0 == b && 10 == c && 'e' == d){
        bwprintf(COM2, (char*) "ReadTicks test Passed!. \n\r");
    }else{
        bwprintf(COM2, (char*) "ReadTicks test failed: a: %d vs -1, b: %d vs. 0, c: %d vs. 10, d: %c vs. e.\n\r", a,b,c,d);
    }

    a = -1, b = 0, c = 10;
    d = 'e';
    int volatile clock_tid = WhoIs(CLOCK_SERVER_NAME);
    assert(clock_tid == 4);
    int tid = clock_tid;
    int *id = &tid;
    Time(*id);
    if ( -1 == a && 0 == b && 10 == c && 'e' == d && clock_tid == 4){
        bwprintf(COM2, (char*) "Time test Passed!. \n\r");
    }else{
        bwprintf(COM2, (char*) "Time test failed: a: %d vs -1, b: %d vs. 0, c: %d vs. 10, d: %c vs. e, clock: %d vs. 4.\n\r", a,b,c,d,clock_tid);
    }

    a = -1, b = 0, c = 10;
    d = 'e';
    clock_tid = WhoIs(CLOCK_SERVER_NAME);
    assert(clock_tid == 4);
    int time = Delay(clock_tid, 100);
    if ( -1 == a && 0 == b && 10 == c && 'e' == d && clock_tid == 4){
        bwprintf(COM2, (char*) "Delay test Passed!. \n\r");
    }else{
        bwprintf(COM2, (char*) "Delay test failed: a: %d vs -1, b: %d vs. 0, c: %d vs. 10, d: %c vs. e, clock: %d vs. 4.\n\r", a,b,c,d,clock_tid);
    }

    a = -1, b = 0, c = 10;
    d = 'e';
    clock_tid = WhoIs(CLOCK_SERVER_NAME);
    assert(clock_tid == 4);
    DelayUntil(clock_tid, time + 100);
    if ( -1 == a && 0 == b && 10 == c && 'e' == d && clock_tid == 4){
        bwprintf(COM2, (char*) "DelayUntil test Passed!. \n\r");
    }else{
        bwprintf(COM2, (char*) "DelayUntil test failed: a: %d vs -1, b: %d vs. 0, c: %d vs. 10, d: %c vs. e, clock: %d vs. 4.\n\r", a,b,c,d,clock_tid);
    }

}

/* 
test receiver first, in main:
 	k_Create(ETP_Medium, &test_receiver_1);
	k_Create(ETP_Medium, &test_sender_1);
*/
void test_receiver_1() {
	int sender_tid;
	char msg_buffer[11];
	// receiver first
    bwprintf(COM2, (char*)"t1 receive\r\n");
	int len = Receive(&sender_tid, msg_buffer, 10);
	msg_buffer[len] = '\0';
	bwprintf(COM2, (char*)"t1 Message received from %d:", sender_tid);
	bwprintf(COM2, msg_buffer);
	bwprintf(COM2, (char*)"\r\n");

	strncpy(msg_buffer, (char*)"hello", 10);
	bwprintf(COM2, (char*)"t1 Send reply:");
	bwprintf(COM2, msg_buffer);
	bwprintf(COM2, (char*)"\r\n");

	len = Reply(sender_tid, msg_buffer, 5);
}

void test_sender_1() {
	// sender second
	char msg[] = "12345";
	char reply[10];
	bwprintf(COM2, (char*)"t2 Send msg:");
	bwprintf(COM2, msg);
	bwprintf(COM2, (char*)"\r\n");
	int len = Send(1, msg, 5, reply, 10);

	bwprintf(COM2, (char*)"t2 Sender unblocked\r\n");

	bwprintf(COM2, (char*)"t2 Reply received, len = %d:", len);
	reply[len] = '\0';
	bwprintf(COM2, reply);
	bwprintf(COM2, (char*)"\r\n");

}


/* 
test sender first, in main:
 	k_Create(ETP_Medium, &test_sender_2);
	k_Create(ETP_Medium, &test_receiver_2);
*/
void test_sender_2() {
	char msg[] = "12345";
	char reply[11];
	// sender first
	bwprintf(COM2, (char*)"t1 Sender send 12345\r\n");
	int len = Send(2, msg, 5, reply, 10);
	bwprintf(COM2, (char*)"t1 Sender unblocked\r\n");


	bwprintf(COM2, (char*)"t2 Reply received, len = %d:", len);
	reply[len] = '\0';
	bwprintf(COM2, reply);
	bwprintf(COM2, (char*)"\r\n");
}

void test_receiver_2() {
	int sender_tid;
	char msg_buffer[11];
	// receiver second
	bwprintf(COM2, (char*)"t2 Receiver receive\r\n");
	int len = Receive(&sender_tid, msg_buffer, 10);

	msg_buffer[len] = '\0';
	bwprintf(COM2, (char*)"t2 Message received:");
	bwprintf(COM2, msg_buffer);
	bwprintf(COM2, (char*)"\r\n");

	strncpy(msg_buffer, (char*)"hellohello", 11);
	bwprintf(COM2, (char*)"t2 Send reply:");
	bwprintf(COM2, msg_buffer);
	bwprintf(COM2, (char*)"\r\n");

	len = Reply(sender_tid, msg_buffer, 10);
	bwprintf(COM2, (char*)"t2 reply sent length %d\r\n", len);
}

/* 
test sender send to non-existent tid, should return -1, in main:
 	k_Create(ETP_Medium, &test_sender_3);
*/
void test_sender_3() {
	char msg[] = "12345";
	char reply[10];
	// sender first
	bwprintf(COM2, (char*)"t1 Sender send 12345\r\n");
	int result = Send(2, msg, 5, reply, 10);
	bwprintf(COM2, (char*)"t1 Sender result %d\r\n", result);
}

/* 
test msg truncated, in main:
 	k_Create(ETP_Medium, &test_sender_truncated);
	k_Create(ETP_Medium, &test_receiver_truncated);
*/
void test_sender_truncated() {
	char msg[] = "12345";
	char reply[3];
	// sender first
	bwprintf(COM2, (char*)"t1 Sender send 12345\r\n");
	int len = Send(2, msg, 5, reply, 3);
	bwprintf(COM2, (char*)"t1 Sender unblocked\r\n");


	bwprintf(COM2, (char*)"t2 Reply received, reply buffer size is 3, replylen = %d:", len);
	reply[2] = '\0';
	bwprintf(COM2, reply);
	bwprintf(COM2, (char*)"\r\n");
}

void test_receiver_truncated() {
	int sender_tid;
	char msg_buffer[4];
	// receiver second
	bwprintf(COM2, (char*)"t2 Receiver receive\r\n");
	int len = Receive(&sender_tid, msg_buffer, 3);

	msg_buffer[2] = '\0';
	bwprintf(COM2, (char*)"t2 Message received, sender %d, buffer size is 3, msg len is %d:", sender_tid, len);
	bwprintf(COM2, msg_buffer);
	bwprintf(COM2, (char*)"\r\n");

    char reply_buffer[10];
	strncpy(reply_buffer, (char*)"hihi", 10);
	bwprintf(COM2, (char*)"t2 Send reply:");
	bwprintf(COM2, reply_buffer);
	bwprintf(COM2, (char*)"\r\n");

	len = Reply(sender_tid, reply_buffer, 4);
	bwprintf(COM2, (char*)"t2 reply sent length %d\r\n", len);
}

/*
Test reply exceptional values
*/
void test_reply_non_existing_task() {
    char reply_buffer[10];
	int len = Reply(155, reply_buffer, 10);  // expect return -1
	bwprintf(COM2, (char*)"t1 reply return: %d\r\n", len);
}

void test_reply_non_blocked_task() {
    char reply_buffer[10];
	int len = Reply(1, reply_buffer, 10); // expect return -2
	bwprintf(COM2, (char*)"t2 reply return: %d\r\n", len);
}

/*
test multiple senders, in main:
	k_Create(ETP_Medium, test_multiple_sender_1);
	k_Create(ETP_Medium, test_multiple_sender_2);
	k_Create(ETP_Medium, test_multiple_sender_receiver);
*/
void test_multiple_sender_1() {
	char msg[] = "12345";
	char reply[11];
	// sender first
	bwprintf(COM2, (char*)"t1 Sender send 12345\r\n");
	int len = Send(3, msg, 5, reply, 10);
	bwprintf(COM2, (char*)"t1 Sender unblocked\r\n");


	bwprintf(COM2, (char*)"t1 Reply received, len = %d:", len);
	reply[len] = '\0';
	bwprintf(COM2, reply);
	bwprintf(COM2, (char*)"\r\n");
}

void test_multiple_sender_2() {
	char msg[] = "67890";
	char reply[10];
	// sender first
	bwprintf(COM2, (char*)"t2 Sender send 12345\r\n");
	int len = Send(3, msg, 5, reply, 10);
	bwprintf(COM2, (char*)"t2 Sender unblocked\r\n");


	bwprintf(COM2, (char*)"t2 Reply received, len = %d:", len);
	reply[len] = '\0';
	bwprintf(COM2, reply);
	bwprintf(COM2, (char*)"\r\n");
}

void test_multiple_sender_receiver() {
	int sender_tid;
	char msg_buffer[11];
	// receive 1
	bwprintf(COM2, (char*)"t3 Receiver receive\r\n");
	int len = Receive(&sender_tid, msg_buffer, 10);
	bwprintf(COM2, (char*)"t3 Receiver receive length %d\r\n", len);

	msg_buffer[len] = '\0';
	bwprintf(COM2, (char*)"t3 Message received:");
	bwprintf(COM2, msg_buffer);
	bwprintf(COM2, (char*)"\r\n");

	strncpy(msg_buffer, (char*)"hello 1", 10);
	bwprintf(COM2, (char*)"t3 Send reply:");
	bwprintf(COM2, msg_buffer);
	bwprintf(COM2, (char*)"\r\n");

	len = Reply(sender_tid, msg_buffer, 7);
	bwprintf(COM2, (char*)"t3 reply sent length %d\r\n", len);

	// receive 2
	bwprintf(COM2, (char*)"t3 Receiver receive\r\n");
	len = Receive(&sender_tid, msg_buffer, 10);
	bwprintf(COM2, (char*)"t3 Receiver receive length %d\r\n", len);

	msg_buffer[len] = '\0';
	bwprintf(COM2, (char*)"t3 Message received:");
	bwprintf(COM2, msg_buffer);
	bwprintf(COM2, (char*)"\r\n");

	strncpy(msg_buffer, (char*)"hello 2", 10);
	bwprintf(COM2, (char*)"t3 Send reply:");
	bwprintf(COM2, msg_buffer);
	bwprintf(COM2, (char*)"\r\n");

	len = Reply(sender_tid, msg_buffer, 7);
	bwprintf(COM2, (char*)"t3 reply sent length %d\r\n", len);
}

// --- nameserver testing stuff
// k_Create(ETP_High, test_register_name);
// k_Create(ETP_Medium, test_who_is);
void test_register_name() {
    name_server_tid = Create(ETP_Highest, task_name_server);    // tid: 2
	char my_name[] = "Simon";
	bwprintf(COM2, (char*) "Registering as Simon\r\n");
	int result =  RegisterAs(my_name);
	bwprintf(COM2, (char*) "Registering result %d\r\n", result);
}

void test_who_is() {
	char query[] = "Simon";
	bwprintf(COM2, (char*) "Whois Simon\r\n");
	int result =  WhoIs(query);
	bwprintf(COM2, (char*) "Whois return %d\r\n", result);
}


// test:
// k_Create(ETP_Medium, test_receiver_do_not_receive);
// k_Create(ETP_High, test_sender_do_not_receive);

// receiver did not receive
void test_receiver_do_not_receive() {
	// receiver second
    bwprintf(COM2, (char*)"Receiver: start\r\n");
	bwprintf(COM2, (char*)"Receiver does not wait for sender and exited.\n\r");
	bwprintf(COM2, (char*)"Receiver exited gracefully - (1/2) Good.\n\r");
}

void test_sender_do_not_receive() {
	// sender first
	char msg[] = "12345";
	char reply[10];
	bwprintf(COM2, (char*)"Sender: Send msg: %s.\n\r", msg);
	int len = Send(1, msg, 5, reply, 9);        // the size is always the actual size - 1
	if(-2 != len){
		bwprintf(COM2, (char*)"Exception!\r\n");
	}else{
		bwprintf(COM2, (char*)"Sender: Sender unblocked\r\n");
		bwprintf(COM2, (char*)"Sender: Return value:%d, expecting -2.\r\n", len);
		bwprintf(COM2, (char*)"Sender exited gracefully - (2/2) Good.\n\r");
	}
}


// test:
// k_Create(ETP_High, test_receiver_receiver_quitted);
// k_Create(ETP_Medium, test_sender_receiver_quitted);

// receiver quitted before send
void test_receiver_receiver_quitted() {
	// receiver first
    bwprintf(COM2, (char*)"Receiver: start\r\n");
	bwprintf(COM2, (char*)"Receiver exited before sender sends.\n\r");
	bwprintf(COM2, (char*)"Receiver exited gracefully - (1/2) Good.\n\r");
}

void test_sender_receiver_quitted() {
	// sender second
	char msg[] = "12345";
	char reply[10];
	bwprintf(COM2, (char*)"Sender: Send msg: %s.\n\r", msg);
	int len = Send(1, msg, 5, reply, 9);
	if(-1 != len){
		bwprintf(COM2, (char*)"Exception!\r\n");
	}else{
		bwprintf(COM2, (char*)"Sender: Sender unblocked\r\n");
		bwprintf(COM2, (char*)"Sender: Return value:%d, expecting -1\r\n", len);
		bwprintf(COM2, (char*)"Sender exited gracefully - (2/2) Good.\n\r");
	}
}

// Only used for testings, not for k1
void task_test_1()
{
    int a = 1;
    int b = 2;

    bwprintf(COM2, (char *)"Task 1 printing 1\n\r");

    Yield();

    int c = 3;
    bwprintf(COM2, (char *)"Task 1 printing 2\n\r");
    bwprintf(COM2, (char *)"Task 1, a: %u\n\r", a);
    bwprintf(COM2, (char *)"Task 1, b: %u\n\r", b);
    bwprintf(COM2, (char *)"Task 1 is Task %u\n\r", MyTid());

    Yield();

    bwprintf(COM2, (char *)"Task 1 printing 3\n\r");
    bwprintf(COM2, (char *)"Task 1, a: %u\n\r", a);
    bwprintf(COM2, (char *)"Task 1, b: %u\n\r", b);
    bwprintf(COM2, (char *)"Task 1, c: %u\n\r", c);
    bwprintf(COM2, (char *)"Task 1 is Task %u\n\r", MyTid());

    Exit();
}

void task_test_2()
{
    int c = 4;
    int d = 5;

    bwprintf(COM2, (char *)"Task 2 printing 1\n\r");

    Yield();

    int e = 6;
    bwprintf(COM2, (char *)"Task 2 printing 2\n\r");
    bwprintf(COM2, (char *)"Task 2, c: %u\n\r", c);
    bwprintf(COM2, (char *)"Task 2, d: %u\n\r", d);
    bwprintf(COM2, (char *)"Task 2 is Task %u\n\r", MyTid());

    Yield();

    bwprintf(COM2, (char *)"Task 2 printing 3\n\r");
    bwprintf(COM2, (char *)"Task 2, a: %u\n\r", c);
    bwprintf(COM2, (char *)"Task 2, b: %u\n\r", d);
    bwprintf(COM2, (char *)"Task 2, c: %u\n\r", e);
    bwprintf(COM2, (char *)"Task 2 is Task %u\n\r", MyTid());

    Exit();
}

// test:
// k_Create(ETP_Medium, test_await_event);
void test_await_event() {
    g_idle_task_tid = Create(ETP_Idle, task_idle);

    unsigned int start_time;
    unsigned int end_time;
    int nloops = 100;
    int tid = MyTid();
    for (int i = 0; i < 10; i++) {
        start_time  = ReadTimerUS();
        AwaitEvent(Event::EVT_TIMER_TICK);
        end_time  = ReadTimerUS();
        bwprintf(COM2, (char*)"%d %d %d\r\n", tid, i, end_time-start_time);
    }

    start_time  = ReadTimerUS();
    for (int i = 0; i < nloops; i++) {
        AwaitEvent(Event::EVT_TIMER_TICK);
    }
    end_time  = ReadTimerUS();
    bwprintf(COM2, (char*)"%d us, expecting 10000 us\r\n", (end_time-start_time)/nloops);

    start_time  = ReadTimer();
    for (int i = 0; i < nloops; i++) {
        AwaitEvent(Event::EVT_TIMER_TICK);
    }
    end_time  = ReadTimer();
    bwprintf(COM2, (char*)"%d ms, expecting 10 ms\r\n", (end_time-start_time)/508/nloops);

    int result = AwaitEvent(123);
    bwprintf(COM2, (char*)"%d invalid event returns: %d, expecting -1\r\n", tid, result);


    bwprintf(COM2, (char*)"%d waitting for UART 2 timeout\r\n", tid);
    AwaitEvent(Event::EVT_UART2_TIMEOUT);
    bwprintf(COM2, (char*)"%d got UART 2 timeout\r\n", tid);

    // volatile int* reg = (volatile int *)(VIC2_BASE + VIC_SOFT_INT_OFFSET);
	// *reg = INT_UART2_MASK;
}

// test:
// k_Create(ETP_Highest, test_timer_and_Ticks);
void test_timer_and_Ticks(){
    unsigned int usStart, usEnd, tkStart, tkEnd;
    usStart = ReadTimerUS();
    tkStart = ReadTicks();     // 1 tick is 10ms
    while (true){
        usEnd = ReadTimerUS();
        if(2000000 <=  usEnd - usStart){
            tkEnd = ReadTicks();
            break;
        }
    }
    bwprintf(COM2, (char*)"US diff: %d, tick diff: %d. Expecting around 2000000 and 200.\n\r", usEnd - usStart, tkEnd - tkStart);
}

// test:
// k_Create(ETP_High, test_task_time);
void test_task_time(){
    // creates the idle task
    g_idle_task_tid = Create(ETP_Idle, task_idle);

    // creates the name server
    name_server_tid = Create(ETP_Highest, task_name_server);

    // creates the clock server
    Create(ETP_Highest, task_clock_server);     // tid: 4

    int volatile n = 0;
    // find the clock server
    int volatile clock_tid = WhoIs(CLOCK_SERVER_NAME);

    // testings

    // each tick should be accurate
    int const tickStart = Time(clock_tid);
    while(n < 101){
        AwaitEvent(Event::EVT_TIMER_TICK);
        n++;
        bwprintf(COM2, (char*)"T: %d, expecting: %d\n\r", Time(clock_tid) - tickStart, n);
    }
}

// test:
// k_Create(ETP_High, test_task_delayUntil);
void test_task_delayUntil(){
    // creates the idle task
    g_idle_task_tid = Create(ETP_Idle, task_idle);

    // creates the name server
    name_server_tid = Create(ETP_Highest, task_name_server);

    // creates the clock server
    Create(ETP_Highest, task_clock_server);     // tid: 4

    int res, tickStart, delay;

    // find the clock server
    int volatile clock_tid = WhoIs(CLOCK_SERVER_NAME);

    // testings

    // successful delay
    delay = 123;
    tickStart = Time(clock_tid);
    res = DelayUntil(clock_tid, tickStart + delay);
    assert( res != -1 );
    assert( res != -2 );
    bwprintf(COM2, (char*)"Test 1.1 - Waited for %d ticks. Expecting around %d.\n\r", res - tickStart, delay);
    
    tickStart = Time(clock_tid);
    res = DelayUntil(clock_tid, tickStart);
    assert( res != -1 );
    assert( res != -2 );
    bwprintf(COM2, (char*)"Test 1.2 - Waited for %d ticks. Expecting around %d.\n\r", res - tickStart, 0);
    
    // invalid target tid
    res = DelayUntil(-1, Time(clock_tid) + delay);
    if( res == -1){
        bwprintf(COM2, (char*)"Test 2 Passed!.\n\r");
    }else{
        bwprintf(COM2, (char*)"Test 2 failed!\n\r");
    }
    
    // target tid is not the clock server
    res = DelayUntil(3, Time(clock_tid) + delay);
    if( res == -1){
        bwprintf(COM2, (char*)"Test 3 Passed!.\n\r");
    }else{
        bwprintf(COM2, (char*)"Test 3 failed!\n\r");
    }
    
    // negative delays
    tickStart = Time(clock_tid);
    res = DelayUntil(clock_tid, tickStart - 1);
    if( res == -2){
        bwprintf(COM2, (char*)"Test 4 Passed!.\n\r");
    }else{
        bwprintf(COM2, (char*)"Test 4 failed! res: %d, expecting: -2\n\r", res);
    }
}

// k_Create(ETP_High, test_task_delay);
void test_task_delay(){
    // creates the idle task
    g_idle_task_tid = Create(ETP_Idle, task_idle);

    // creates the name server
    name_server_tid = Create(ETP_Highest, task_name_server);

    // creates the clock server
    Create(ETP_Highest, task_clock_server);     // tid: 4

    int res, tickStart, delay;

    // find the clock server
    int volatile clock_tid = WhoIs(CLOCK_SERVER_NAME);

    // testings

    // successful delay
    delay = 123;
    tickStart = Time(clock_tid);
    res = Delay(clock_tid, delay);
    assert( res != -1 );
    assert( res != -2 );
    bwprintf(COM2, (char*)"Test 1.1 - Waited for %d ticks. Expecting around %d.\n\r", res - tickStart, delay);
    
    delay = 0;
    tickStart = Time(clock_tid);
    res = Delay(clock_tid, delay);
    assert( res != -1 );
    assert( res != -2 );
    bwprintf(COM2, (char*)"Test 1.2 - Waited for %d ticks. Expecting around %d.\n\r", res - tickStart, delay);
    
    // invalid target tid
    res = Delay(-1, Time(clock_tid) + delay);
    if( res == -1){
        bwprintf(COM2, (char*)"Test 2 Passed!\n\r");
    }else{
        bwprintf(COM2, (char*)"Test 2 failed!\n\r");
    }
    
    // target tid is not the clock server
    res = Delay(3, Time(clock_tid) + delay);
    if( res == -1){
        bwprintf(COM2, (char*)"Test 3 Passed!\n\r");
    }else{
        bwprintf(COM2, (char*)"Test 3 failed!\n\r");
    }
    
    // negative delays
    tickStart = Time(clock_tid);
    res = Delay(clock_tid, -1);
    if( res == -2){
        bwprintf(COM2, (char*)"Test 4 Passed!\n\r");
    }else{
        bwprintf(COM2, (char*)"Test 4 failed! res: %d, expecting: -2\n\r", res);
    }
}

void test_halt() {
    bwprintf(COM2, (char*)"halting now...\r\n");
    halt();
    bwprintf(COM2, (char*)"after interrupt\r\n");
}

void test_get_putc() { // print whatever is typed in
    int server_tid = WhoIs(UART_SERVER_NAME);
    // test uart1 get c

    Putc(server_tid, COM1, 133);
    bwprintf(COM2, (char*)"received sensor query result ");  
    for (int i = 0; i < 10; i++) {
        char first_char = Getc(server_tid, COM1);
        bwprintf(COM2, (char*)"%u ", first_char);  
    }
    bwprintf(COM2, (char*)"\r\n");  

    for (int i = 0; i < 4; i++) { // test uart 2 burst transmit
        for (int j = 0; j < 18; j++) {
            Putc(server_tid, COM2, 'c');
        }
        Putc(server_tid, COM2, '\n');
    }
    while (true) {
        char c = Getc(server_tid, COM2);
        Putc(server_tid, COM2, c);
        if (c == 'g') { // go
            c = 96;
            Putc(server_tid, COM1, c);
        } else if (c == 's') { // stop
            c = 97;
            Putc(server_tid, COM1, c);
        } else if (c == '0') { // stop trains
            c = 0;
            Putc(server_tid, COM1, c);
            c = 1;
            Putc(server_tid, COM1, c);
        } else if (c == '1') { // start train
            c = 10;
            Putc(server_tid, COM1, c);
            c = 1;
            Putc(server_tid, COM1, c);
        } else if (c == '2') { // test uart 1 burst transmit
            c = 10;
            Putc(server_tid, COM1, c);
            c = 1;
            Putc(server_tid, COM1, c);
            c = 10;
            Putc(server_tid, COM1, c);
            c = 78;
            Putc(server_tid, COM1, c);
        } else if (c == '3') { // test uart 1 burst transmit
            c = 0;
            Putc(server_tid, COM1, c);
            c = 1;
            Putc(server_tid, COM1, c);
            c = 0;
            Putc(server_tid, COM1, c);
            c = 78;
            Putc(server_tid, COM1, c);   
        }
    }
}

// k_Create(ETP_High, test_task_printf_u);
void test_task_printf_u(){
    name_server_tid = Create(ETP_Highest, task_name_server);
    Create(ETP_Highest, task_clock_server);
    Create(ETP_Highest, task_uart_server);
    g_idle_task_tid = Create(ETP_Idle, task_idle);

    printf(COM2, (char*)"Test tid: %d, print testing - (1/10)\n\r", 1);
    printf(COM2, (char*)"Test tid: %d, print testing - (2/10)\n\r", 1);
    printf(COM2, (char*)"Test tid: %d, print testing - (3/10)\n\r", 1);
    printf(COM2, (char*)"Test tid: %d, print testing - (4/10)\n\r", 1);
    printf(COM2, (char*)"Test tid: %d, print testing - (5/10)\n\r", 1);
    printf(COM2, (char*)"Test tid: %d, print testing - (6/10)\n\r", 1);
    printf(COM2, (char*)"Test tid: %d, print testing - (7/10)\n\r", 1);
    printf(COM2, (char*)"Test tid: %d, print testing - (8/10)\n\r", 1);
    printf(COM2, (char*)"Test tid: %d, print testing - (9/10)\n\r", 1);
    printf(COM2, (char*)"Test tid: %d, print testing - (10/10)\n\r", 1);
}

// k_Create(ETP_High, test_task_printf_m);
void test_task_printf_2(){
    printf(COM2, (char*)"Test tid: %d, print testing - (2/9)\n\r", 2);
    printf(COM2, (char*)"Test tid: %d, print testing - (4/9)\n\r", 2);
    printf(COM2, (char*)"Test tid: %d, print testing - (8/9)\n\r", 2);
}
void test_task_printf_3(){
    printf(COM2, (char*)"Test tid: %d, print testing - (5/9)\n\r", 3);
    printf(COM2, (char*)"Test tid: %d, print testing - (7/9)\n\r", 3);
    printf(COM2, (char*)"Test tid: %d, print testing - (9/9)\n\r", 3);
}
void test_task_printf_m(){
    name_server_tid = Create(ETP_Highest, task_name_server);
    Create(ETP_Highest, task_clock_server);
    Create(ETP_Highest, task_uart_server);
    g_idle_task_tid = Create(ETP_Idle, task_idle);

    int u_id = WhoIs(UART_SERVER_NAME);

    char str[] = "12345678-aByZ - good!\r\n";

    Putstr(u_id ,COM2, str, strlen(str));

    printf(COM2, (char*)"Test tid: %d, print testing - (1/9)\n\r", 1);
    Create(ETP_High, test_task_printf_2);
    printf(COM2, (char*)"Test tid: %d, print testing - (3/9)\n\r", 1);
    Create(ETP_High, test_task_printf_3);
    printf(COM2, (char*)"Test tid: %d, print testing - (6/9)\n\r", 1);
}



// special task
void task_idle(){
    while(true){
        // Ideally, the idle task should put the CPU into a low-power halt state. 
        halt();
    }
}

// ------ user tasks required in kernel assignments ------
/**
 * @brief This part includes all the tasks required by kernel assignments. 
 * They should appear in a specified block in `main.cpp`. The user can just
 * uncomment the desired part to execute and test. Note that only one test
 * can be executed at the same time. I.e., only uncomment the code for one
 * assignment at a time.
 */


// ---- k1 ----

void task_other_k1()
{
    bwprintf(COM2, (char *)"Task id: %d, Parent tid: %d\n\r", MyTid(), MyParentTid());
    Yield();
    bwprintf(COM2, (char *)"Task id: %d, Parent tid: %d\n\r", MyTid(), MyParentTid());
}

void task_first_task_k1()
{
#ifdef DEBUG_TASK_AE
    bwprintf(COM2, (char *)"[Debug] First task is started\n\r");
#endif
    // Send(2, (char*) 3, 4, (char*) 5, 6);
    // create 2 tasks with lower priorities
    bwprintf(COM2, (char *)"Created: %d\n\r", Create(ETP_Low, &task_other_k1));
    bwprintf(COM2, (char *)"Created: %d\n\r", Create(ETP_Low, &task_other_k1));

    // create 2 tasks with higher priorities
    bwprintf(COM2, (char *)"Created: %d\n\r", Create(ETP_High, &task_other_k1));
    bwprintf(COM2, (char *)"Created: %d\n\r", Create(ETP_High, &task_other_k1));

    bwprintf(COM2, (char *)"FirstUserTask: exiting\n\r");
    // Exit
}


// ---- k2 ----

// helper functions for RPSServer
// turn Rock, paper, scissor to int, Rock:1, Paper:2 , Scissor:3 
int RPS2Int(char rps){
    if('R' == rps){
        return 1;
    }else if('P' == rps){
        return 2;
    }else if('S' == rps){
        return 3;
    }
    bwprintf(COM2, (char*)"Invalid input for RPS2Int.\n\r");
    return 0;
}
char* int2RPS(int rps){
    if(1 == rps){
        return (char*)"Rock";
    }else if(2 == rps){
         return (char*)"Paper";
    }else if(3 == rps){
         return (char*)"Scissor";
    }
    bwprintf(COM2, (char*)"Invalid input for int2RPS.\n\r");
    return (char*)"0";
}

// get the result of RPS for this round
// returns -1: lose, 0: tie, 1: win
int resRPS(const int res){
    if(1 == res || -2 == res){
        return 1;
    }else if (0 == res){
        return 0;
    }
    return -1;
}

// RPS Server
void RPSServer(){
    int response = 1;
    while(response){            // make sure register is successful
        response = RegisterAs("RPSServer");
    }

    int opponents[110] = {0};   // opponents (assume at most 110 clients)
    int choices[110] = {0};     // player choices, Rock:1, Paper:2 , Scissor:3 

    int sender_tid = 0, opponent_tid, prev_tid = 0, choice, res;
	char receive_buf[10];

    // Server should never quit
    while (true){
        Receive(&sender_tid, receive_buf, 10);

        if(0 == strcmp(receive_buf, "Sign")){    // sign up
            #ifdef DEBUG_TASK_AE
            bwprintf(COM2, "[Debug] Sign \n\r");
            #endif

            if(0 != prev_tid){                  // there is another player, pair up
                // record opponents
                opponents[sender_tid] = prev_tid;
                opponents[prev_tid] = sender_tid;
            
                #ifdef DEBUG_TASK_AE
                bwprintf(COM2, "[Debug] Pair up: %d + %d\n\r", prev_tid, sender_tid);
                #endif
            
                // reply - ask players to play
                Reply(sender_tid, "Play", 4);
                Reply(prev_tid, "Play", 4);

                // reset
                prev_tid = 0;
            }else{
                prev_tid = sender_tid;
            }
        }else if(0 == strncmp(receive_buf, "Play", 4)){     // play
            #ifdef DEBUG_TASK_AE
            bwprintf(COM2, "[Debug] Play \n\r");
            #endif

            if(0 == opponents[sender_tid]){                 // opponent quitted
                Reply(sender_tid, "Other Player Quit", 17);
            }else{
                // get the choice
                choice = RPS2Int(receive_buf[4]);
                // check if opponent has already played
                opponent_tid = opponents[sender_tid];
                if(0 != choices[opponent_tid]){             // end this round

                    // round ended, pause
                    bwprintf(COM2, (char*)"\n\r[Press any key to continue]");
                    bwgetc(COM2);
                    bwprintf(COM2, (char*)"\x1b[1K\r");

                    res = resRPS(choice - choices[opponent_tid]);
                    if(1 == res){
                        Reply(sender_tid, "Win", 3);
                        Reply(opponent_tid, "Lose", 4);  

                        // stdoutput the information
                        bwprintf(COM2, (char*)"Client %d played %s. Reply: %s \n\r",sender_tid, int2RPS(choice), "Win");
                        bwprintf(COM2, (char*)"Client %d played %s. Reply: %s \n\r",opponent_tid, int2RPS(choices[opponent_tid]), "Lose");
                    }else if(0 == res){
                        Reply(sender_tid, "Tie", 3);
                        Reply(opponent_tid, "Tie", 3);  
                        
                        // stdoutput the information
                        bwprintf(COM2, (char*)"Client %d played %s. Reply: %s \n\r",sender_tid, int2RPS(choice), "Tie");
                        bwprintf(COM2, (char*)"Client %d played %s. Reply: %s \n\r",opponent_tid, int2RPS(choices[opponent_tid]), "Tie");
                    }else{
                        Reply(sender_tid, "Lose", 4);
                        Reply(opponent_tid, "Win", 3);  

                        // stdoutput the information
                        bwprintf(COM2, (char*)"Client %d played %s. Reply: %s \n\r",sender_tid, int2RPS(choice), "Lose");
                        bwprintf(COM2, (char*)"Client %d played %s. Reply: %s \n\r",opponent_tid, int2RPS(choices[opponent_tid]), "Win");
                    }
                    // reset
                    choices[opponent_tid] = 0;
                }else{
                    // store the choice
                    choices[sender_tid] = choice;
                }
            }
        }else if(0 == strcmp(receive_buf, "Quit")){           // quit
            // stdoutput the information
            if(0 != opponents[sender_tid]){     // quit before opponent
                bwprintf(COM2, (char*)"Client %d quitted before its opponent, its opponent is %d.\n\r",sender_tid, opponents[sender_tid]);
            }else{
                bwprintf(COM2, (char*)"Client %d quitted after its opponent.\n\r",sender_tid);
            }

            Reply(sender_tid, "Quit", 4);

            // if the other player is waiting
            if(0 != choices[opponents[sender_tid]]){
                Reply(opponents[sender_tid], "Other Player Quit", 17);
            }
            opponents[opponents[sender_tid]] = 0;
            opponents[sender_tid] = 0;
        }
    }
}

// helper function for RPS Clients

// player plays a choice, Rock:1, Paper:2 , Scissor:3 
void playerPlay(int serverTid, short rps, char* reply, int replyLen){
    char msg[] = "PlayR";
    if (2 == rps){
        msg[4] = 'P';
    }else if(3 == rps){
        msg[4] = 'S';
    }
    Send(serverTid, msg, 5, reply, replyLen);
}

// RPS Clients
void RPSClient(){

	int serverTid, replyLen = 25;
    short nRounds = 3, choice = 1;
	char msg[] = "Sign", reply[replyLen+1];

    // find the RPS server by querying the name server
    serverTid = WhoIs("RPSServer");
    
    // perform a set of requests that adequately test the RPS server,
    // sign up and blocked until reply is received
    Send(serverTid, msg, 4, reply, replyLen);

    // pick a random number for the number of rounds, in [2,10]
    nRounds = (short)rand(2,10);
    
    // play a random choice
    for(int i = 0; i < nRounds ; i++){
        choice = (short)rand(1,3);     // Rock:1, Paper:2 , Scissor:3 
        playerPlay(serverTid, choice, reply, replyLen);

        // stop playing if the other player quitted
        if(0 == strcmp(reply, "Other Player Quit")){
            break;      
        }
    }

    // quit
    strcpy(msg,"Quit");
    Send(serverTid, msg, 4, reply, replyLen);

    #ifdef DEBUG_TASK_AE
    bwprintf(COM2, (char*)"[Debug] Client quit gracefully \r\n");
    #endif
    // Exit
}

void task_first_task_k2()
{
    #ifdef DEBUG_TASK_AE
    bwprintf(COM2, (char *)"[Debug] First task is started\n\r");
    #endif
    
    // creates the name server
    name_server_tid = Create(ETP_High, task_name_server);

    // creates the RPS server
    Create(ETP_Medium, RPSServer);

    // creates 6 (NUM_CLIENTS) RPS clients
    for(int i = 0; i < NUM_CLIENTS; i++){
        Create(ETP_Low, RPSClient);
    }

    bwprintf(COM2, (char *)"FirstUserTask: exiting\n\r");
    // Exit
}

// -- measurements --

void task_measurement_receiver(){       // tid: 1
	int sender_tid, it = 0;
	char msg_buffer[MEASUREMENT_BUFFER_SIZE+1];

    for(; it < NUM_CYCLE; it++){
        Receive(&sender_tid, msg_buffer, MEASUREMENT_BUFFER_SIZE);
        Reply(sender_tid, msg_buffer, MEASUREMENT_BUFFER_SIZE);
    }
}

void task_measurement_sender(){
    // prepare variables
    unsigned int it = 0, start, end, cost, error = 0;
    char reply[MEASUREMENT_BUFFER_SIZE + 1] = {0};
    char msg_buffer[MEASUREMENT_BUFFER_SIZE + 1] = {0};

    // calculate the error of reading timer and the cycle itself
    start = ReadTimerUS();
    end = ReadTimerUS();
    error += end - start;

    start = ReadTimerUS();
    for(; it < NUM_CYCLE; it++){
		asm("NOP");
	}
    end = ReadTimerUS();
    error += end - start;

    // reset cycle
    it = 0;

    // measurement starts
    start = ReadTimerUS();
    for(; it < NUM_CYCLE; it++){
        asm("NOP");
        Send(1, msg_buffer, MEASUREMENT_BUFFER_SIZE, reply, MEASUREMENT_BUFFER_SIZE);
    }
    end = ReadTimerUS();

    cost = end - start - error;
    bwprintf(COM2, (char*)"In total: %u us (~%u s). Each iteration is around %u us. (us = microsecond)\n\r", cost, cost/1000000 , cost/NUM_CYCLE);
}

// ---- k3 ----

void task_client_k3()
{
    // requesting a delay interval, t in ticks, and a number, n, of delays from the first user task
    char reply[3];
    assert(2 == Send(MyParentTid(), "t", 1, reply, 2));

    // getting the parameters
    const int volatile delayInterval = (int)reply[0];
    const int volatile nDelays = (int)reply[1];

    // debug
    #ifdef DEBUG_TASK_AE
    bwprintf(COM2, (char*)"[Debug] Tid: %d, delay: %d, number of delays: %d.\n\r",myTid, delayInterval, nDelays);
    #endif

    // find the clock server
    const int volatile clock_server_tid = WhoIs(CLOCK_SERVER_NAME);

    // delay n times, each time delay t 
    for(int volatile i = 0; i < nDelays; i++){
        // delay t using DelayUntil
        // Delay(clock_server_tid, delayInterval);
        DelayUntil(clock_server_tid, Time(clock_server_tid) + delayInterval);
        
        // print its tid
        bwprintf(COM2, (char*)"Tid: %d. Delay: %d.: %d delays.\n\r",MyTid(), delayInterval, i + 1);
    }
}

void task_first_task_k3()
{
    #ifdef DEBUG_TASK_AE
    bwprintf(COM2, (char *)"[Debug] First task is started\n\r");
    #endif
    
    int sender_tid;
    char receive_buf[2], reply_buffer[2];

    // creates the name server
    name_server_tid = Create(ETP_Highest, task_name_server);    // tid: 3

    // creates the clock server
    Create(ETP_Highest, task_clock_server);                     // tid: 4

    // creates client tasks with different priorities
    Create(ETP_High, task_client_k3);                       // tid: 5
    Create(ETP_Medium, task_client_k3);                     // tid: 6
    Create(ETP_Low, task_client_k3);                        // tid: 7
    Create(ETP_Lowest, task_client_k3);                     // tid: 8

    // sending parameters to the clients
    assert(1 == Receive(&sender_tid, receive_buf, 1));      // task 5
    assert(0 == strcmp("t", receive_buf));
    reply_buffer[0] = 10;
    reply_buffer[1] = 20;
    Reply(sender_tid, reply_buffer, 2);

    assert(1 == Receive(&sender_tid, receive_buf, 1));      // task 6
    assert(0 == strcmp("t", receive_buf));
    reply_buffer[0] = 23;
    reply_buffer[1] = 9;
    Reply(sender_tid, reply_buffer, 2);

    assert(1 == Receive(&sender_tid, receive_buf, 1));      // task 7
    assert(0 == strcmp("t", receive_buf));
    reply_buffer[0] = 33;
    reply_buffer[1] = 6;
    Reply(sender_tid, reply_buffer, 2);

    assert(1 == Receive(&sender_tid, receive_buf, 1));      // task 8
    assert(0 == strcmp("t", receive_buf));
    reply_buffer[0] = 71;
    reply_buffer[1] = 3;
    Reply(sender_tid, reply_buffer, 2);

    // Exit
}

void task_first_task_k4()
{
    #ifdef DEBUG_TASK_AE
    bwprintf(COM2, (char *)"[Debug] First task is started\n\r");
    #endif
    // creates the name server
    name_server_tid = Create(ETP_Highest, task_name_server);    // tid: 2

    // creates the clock server
    Create(ETP_Highest, task_clock_server);                     // tid: 3
    Create(ETP_Highest, task_uart_server);                      // tid: 5

    Create(ETP_Medium, task_train_control);                     // tid: 12
}