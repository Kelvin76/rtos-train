#include "interrupt.h"
#include "ts7200.h"
extern "C" {
    #include "bwio.h"
}
#include "global.h"


void interrupt_init() {
	volatile int *reg;
    reg = (volatile int *)(VIC1_BASE + VIC_INT_ENABLE_OFFSET);
	*reg = IRQ_STATUS_TC1UI; // only enable timer interrupt

    reg = (volatile int *)(VIC2_BASE + VIC_INT_ENABLE_OFFSET);
	*reg = INT_UART2_MASK | INT_UART1_MASK; // enable UART2 and UART 1 general interrupt

    enable_uart_2_timeout();
    enable_uart_tx_interrupt(COM2);
    enable_uart_tx_interrupt(COM1);
    enable_uart_1_CTS_interrupt();
    enable_uart_1_rx_interrupt();
}

void enable_uart_2_timeout() {
	volatile int *reg;
    reg = (volatile int*)(UART2_BASE + UART_CTLR_OFFSET);
    *reg |= RTIEN_MASK;  // enable uart2 Receive Timeout 
}

void disable_uart_2_timeout() {
    volatile int *reg = (volatile int*)(UART2_BASE + UART_CTLR_OFFSET);;
    *reg &= ~RTIEN_MASK;  // disable uart2 Receive Timeout 
}

void enable_uart_tx_interrupt(int channel) {
	volatile int *reg;
    switch(channel){
        case COM1:
            reg = (volatile int*)(UART1_BASE + UART_CTLR_OFFSET);
            break;
        default:
        case COM2:
            reg = (volatile int*)(UART2_BASE + UART_CTLR_OFFSET);
            break;
    }
    *reg |= TIEN_MASK; 
}

void disable_uart_tx_interrupt(int channel) {
	volatile int *reg;
    switch(channel){
        case COM1:
            reg = (volatile int*)(UART1_BASE + UART_CTLR_OFFSET);
            break;
        default:
        case COM2:
            reg = (volatile int*)(UART2_BASE + UART_CTLR_OFFSET);
            break;
    }   
    *reg &= ~TIEN_MASK; 
}

void enable_uart_1_CTS_interrupt() {
	volatile int *reg = (volatile int*)(UART1_BASE + UART_CTLR_OFFSET);
    *reg |= MSIEN_MASK; 
}

void disable_uart_1_CTS_interrupt() {
	volatile int *reg = (volatile int*)(UART1_BASE + UART_CTLR_OFFSET);
    *reg &= ~MSIEN_MASK;
}

bool is_uart_1_CTS_interrupt_enabled() {
	volatile int *reg = (volatile int*)(UART1_BASE + UART_CTLR_OFFSET);
    return *reg & MSIEN_MASK;
}

int get_cts_status() {
	volatile int *reg = (volatile int*)(UART1_BASE + UART_FLAG_OFFSET);
    return *reg & CTS_MASK;
}


void confirm_cts_interrupt() {
	volatile int *reg = (volatile int*)(UART1_BASE + UART_INTR_OFFSET);
    *reg = 0;
}

void enable_uart_1_rx_interrupt() {
	volatile int *reg = (volatile int*)(UART1_BASE + UART_CTLR_OFFSET);
    *reg |= RIEN_MASK; 
}

void disable_uart_1_rx_interrupt() {
	volatile int *reg = (volatile int*)(UART1_BASE + UART_CTLR_OFFSET);
    *reg &= ~RIEN_MASK; 
}

// see ep9300 manual, 3.2.1.1
asm(R"(
    .text
    .align 2
    .global	enable_halt
    .type enable_halt, %function
    enable_halt:
    ldr r0, =0x80930000   // Syscon base address
    mov r1, #0xaa       // sw lock key
    str r1, [r0, #0xc0] // unlock
    ldr r1, [r0, #0x80] // Turn on SHENA bit in DEVCFG register
    orr r1, r1, #0x00000001
    str r1, [r0, #0x80]
    bx lr
    )"
);


void halt() {
    // read value from this register to trigger halt
    __attribute__((unused))auto dummy_value = *(volatile int*)HALT_UNTIL_INT_BASE;
}
