#include "global.h"
#include "trainControl.h"
#include "track_node.h"
#include "track.h"

track_edge::track_edge() : reverse{nullptr}, src{nullptr}, dest{nullptr}, dist{0} {}

track_node::track_node() : name{nullptr}, type{NodeType::NODE_NULL}, num{0}, reverse{nullptr}, current_dir{DIR_AHEAD} {}

track_edge* track_node::next_edge(NodeDir dir){
    switch (this->type)
    {
    case NodeType::NODE_SENSOR:
    case NodeType::NODE_ENTER:
    case NodeType::NODE_MERGE:
        return &(this->edge[DIR_AHEAD]);
        break;
    case NodeType::NODE_BRANCH:
        return &(this->edge[(dir == DIR_AHEAD)? current_dir : dir]);
        break;
    case NodeType::NODE_NULL:
    case NodeType::NODE_EXIT:
    default:
        return nullptr;
        break;
    }
}

track_node* track_node::next_node(NodeDir dir){
    track_edge* edge = next_edge(dir);
    return (edge)? edge->dest : nullptr;
}

track_node* track_node::step_one_edge(unsigned int& distance, NodeDir dir) {
    /*
        Step one edge, return next node, set distance to edge distance
    */
    track_edge* edge = next_edge(dir);
    if(edge){
        distance = edge->dist;
        return edge->dest;
    }
    distance = 0;
    return nullptr;
}

void track_node::setReservedBy(int train_number){
    #ifdef DEBUG_RESERVATION
    debugprintf("\x1b[37;1f%d reserves %s", train_number ,name);
    #endif
    reservedBy = train_number;
}

void track_node::clearReserve(){
    reservedBy = 0;
}

unsigned int track_node::distance_to_next_sensor(NodeDir dir) {
    unsigned int distance = 0;
    track_node* cur = this->step_one_edge(distance, dir);

    while ( cur && cur->type != NodeType::NODE_SENSOR) {
        unsigned int step_dist;
        cur = cur->step_one_edge(step_dist, dir);
        distance += step_dist;
    }
    return distance;
}

track_node* track_node::next_sensor(NodeDir dir) {
    unsigned int distance;
    track_node* cur = this->step_one_edge(distance, dir);

    while ( cur && cur->type != NodeType::NODE_SENSOR) {
        cur = cur->step_one_edge(distance, dir);
    }
    return cur;
}

Location::Location() : landmark(nullptr), offset_mm(0) {}

Location::Location(track_node* landmark, int offset_mm) {
    set_location(landmark, offset_mm);
}

void Location::set_location(track_node* landmark, int offset_mm) {
    if (offset_mm < 0) {
        // convert to positive offset
        landmark = landmark->reverse;
        offset_mm *= -1;
    }
    this->landmark = landmark;
    this->offset_mm = offset_mm;
}

void Location::set_location(Location &loc){
    set_location(loc.get_landmark(), loc.get_offset_mm());
}

track_node* Location::get_landmark() const {
    return landmark;
};

unsigned int Location::get_offset_mm() const {
    return offset_mm;
};


void Location::caliberate() {
    /*
    move landmark forward/backward such that offset is minimum and positive
    Useful for move train for a smaller distance
    I am not currently calling this by default (in constructor) because
    sometimes it is useful to have landmark + non-minimum offset
    For example, sometimes we want landmark to be a sensor (return value of path planning)
    If we caliberate, we might end up with a branch + offset
    */
    unsigned int d_to_next_landmark;
    landmark->step_one_edge(d_to_next_landmark);
    while (d_to_next_landmark < offset_mm) {
        offset_mm -= d_to_next_landmark;
        track_node* tmp_landmark = landmark->step_one_edge(d_to_next_landmark);
        if (tmp_landmark) {
            landmark = tmp_landmark;
            landmark->step_one_edge(d_to_next_landmark);
        } else {
            offset_mm = 0;
        }
    }
}

Location Location::get_reverse_location(){
    this->caliberate();
    Location location(nullptr, 0);

    if(offset_mm > 0){
        track_node* reverse_landmark;
        unsigned int d_to_next_landmark;
        reverse_landmark = landmark->step_one_edge(d_to_next_landmark)->reverse;
        if(reverse_landmark){
            int reverse_offset_mm = (-1)*(int)(offset_mm - d_to_next_landmark);
            assert(reverse_offset_mm >= 0, (char*)"Reverse offset pos");
            location.set_location(reverse_landmark, reverse_offset_mm);
        }else{
            location.set_location(nullptr, 0);
        }
    }else{  // offset is 0
        location.set_location(landmark->reverse, offset_mm);
    }
    return location;
}

unsigned int Location::distance_to_next_sensor() {
    return landmark->distance_to_next_sensor() - offset_mm;
}
