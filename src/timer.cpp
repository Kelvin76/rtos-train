#include "timer.h"
#include "ts7200.h"
#include "global.h"

// initialize timer
int timerInit(){
    // -- tc3 setup
	// disable the 32-bit timer
	volatile int *line = (int *)(TIMER3_BASE + CRTL_OFFSET);
	*line &= ~ENABLE_MASK;

	// load initial value
	volatile int *data = (int *)(TIMER3_BASE + LDR_OFFSET);
	*data = TIMER_MAX;

	// enable: 1, CLKSEL: 1 (508kHz), mode: 0 (free running)
	*line |= (ENABLE_MASK & (~MODE_MASK)) | CLKSEL_MASK;

    // -- tc1 setup
    // for timer interrupts
	// disable tc1
	line = (int *)(TIMER1_BASE + CRTL_OFFSET);
	*line &= ~ENABLE_MASK;

	// load initial value
	data = (int *)(TIMER1_BASE + LDR_OFFSET);
	*data = TIMER_10MS_TICKS;

	// enable: 1, CLKSEL: 1 (508kHz), mode: 1 (pre-load)
	*line |= ENABLE_MASK | MODE_MASK | CLKSEL_MASK;

	return 0;
}
