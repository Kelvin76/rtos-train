#include "math_lib.h"
#include "global.h"

// ref: https://en.wikipedia.org/wiki/Integer_square_root
int math_lib::integer_sqrt_iter(int n) {
    assert(n >=0, (char*)"math_lib::integer_sqrt_iter negative argument");
    if (n < 2) {
        return n;
    }

    // Find the shift amount. See also [[find first set]],
    // shift = ceil(log2(n) * 0.5) * 2 = ceil(ffs(n) * 0.5) * 2
    int shift = 2;
    while ((n >> shift) != 0) {
        shift += 2;
    }

    // Unroll the bit-setting loop.
    int result = 0;
    while (shift >= 0) {
        result = result << 1;
        int large_cand = result + 1; // Same as result ^ 1 (xor), because the last bit is always 0.
        if (large_cand * large_cand <= n >> shift) {
            result = large_cand;
        }
        shift -= 2;
    }

    return result;
}

// ref https://www.programiz.com/cpp-programming/examples/quadratic-roots
int math_lib::solve_quadratic_formula(int a, int b, int c) {
    // only return smallest positive root
    int discriminant = b*b - 4*a*c;
    
    int x1, x2;
    if (discriminant > 0) {
        x1 = (-b + integer_sqrt_iter(discriminant)) / (2*a);
        x2 = (-b - integer_sqrt_iter(discriminant)) / (2*a);
        if (x1 > 0 && x2 > 0) {
            return (x1 < x2)? x1 : x2;
        }
        else if (x1 > 0) {
            return x1;
        }
        else if (x2 > 0) {
            return x2;
        }
    }
    
    else if (discriminant == 0) {
        x1 = -b/(2*a);
        if (x1 > 0) return x1;
    }

    return -1;
}