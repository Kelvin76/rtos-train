#include <io_lib.h>
#include <task_lib.h>
#include <ts7200.h>
#include <string.h>
#include <global.h>
#include "trainControl.h"
#include <RBuf.h>
#include "train.h"

/**
    Ref: some of this code is inspired by previous work on the train system (assignment 0)
    1. The concept of sending delayed command
    2. The way to use cursor addressing
    3. The instructions sent to the train system
    4. Sensor encoding/decoding (functions)
*/

// ---- helper functions for printing ----

// print debug info starting on line 22
void debugprintf(char* input_str, ...){
    char fmt[70] = "\x1b[22;1f";
    strcat(fmt, input_str);
    strcat(fmt, "\x1b[20;1f");

    va_list va;
    va_start(va, input_str);
    va_cprintf(COM2, fmt, va);
    va_end(va);
}

Train* TrainSystem::get_train(int train_number) {
    // hardcode the train_number <-> trains index mapping
    int trains_idx = -1;
    switch (train_number)
    {
    case 1:
        trains_idx = 0;
        break;
    case 2:
        trains_idx = 1;
        break;
    case 24:
        trains_idx = 2;
        break;
    case 58:
        trains_idx = 3;
        break;
    case 74:
        trains_idx = 4;
        break;
    case 78:
        trains_idx = 5;
        break;
    default:
        break;
    }
    return (trains_idx == -1)? nullptr : &trains[trains_idx];
}

// print the prompt information on the line after "input>"
void TrainSystem::printPrompt(char* input_str){
    cprintf(uart_tid, COM2, (char*)"\x1b[21;1f%s\x1b[20;1f", input_str);
}

void TrainSystem::printSensorTriggerPrediction(int time_diff, int distance_diff) {
    cprintf(uart_tid, COM2, (char*)"\x1b[s\x1b[3;78f%d ms, %d mm\x1b[0K\x1b[u", time_diff, distance_diff);
}

void TrainSystem::printTrainTable(){
    cprintf(uart_tid, COM2, (char*)"\x1b[s\x1b[5;78f--------------------------- Train information -------------------------");
    // original version
    // cprintf(uart_tid, COM2,  (char*)"\x1b[6;78f|T#|   State   |WO |v |Location  | Dest    |Next | In  |Acc   | reserve");
    // current version
    cprintf(uart_tid, COM2,  (char*)"\x1b[6;78f|T#|   State   |WO |v |Location  | Dest    |Next | reserve");
    cprintf(uart_tid, COM2,  (char*)"\x1b[7;78f|01|");
    cprintf(uart_tid, COM2,  (char*)"\x1b[8;78f|02|");
    cprintf(uart_tid, COM2,  (char*)"\x1b[9;78f|24|");
    cprintf(uart_tid, COM2, (char*)"\x1b[10;78f|58|");
    cprintf(uart_tid, COM2, (char*)"\x1b[11;78f|74|");
    cprintf(uart_tid, COM2, (char*)"\x1b[12;78f|78|\x1b[u");
}

inline char* i2a_with_size(char str[], int n, int length){
    int len = length;
    memcpy(str, "00000000", len);
    for (int val = (n<0)? -n : n ; len > 0 && val != 0 ; --len , val /= 10){
       str[len-1]= val % 10 + '0';
    }
    if (len >= 0 && n<0 ){ str[0] = '-'; }
    str[length] = 0;
    return str;
}

void TrainSystem::updateTrainStatus(int row_offset, Train* tr, RBuf<ReservedSensorData> &rsensor_buf) {
    // use table
    const char* location_name;
    unsigned int location_offset = 0;

    const char* dest_name;
    unsigned int dest_offset = 0;

    const char* state_name;

    const char* wait_on_train;

    const char* next_expected_sensor_name;
    unsigned int time_to_next_sensor = 0;

    int a = 0;

    switch (tr->get_train_state() ) {
        case Train::State::TravellingToDest:
            state_name = "In motion";
            break;
        case Train::State::StandBy:
            if(tr->is_random_player()){
                state_name = "Halt     ";
            }else{
                state_name = "Stand By ";
            }
            break;
        case Train::State::WaitingForOthers:
            state_name = "Waiting  ";
            break;
        case Train::State::Reversing:
            state_name = "Reversing";
            break;
        case Train::State::RegisteredToStop:
            state_name = "Stopping ";
            break;
        case Train::State::FreeRunning:
            state_name = "Free Run ";
            break;
        default:
            state_name = "NA       ";
            break;
    }

    char train[3];
    if(Train::State::WaitingForOthers == tr->get_train_state() && tr->waitingOnNode){
        if(tr->waitingOnNode->reservedBy){
            i2a_with_size(train, tr->waitingOnNode->reservedBy, 2);
        }else{
            i2a_with_size(train, tr->waitingOnNode->reverse->reservedBy, 2);
        }
        wait_on_train = train;
    }else{
        wait_on_train = "NA";
    }

    if (tr->get_current_estimated_location().get_landmark() && tr->get_current_estimated_location().get_landmark()->next_sensor()) {
        next_expected_sensor_name = tr->get_current_estimated_location().get_landmark()->next_sensor()->name;
        time_to_next_sensor = tr->estimate_time_ms(tr->get_current_estimated_location().distance_to_next_sensor());
    }else{
        next_expected_sensor_name = "NA ";
    }

    if (tr->get_current_estimated_location().get_landmark()) {
        Train::PhysicalStateSnapshot s = tr->get_current_physical_state_snapshot();
        location_name = s.location.get_landmark()->name;
        location_offset = s.location.get_offset_mm();
        a = s.a;
    }else{
        location_name = " ? ";
    }

    if (tr->destination.get_landmark()) {
        dest_name = tr->destination.get_landmark()->name;
        dest_offset = tr->destination.get_offset_mm();
    }else{
        dest_name = " ? ";
    }

    // tmp string
    char speed[3];
    i2a_with_size(speed, tr->get_normalized_speed(), 2);
    char loff[5];
    i2a_with_size(loff, location_offset, 4);
    char doff[5];
    i2a_with_size(doff, dest_offset, 4);

    // original version
    // char in[5];
    // i2a_with_size(in, time_to_next_sensor, 4);
    // char acc[6];
    // i2a_with_size(acc, a, 5);
    // char n_reserved[3];
    // i2a_with_size(n_reserved, tr->reserved_num, 2);

    // cprintf(uart_tid, COM2, (char*)"\x1b[s\x1b[%d;83f%s  %s  %s  %s+%s  %s+%s  %s  %s  %s  %s: ",
    // 7+row_offset,
    // state_name,
    // wait_on_train,
    // speed,
    // location_name,
    // loff,
    // dest_name,
    // doff,
    // next_expected_sensor_name,
    // in,
    // acc,
    // n_reserved);

    // current version
    cprintf(uart_tid, COM2, (char*)"\x1b[s\x1b[%d;83f%s  %s  %s  %s+%s  %s+%s  %s  |",
    7+row_offset,
    state_name,
    wait_on_train,
    speed,
    location_name,
    loff,
    dest_name,
    doff,
    next_expected_sensor_name);

    // print reserved sensors for each train
    if(0 != tr->reserved_num && !rsensor_buf.is_buf_empty()){
        int index = rsensor_buf.get_start();
        do{
            if(tr->get_train_number() == rsensor_buf[index].train_num){
                cprintf(uart_tid, COM2, (char*)"%s ", rsensor_buf[index].name);
            }
        } while(!rsensor_buf.get_next_index(index));
    }else{
        cprintf(uart_tid, COM2, (char*)" NA");
    }

    cprintf(uart_tid, COM2, (char*)"\x1b[0K\x1b[u");
}

void TrainSystem::clearPrompt(){
    cprintf(uart_tid, COM2, (char*)"\x1b[21;1f\x1b[0J\x1b[1F");
}
void TrainSystem::refreshCli(){
    cprintf(uart_tid, COM2, (char*)"\x1b[20;1f\x1b[2K\x1b[1GInput>");
}
// print the basic structure for display
void TrainSystem::initUI(){
    // print titles
    cprintf(uart_tid, COM2, (char*)"\x1b[2;1f---Switch Table---     --Triggered Sensors & Train# (higher is newer)--     --Sensor Prediction Time & Distance Diff--\r\n");

    // print the switch table
    int index = 1;
    for (; index <= 7; index += 2){
        cprintf(uart_tid, COM2, (char*)" %d   : S # %d   : S\n\r", index, index+1);
    }
    cprintf(uart_tid, COM2, (char*)" 9   : S # 10  : S\n\r");
    for (index = 11; index <= 18; index += 2){
        cprintf(uart_tid, COM2, (char*)" %d  : S # %d  : S\n\r", index, index+1);
    }
    for (index = 153; index < 157; index += 2){
        cprintf(uart_tid, COM2, (char*)" %d : C # %d : S\n\r", index, index+1);
    }

    // update switch table for broken turnouts
    if(track.is_track_a){
        // track a: broken switches: BR3 - 84, BR155 - 120, BR156 - 122
        updateSwitchTable(3, 'b');
        updateSwitchTable(16, 'b');
        updateSwitchTable(155, 'b');
        updateSwitchTable(156, 'b');
    }else{
        // track b: broken switches: BR4 - 86, BR5 - 88, BR155 - 120, BR156 - 122
        updateSwitchTable(4, 'b');
        updateSwitchTable(5, 'b');
        updateSwitchTable(155, 'b');
        updateSwitchTable(156, 'b');
    }
    #ifdef SENSOR_MEASUREMENT
    cprintf(uart_tid, COM2, (char*)"\x1b[s\x1b[3;25fSensor response: NA ms - NA ms  \x1b[u");
    #endif

    // Train table
    printTrainTable();
}

// ---- other helper functions ----

// parse a string and turn it into integer 
// base 10, no length check, could overflow
int a2d(char* str, bool &good){
    good = true;
    int res = 0;
    bool is_negative = str[0] == '-';
    size_t index = (is_negative)? 1 : 0; 
    for(; index < strlen(str); index++){
        if(str[index] >= '0' && str[index] <= '9'){
            res = res*10 + str[index] - '0';
        } else{
            good = false;
            return -1;
        }
    }
    return (is_negative) ? (-1)*res : res;
}

EExecutionResult TrainSystem::train_path_planning(Train* tr, Location &dest, bool reverse_start, unsigned int min_distance){
    // reverse_start (default: false), if true, will use reverse node of the location as the starting point
    
    unsigned int dist[TRACK_MAX];   // the shortest distance from src to any node with index i
    int parent[TRACK_MAX] = {-1};   // used to get the shortest path
    Location train_location = tr->get_current_estimated_location();

    Location src = (reverse_start)? train_location.get_reverse_location() : train_location;
    /*
    | -begin_offset |
    |track.path_planning distance|dest.get_offset|  = total distance of this path
    src_landmark-> src  ...  --> dest_landmark -> dest
    */
    int begin_offset = -src.get_offset_mm(); // should subtract src.get_offset_mm from distance to get actual distance
    // Source is the same as the target, change the starting point to the next sensor

    if (src.get_landmark() == dest.get_landmark()) {
        // Do not allow such a path
        if(reverse_start){
            tr->clear_destination();
            return reverseTrainAndStop(tr->get_train_number(), false);
        }
        return EExecutionResult::EER_DestinationSameAsSource;

        // TODO: could allow this behavior, but currently it's better without it
        
        // if we just passed the dest sensor, ie src == dest
        // then dijkstra won't tell use the correct result
        // ideally, we should reverse the train to go to the dest
        // right now we want to keep things simple, so we start the 
        // path planning from next sensor
        // so it will still generate a full path from src to dest
        assert(src.get_landmark()->next_node(), (char*)"Next node does not exist"); // deal with this later
        /*
                        |begin_offset|track.path_planning distance|dest.get_offset|  = total distance of this path
        src_landmark-> src    ----> the node after src  ...  --> dest_landmark -> dest
        */
        begin_offset = src.get_landmark()->next_edge()->dist - src.get_offset_mm();
        src.set_location(src.get_landmark()->next_node(), 0);
    }else if (src.get_landmark()->reverse == dest.get_landmark()){
        if(!reverse_start){
            tr->clear_destination();
            return reverseTrainAndStop(tr->get_train_number(), false);
        }
        return EExecutionResult::EER_DestinationSameAsSource;
    }
    track_node* dijkstra_start_node = src.get_landmark();
    track.path_planning(dijkstra_start_node, dest.get_landmark(), dist, parent, tr);            

    // TODO: in tc2, we should also taking reverse the train into consideration
    // check the distance to the target or its reverse node
    Location reverse_dest = dest.get_reverse_location();

    unsigned int distance2Dest = dist[track.get_track_node_idx(dest.get_landmark())];
    unsigned int distance2ReverseDest = (reverse_dest.get_landmark())? dist[track.get_track_node_idx(reverse_dest.get_landmark())] : U_INT_MAX;

    if(distance2Dest == U_INT_MAX && distance2ReverseDest == U_INT_MAX){
        return EExecutionResult::EER_PathNotFound;
    }

    if (distance2Dest != U_INT_MAX) {
        distance2Dest = dist[track.get_track_node_idx(dest.get_landmark())] + begin_offset + dest.get_offset_mm();
        if (
            train_location.get_landmark() == dest.get_landmark() && 
            train_location.get_offset_mm() < dest.get_offset_mm()
        ) {
            // special case: train location and destination is the same
            // but destination is in front of train location
            unsigned int d = dest.get_offset_mm() - train_location.get_offset_mm();
            distance2Dest = d;
        }
    }

    if (distance2ReverseDest != U_INT_MAX) {
        distance2ReverseDest = (reverse_dest.get_landmark())? (dist[track.get_track_node_idx(reverse_dest.get_landmark())] + begin_offset + reverse_dest.get_offset_mm()): U_INT_MAX;
    }

    int desired_speed = 0;

    // find if one distance is above the safe distance for acceleration (hard-coded)
    if((U_INT_MAX != distance2Dest) && (min_distance <= distance2Dest) && (tr->is_dynamic_stop_possible(distance2Dest) != -1)){
        #ifdef DT_INFO
        debugprintf((char*)"src: %s + %u, dest: %s + %u, dist: %u", train_location.get_landmark()->name, train_location.get_offset_mm(), dest.get_landmark()->name, dest.get_offset_mm(), distance2Dest);
        #endif

        if(reverse_start) { reverseTrainAndStop(tr->get_train_number(), false);}
        desired_speed = tr->is_dynamic_stop_possible(distance2Dest);
        set_train_path(*tr, dijkstra_start_node, train_location, dest, dist, parent, distance2Dest);
        tr->set_detination(dest);
    }else if((U_INT_MAX != distance2ReverseDest) && (min_distance <= distance2ReverseDest) && (tr->is_dynamic_stop_possible(distance2ReverseDest) != -1)){
        #ifdef DT_INFO
        debugprintf((char*)"src: %s, dest: %s + %u, dist: %u", train_location.get_landmark()->name, reverse_dest.get_landmark()->name, reverse_dest.get_offset_mm(), distance2ReverseDest);
        #endif

        if(reverse_start) { reverseTrainAndStop(tr->get_train_number(), false);}
        desired_speed = tr->is_dynamic_stop_possible(distance2ReverseDest);
        set_train_path(*tr, dijkstra_start_node, train_location, reverse_dest, dist, parent, distance2ReverseDest);
        tr->set_detination(reverse_dest);
    }else{
        return EExecutionResult::EER_PathNotFound;
    }

    tr->clear_reserve(track);
    tr->reserve_track_in_the_front(this);
    return EExecutionResult::EER_OK;
}

int TrainSystem::set_train_path(Train& tr, track_node* dijkstra_start_node, Location& train_location, Location& dest, unsigned int dist[TRACK_MAX], int parent[TRACK_MAX], unsigned int total_dist){ 
    // pre-condition: tr must exist
    // pre-condition: dist and parent are already calculated

    // TODO: this condition seems to be unnecessary
    if(U_INT_MAX != dist[track.get_track_node_idx(dest.get_landmark())]){
        // path found
        const unsigned int stop_distance_mm = tr.get_stop_distance();
        const int dest_index = track.get_track_node_idx(dest.get_landmark());
        unsigned int cur_index;
        // if the distance from us to stopping location is smaller than from us to next sensor
        // then we will need to stop before next sensor
        bool passed_stop_sensor = (total_dist - tr.get_stop_distance()) < train_location.distance_to_next_sensor();

        if (passed_stop_sensor) {
            // already passed stop sensor, send delayed stop command now
            delayedStopTrain(tr, -1, total_dist);
        } else {         
            // find the stopping sensor
            cur_index = dest_index;          
            unsigned int cur_dist = dest.get_offset_mm(); // cur_dist is distance from stop sensor to stopping location

            while (cur_dist < stop_distance_mm) {
                // accumulating distance
                cur_dist += dist[cur_index] - dist[parent[cur_index]];
                cur_index = parent[cur_index];
            }

            // we got the last landmark before we issue the stop command
            // but it is not necessarily a sensor
            // find the sensor before this
            while (track[cur_index].type != NodeType::NODE_SENSOR && &track[cur_index] != dijkstra_start_node) {
                cur_dist += dist[cur_index] - dist[parent[cur_index]];
                cur_index = parent[cur_index]; 
            }
            
            /** set the stop_sensor : a Location object
                the train should send stop command at this location
                the landmark of this location is guaranteed to be a sensor
                stop_sensor_return + stop distance = dest */
            Location stop_sensor = Location(&track[cur_index], cur_dist - stop_distance_mm);
            tr.stop_at(stop_sensor);


            #ifdef DEBUG_ROUTING_PLAN
            debugprintf("Dist: %d, stop at %s %d, sd: %d", dist[dest_index], stop_sensor.get_landmark()->name, stop_sensor.get_offset_mm(), stop_distance_mm);
            cprintf(COM2, "\x1b[3E");
            cprintf(COM2, "%d\r\n", dist[dest_index]);    
            int i = dest_index;
            while (parent[i] != -1) {
                cprintf(COM2, "%s\r\n", track[i].name);
                i = parent[i];
            }
            cprintf(COM2, "\x1b[20;1f");
            #endif
        }
        // update path

        // clear previous path
        tr.clear_path_buf();

        // store the new path for the train
        cur_index = dest_index;
        if(NodeType::NODE_SENSOR == track[cur_index].type || NodeType::NODE_BRANCH == track[cur_index].type){
            tr.path_node_buf.push_front_rbuf_item(Train::PathNode(&track[cur_index]));
        }

        while (cur_index != track.get_track_node_idx(dijkstra_start_node)) {
            if(NodeType::NODE_SENSOR == track[parent[cur_index]].type){
                // store sensor nodes
                tr.path_node_buf.push_front_rbuf_item(Train::PathNode(&track[parent[cur_index]]));
            }else if(NodeType::NODE_BRANCH == track[parent[cur_index]].type){
                // store branch nodes
                if(0 == strcmp(track[cur_index].name, track[parent[cur_index]].edge[DIR_STRAIGHT].dest->name)){
                    tr.path_node_buf.push_front_rbuf_item(Train::PathNode(&track[parent[cur_index]], DIR_STRAIGHT));
                }else{
                    tr.path_node_buf.push_front_rbuf_item(Train::PathNode(&track[parent[cur_index]], DIR_CURVED));
                }
            }
            cur_index = parent[cur_index];
        }

        #ifdef DEBUG_ROUTING
        int index = tr.path_node_buf.get_start();
        cprintf(uart_tid, COM2, "\x1b[s\x1b[19;1f");
        do{
            cprintf(uart_tid, COM2, "%d:%s ", index, tr.path_node_arr[index].node->name);
        } while(!tr.path_node_buf.get_next_index(index));
        cprintf(uart_tid, COM2, "\x1b[u");
        #endif

        return dist[dest_index];
    }
    // if there is no path to the destination
    return -1;
}

// ---- command helpers ----

void TrainSystem::sendCommand(char* const commands, const int size){
    Putstr(uart_tid, COM1, commands, size);
}

// can only send delayed commands with size of 2
void TrainSystem::sendDelayedCommandDouble(char* const commands, const unsigned int activationTick){
    // msg format: [(char type), (unsigned int activationTime), (char*)]
    short totalSize = sizeof(char) + sizeof(unsigned int) + 2;
    char msg_buffer[totalSize + 1];
	char reply_buffer[] = "0";

    msg_buffer[0] = DELAY_COMMAND_2_CHAR;
    uint2Chars(&msg_buffer[1], activationTick);
    memcpy(&msg_buffer[1 + sizeof(unsigned int)], commands, 2);

    if(0 > Send(delayed_command_task_tid, msg_buffer, totalSize, reply_buffer, 1)){
        assert(false, (char*)"Exception in sendDelayedCommandDouble");
    }
}

// can only send delayed commands with size of 1
void TrainSystem::sendDelayedCommandSingle(char* const commands, const unsigned int activationTick){
    // msg format: [(char type), (unsigned int activationTime), (char*)]
    short totalSize = sizeof(char) + sizeof(unsigned int) + 1;
    char msg_buffer[totalSize + 1];
	char reply_buffer[] = "0";

    msg_buffer[0] = DELAY_COMMAND_1_CHAR;
    uint2Chars(&msg_buffer[1], activationTick);
    memcpy(&msg_buffer[1 + sizeof(unsigned int)], commands, 1);

    if(0 > Send(delayed_command_task_tid, msg_buffer, totalSize, reply_buffer, 1)){
        assert(false, (char*)"Exception in sendDelayedCommandSingle");
    }
}

void TrainSystem::delayed_set_speed_async(Train& tr, int speed, int delay_until) {
    assert(tr.delayed_command_setspeed_worker_tid == -1, (char*)"Already delaying ss");
    assert(!delay_workers.is_buf_empty(), (char*)"No available workers ss");
    tr.delayed_command_setspeed_worker_tid = delay_workers.pop_rbuf_item();
    tr.delayed_command_setspeed_speed = speed;

    char reply_buf[5];
    memcpy(reply_buf, &delay_until, sizeof(delay_until));
    Reply(tr.delayed_command_setspeed_worker_tid, (const char*)&delay_until, 4);
}

void TrainSystem::delayed_reverse_async(Train& tr, int speed, int delay_until) {
    assert(tr.delayed_command_reverse_worker_tid == -1, (char*)"Already delaying re");
    assert(!delay_workers.is_buf_empty(), (char*)"No available workers re");
    tr.set_train_state(Train::State::Reversing);
    tr.delayed_command_reverse_worker_tid = delay_workers.pop_rbuf_item();
    tr.delayed_command_reverse_speed = speed;

    char reply_buf[5];
    memcpy(reply_buf, &delay_until, sizeof(delay_until));
    Reply(tr.delayed_command_reverse_worker_tid, (const char*)&delay_until, 4);
}

EExecutionResult TrainSystem::assert_train_state(Train *&tr, int train_number, bool check_activation_time){
    tr = get_train(train_number);
    if (!tr) { return EExecutionResult::EER_TrainNotFound; }
    if(check_activation_time && curTicks < tr->get_activation_time()){
        curTicks = Time(clock_tid);
        if(curTicks < tr->get_activation_time()){   // tried to set speed but before activation time
            // not 
            switch(tr->get_train_state()){
                case Train::State::Reversing:          
                    return EExecutionResult::EER_TrainAlreadyDelayedReversing; 
                case Train::State::WaitingForOthers:
                case Train::State::StandBy:
                case Train::State::TravellingToDest:
                case Train::State::FreeRunning:
                case Train::State::RegisteredToStop:
                default:
                    return EExecutionResult::EER_TrainInProcess;
            }
        }
    }
    return EExecutionResult::EER_OK;
}

void TrainSystem::delayedStopTrain(Train& tr, int stop_delay, int total_dist){
    // TODO: should check if train state is TravellingToDest

    // do not send repeated commands
    if(Train::State::RegisteredToStop != tr.get_train_state()) {return;} 

    char bytes[2];
    bytes[0] = (tr.lightOn)? TRAIN_SPEED_STOP + TRAIN_SPEED_LIGHT_OFFSET : TRAIN_SPEED_STOP;
    bytes[1] = tr.get_train_number();
    
    unsigned int delay_until = Time(clock_tid);
    delay_until += (stop_delay != -1)? stop_delay/10 : tr.estimate_time_ms(total_dist - tr.get_stop_distance())/10;

    sendDelayedCommandDouble(bytes, delay_until);
    delayed_set_speed_async(tr, TRAIN_SPEED_STOP, delay_until);
    tr.set_train_state(Train::State::RegisteredToStop);
}

// ====== functions used for implementing instructions ======

// ------------------------------------
// --- Instruction : switch turnout ---

// update the switch table
void TrainSystem::updateSwitchTable(unsigned short switchN, char direction){
    // find the col and row of target 
    short col = (switchN % 2)? 8 : 18;
    short row;
    if(switchN > 18){
        row = 12 + (switchN - 153)/2;
    }else{
        row = 3 + (switchN - 1)/2;
    }
    cprintf(uart_tid, COM2, (char*)"\x1b[s\x1b[%d;%df%c\x1b[u", row, col, direction + 'A' - 'a');
}

// Use this function to switch mutiple turnout if needed, REMEMBER to add turnOffSwitch at the end!!
// set switch direction and update the table
bool TrainSystem::setSwitch(char switchN, char direction, bool shouldUpdate){
    // pre-condition: all arguments are inside the allowed range
    char bytes[2];
    bytes[0] = (direction == 's')? 
    COMMAND_TURNOUT_STRAIGHT: 
    COMMAND_TURNOUT_CURVE;

    bytes[1] = switchN;

    // set_branch returns true if we should do the change
    // reasons for returning false: not a switch node, direction is the same as before
    if (track.set_branch(switchN, (direction == 's')? NodeDir::DIR_STRAIGHT : NodeDir::DIR_CURVED)){
        if (shouldUpdate){  // update switch table
            updateSwitchTable(switchN, direction);
        }
        sendCommand(bytes, 2);

        // switch the opposite turnout
        if (switchN > 18){      
            direction ^= 16;
            bytes[0] = (direction == 's')? 
            COMMAND_TURNOUT_STRAIGHT: 
            COMMAND_TURNOUT_CURVE;
            bytes[1] ^= (switchN >= 155)? 7 : 3;
            track.set_branch(bytes[1], (direction == 's')? NodeDir::DIR_STRAIGHT : NodeDir::DIR_CURVED);
            if (shouldUpdate){  // update switch table
                updateSwitchTable(bytes[1], direction);
            }
            sendCommand(bytes, 2);
        }

        // reset counter
        is_switch_on = true;
        switchCounter = 0;
        return true;
    }
    return false;
}

// turnoff the switch after 100 ms
void TrainSystem::turnOffSwitch(){
    if(is_switch_on){
        is_switch_on = false;
        char bytes[1];
        bytes[0] = COMMAND_SOLENOID_OFF;
        sendDelayedCommandSingle(bytes, Time(clock_tid) + 10);
    }
}

// turnoff the switch right now
void TrainSystem::turnOffSwitchNow(){
    if(is_switch_on){
        is_switch_on = false;
        char bytes[1];
        bytes[0] = COMMAND_SOLENOID_OFF;
        sendCommand(bytes, 1);
    }
}

// Use this function to switch a single turnout if needed
// switchN in [1,18] and [153,156]; direction in ['c', 's']
bool TrainSystem::switchTurnout(int switchN, const char direction){
    if(setSwitch(switchN, direction, true)){
        turnOffSwitch();
        return true;
    }
    return false;
}

// -------------------------------------
// --- Instruction : set train speed ---

int TrainSystem::setTrain(const int train_number, const int train_speed_level){
    // pre-condition: train number must exist
    Train* tr = get_train(train_number);
    return setTrain(*tr, train_speed_level);
}

int TrainSystem::setTrain(Train &tr, int train_speed_level){
    if(train_speed_level >= TRAIN_SPEED_LIGHT_OFFSET){ train_speed_level -= TRAIN_SPEED_LIGHT_OFFSET;}

    int res = -1;

    if(TRAIN_SPEED_STOP == train_speed_level){
        tr.set_train_state(Train::State::StandBy);
    }else if(tr.destination_exists()){
        tr.set_train_state(Train::State::TravellingToDest);
    }else{
        tr.set_train_state(Train::State::FreeRunning);
    }

    if(tr.get_normalized_speed() != train_speed_level){
        // bool is_cur_speed_zero = 0 == tr.get_normalized_speed();

        // do not save the command of reversing
        if(TRAIN_SPEED_REVERSE == train_speed_level){ 
            res = tr.set_speed(TRAIN_SPEED_STOP);
        }else{
            res = tr.set_speed(train_speed_level);
        }
        char bytes[2];
        bytes[0] = (tr.lightOn) ? train_speed_level + TRAIN_SPEED_LIGHT_OFFSET : train_speed_level;     // speed level
        bytes[1] = tr.get_train_number(); // train number
        sendCommand(bytes, 2);

        // // add horn sound for 1 second when setting speed to 12 from 0
        // if(is_cur_speed_zero && 12 == train_speed_level){
        //     bytes[0] = 69;
        //     sendCommand(bytes, 2);
                
        //     unsigned int delay_until = Time(clock_tid) + 100;
        //     bytes[0] = 64;
        //     sendDelayedCommandDouble(bytes, delay_until);
        // }
    }
    return res;
}

int TrainSystem::stopTrain(Train &tr){
    return setTrain(tr, TRAIN_SPEED_STOP);
}

EExecutionResult TrainSystem::trSetTrainSpeed(const int train_number, const int train_speed_level, bool check_activation_time){
    // only send command if activation time is earlier
    Train* tr;
    EExecutionResult res = assert_train_state(tr, train_number, check_activation_time);
    if(EExecutionResult::EER_OK != res) { return res;} 
    
    // this is only called by tr command, so only 2 states possible
    setTrain(*tr, train_speed_level);
    return EExecutionResult::EER_OK;
}

// -----------------------------------
// --- Instruction : reverse train ---
void TrainSystem::reverseTrain(const int train_number){
    // pre-condition: train number must exist
    Train* tr = get_train(train_number);
    reverseTrain(*tr);
}

void TrainSystem::reverseTrain(Train &tr){
    // Warning: the only usage currently is when speed is 0, careful
    char bytes[2];
    bytes[1] = (char)tr.get_train_number(); // train number
    // bytes[0] = (tr.get_speed() > 15) ? TRAIN_SPEED_REVERSE + TRAIN_SPEED_LIGHT_OFFSET : TRAIN_SPEED_REVERSE;
    bytes[0] = TRAIN_SPEED_REVERSE + ((tr.lightOn)? TRAIN_SPEED_LIGHT_OFFSET : 0);
    sendCommand(bytes, 2);
    tr.reverse_heading_direction();
}

EExecutionResult TrainSystem::reverseTrainAndRun(const int train_number, bool check_activation_time, bool is_stop){
    // only send command if activation time is earlier
    Train* tr;
    EExecutionResult res = assert_train_state(tr, train_number, check_activation_time);
    if(EExecutionResult::EER_OK != res) { return res;} 

    // this means the train is already set to reverse
    if(tr->delayed_command_reverse_worker_tid != -1){ return EExecutionResult::EER_TrainAlreadyDelayedReversing;}
    
    // if the train reserved tracks,
    // should clear all track reservation and reserve the reverse node
    if(!tr->path_node_buf.is_buf_empty()){
        tr->clear_path_buf();
        tr->clear_reserve(track);
    }
    if(tr->get_last_sensor()){
        tr->path_node_buf.push_front_rbuf_item(Train::PathNode{tr->get_last_sensor()->reverse});
        tr->get_last_sensor()->reverse->setReservedBy(tr->get_train_number());
    }

    // stop the train if needed
    unsigned int speed = tr->get_speed();    // get original speed
    if(!(TRAIN_SPEED_STOP + TRAIN_SPEED_LIGHT_OFFSET == speed || TRAIN_SPEED_STOP == speed)){
        char bytes[2];
        bytes[1] = (char)train_number;       // train number

        // limit speed in [0,15]
        bool lightOn = tr->lightOn;
        if(speed > 15){
            speed -= 16;
        }

        // stop the train and get time required to stop completely
        // making sure speed after reverse will be low to high 
        unsigned int waiting_time_based_on_acc = stopTrain(*tr); 
        waiting_time_based_on_acc = (-1 == waiting_time_based_on_acc)? U_INT_MAX : waiting_time_based_on_acc/10;
        unsigned int waiting_time_based_on_linear_eq = (TRAIN_STOP_TICK_BASE + speed * 4 / 3) * MULTIPLIER_FOR_TRAIN_STOP_TICK;

        // currently it's using a linear function to decide the waiting time
        // wait for the train to stop
        unsigned int activationTick = Time(clock_tid);

        // using acceleration measurements for data we have
        // assume we have the data for all trains
        if( speed == 7 || speed == 12){ // original speed
            activationTick += (waiting_time_based_on_acc < waiting_time_based_on_linear_eq)? waiting_time_based_on_acc : waiting_time_based_on_linear_eq;
        }else{
            // unit in 10 ms, (6 ~ 26) * 21 => train will wait for 1.3 ~ 5.5 s
            activationTick += waiting_time_based_on_linear_eq;
        }

        if(2 == tr->get_train_number()){
            activationTick += SPECIAL_TRAIN_EXTRA_DELAY;
        } else if(74 == tr->get_train_number()){
            activationTick += 2*SPECIAL_TRAIN_EXTRA_DELAY;
        }

        // set activation time
        tr->set_activation_time(activationTick);
      
        // update speed
        if(is_stop){ speed = TRAIN_SPEED_STOP; }

        // should never cause assertion to fail
        delayed_reverse_async(*tr, ((tr->lightOn)? TRAIN_SPEED_LIGHT_OFFSET + speed : speed), activationTick);
    }else{
        reverseTrain(*tr);
    }
    return EExecutionResult::EER_OK;
}

EExecutionResult TrainSystem::reverseTrainAndStop(const int train_number, bool check_activation_time){
    return reverseTrainAndRun(train_number, check_activation_time, true);
}


// ====== other tasks/notifiers/servers and their helper functions/tasks ======

// -------------------------------------
// --- Server : delayed command task ---
// Note: This task is very similar to the clock server

struct DelayedCommand {
    DelayedCommand *next;
    bool used = false;
    unsigned int unblockAtTick;
    char commands[COMMAND_SIZE + 1];
};

// The same as task_clock_notifier
void task_delayed_command_notifier(){
    char msg_buffer[] = "k";
    char reply_buffer[] = "r";
    msg_buffer[0] = TICK_CHAR;
    unsigned int delayed_command_server_tid = MyParentTid();

    while (true) {
        AwaitEvent(Event::EVT_TIMER_TICK);
        Send(delayed_command_server_tid, msg_buffer, 1, reply_buffer, 1);
    }
}

// TODO: consider make this a notifier or refactor it
// if make this notifier, then it will send commands to main task instead of print it
void task_delayed_command_server(){ 
    Queue delayedCommands;
    DelayedCommand dc_buffer[TC_DELAYED_COMMAND_BUF_SIZE];
    
    unsigned int uart_tid = WhoIs(UART_SERVER_NAME);
    unsigned int clock_tid = WhoIs(CLOCK_SERVER_NAME);

    assert(0 < Create(ETP_High, task_delayed_command_notifier));

    // msg format: [(char type), (unsigned int activationTime), (char*)]
    short totalSize = sizeof(char) + sizeof(unsigned int) + COMMAND_SIZE;
    char msg_buffer[totalSize + 1];
    unsigned int curTicks, activationTime;
    int request_tid;
    char type;

    // used to pop from the queue
    DelayedCommand *prevNode, *node;

    while(true){
        Receive(&request_tid, msg_buffer, totalSize);
        type = msg_buffer[0];
        Reply(request_tid, "k", 1);

        if (TICK_CHAR == type){         // check if any element in queue is ready to be poped
            curTicks = Time(clock_tid);
            
            node = (DelayedCommand *)delayedCommands.get_head();
            while(nullptr != node && node->unblockAtTick <= curTicks){
                if('\0' != node->commands[1]){
                    Putstr(uart_tid, COM1, node->commands, 2); // send the commands
                }else{
                    Putc(uart_tid, COM1, node->commands[0]); // send the commands
                }

                node = node->next;

                // pop and reset the node
                prevNode = (DelayedCommand *)delayedCommands.pop();
                prevNode->next = nullptr;
                prevNode->used = false;
            }
        } else if (DELAY_COMMAND_2_CHAR == type) {    // push into the ordered queue            
            // get the activation time
            activationTime = chars2Uint(&msg_buffer[1]);

            DelayedCommand *dc = (DelayedCommand *)getFreeNode4delayedTask((DelayedTask*)dc_buffer);
            dc->unblockAtTick = activationTime;
            memcpy(dc->commands, &msg_buffer[1 + sizeof(unsigned int)], COMMAND_SIZE);
            dc->commands[2] = '\0';
            dc->next = nullptr;

            // put task into ordered link-list
            pushQueueInOrder(delayedCommands, (DelayedTask*)dc);
        } else if (DELAY_COMMAND_1_CHAR == type) {    // push into the ordered queue            
            // get the activation time
            activationTime = chars2Uint(&msg_buffer[1]);

            DelayedCommand *dc = (DelayedCommand *)getFreeNode4delayedTask((DelayedTask*)dc_buffer);
            dc->unblockAtTick = activationTime;
            memcpy(dc->commands, &msg_buffer[1 + sizeof(unsigned int)], 1);
            dc->commands[1] = '\0';
            dc->commands[2] = '\0';
            dc->next = nullptr;

            // put task into ordered link-list
            pushQueueInOrder(delayedCommands, (DelayedTask*)dc);
        } else {
            debugprintf((char*)"\n\rTD char:%c", type);
            assert(false);
        }
    }
}

// ---------------------------------------
// --- Notifier task : Sensor notifier ---

// helpers for sensor data
char encode_sensor(int byte_index, int index) {
    // sensor code from 0 to 79
    return byte_index * 8 + index;
}

// code is in [0, 79], buf is char[4]
void decode_sensor(char code, char buf[]) {
    short number = code % 16 + 1;
    buf[0] = 'A' + code / 16;
    if (number >= 10) {
        buf[1] = '1';
        buf[2] = '0' + number % 10;
    }else{
        buf[1] = '0' + number % 10;
        buf[2] = ' ';
    }
    buf[3] = 0;
}

// -- the task --
// This task will keep requesting for sensor data and send to the train control system
void task_sensor_notifier(){
    // send parent the sensor info
    short train_control_tid = MyParentTid();
    short uart_tid = WhoIs(UART_SERVER_NAME);

    #ifdef SENSOR_MEASUREMENT
        // time measurement
        short clock_tid = WhoIs(CLOCK_SERVER_NAME);
        unsigned int start, end = 0;    // measured in ticks, so it's already not very accurate
        bool is_measurement_printed = false;
    #endif
    char reply_buffer[] = "r";

    // packets for measurement
    char msg_measure1_buffer[6];     // 1 for identifier + 4 bytes for data + 1 saver
    msg_measure1_buffer[0] = (char)ETrainRequests::ETR_SensorMeasurement1;

    char msg_measure2_buffer[6];     // 1 for identifier + 4 bytes for data + 1 saver
    msg_measure2_buffer[0] = (char)ETrainRequests::ETR_SensorMeasurement2;

    // for parsing bytes
    short nbytes, index;
    char get_data, musk;

    // packets for sensor data
    char msg_buffer[3];        // 1 for identifier + 1 byte for data + 1 saver
    msg_buffer[0] = (char)ETrainRequests::ETR_SensorUpdate;

    unsigned char sensor_groups[SENSOR_GET_BUF_SIZE]; // default initialized to 1

    while(true){
        // request for data from train system
        Putc(uart_tid, COM1, SENSOR_DATA_REQUEST);
        
        #ifdef SENSOR_MEASUREMENT
            start = Time(clock_tid);
        #endif
        
        // wait to get data for triggered sensors
        for (nbytes = 0; nbytes < SENSOR_GET_BUF_SIZE; nbytes++){   
            get_data = Getc(uart_tid, COM1);
            // XOR ensures that the sensor changes
            // AND ensures that the sensor changes to 1
            // cite: inspired by Jianyan's soln
            // https://git.uwaterloo.ca/j978li/cs452-a0/-/blob/master/src/pollingloop.c
            unsigned char triggered = (sensor_groups[nbytes] ^ get_data) & get_data;
            sensor_groups[nbytes] = get_data;

            #ifdef SENSOR_MEASUREMENT
                if(!is_measurement_printed && start - end > SENSOR_INTERVAL){
                    end = Time(clock_tid);
                    uint2Chars(&msg_measure1_buffer[1], end - start);
                    Send(train_control_tid, msg_measure1_buffer, 5, reply_buffer, 1);
                    is_measurement_printed = true;
                }
            #endif

            musk = 0b10000000;
            for (index = 0; index < 8; index++){
                if (triggered & musk){
                    msg_buffer[1] = encode_sensor(nbytes, index);
                    Send(train_control_tid, msg_buffer, 2, reply_buffer, 1);
                    
                    // TODO: delete
                    // char sensorData[] = "A16";
                    // decode_sensor(msg_buffer[1], sensorData);
                    // debugprintf("\x1b[18;5f%s", sensorData);
                }
                musk >>= 1;
            }
        }

        #ifdef SENSOR_MEASUREMENT
        if(is_measurement_printed){
            end = Time(clock_tid);
            uint2Chars(&msg_measure2_buffer[1], end - start);
            Send(train_control_tid, msg_measure2_buffer, 5, reply_buffer, 1);
            is_measurement_printed = false;
        }
        #endif

        // reset memory
        Putc(uart_tid, COM1, SENSOR_MEM_RESET);
    }

    // TODO: test only
    // bool flag = 0;
    // while(true){
    //     Delay(clock_tid, 30);
    //     msg_buffer[1] = encode_sensor((flag)?0:6, 4);
    //     Send(train_control_tid, msg_buffer, 2, reply_buffer, 1);
    //     flag = !flag;
    // }
}

// This task will keep requesting train control task for delay, delay, then notify train control task
void task_delay_worker(){
    // send parent the sensor info
    int train_control_tid = MyParentTid();
    int clock_tid = WhoIs(CLOCK_SERVER_NAME);

    char msg_buffer[2];
    char reply_buffer[5];

    while(true){
        // get work
        msg_buffer[0] = (char)ETrainRequests::ETR_WorkerRequestDelay;
        Send(train_control_tid, msg_buffer, 1, reply_buffer, 4);
        int result = DelayUntil(clock_tid, *(int*)reply_buffer); // replay with a tick
        assert(result > -1, (char*)"Delay worker failed");

        // notify work done
        msg_buffer[0] = (char)ETrainRequests::ETR_WorkerDelayDone;
        Send(train_control_tid, msg_buffer, 1, reply_buffer, 4);

    }
}


// -------------------------------------------
// --- Notifier task : wall clock notifier ---

void task_wall_clock_notifier(){
    // notify parent every 10 ticks, (100 ms)
    // current time (unit: 0.1 s, 100 ms)
    short clock_tid = WhoIs(CLOCK_SERVER_NAME);
    short train_control_tid = MyParentTid();

    char msg_buffer[] = "t";
    char reply_buffer[] = "r";
    msg_buffer[0] = (char)ETrainRequests::ETR_ClockUpdate;

    unsigned int curTicks = Time(clock_tid) + 10;
    while(true){
        // 10 ticks is 100 ms
        DelayUntil(clock_tid, curTicks);
        curTicks += 10;
        Send(train_control_tid, msg_buffer, 1, reply_buffer, 1);
    }
}

// --------------------------------------
// --- Notifier task : input notifier ---

void task_input_notifier(){
    char msg_buffer[] = "rr";
    char reply_buffer[] = "r";
    msg_buffer[0] = (char)ETrainRequests::ETR_InputReceived;
    
    short uart_tid = WhoIs(UART_SERVER_NAME);
    short train_control_tid = MyParentTid();

    while (true) {
        msg_buffer[1] = Getc(uart_tid, COM2);
        Send(train_control_tid, msg_buffer, 2, reply_buffer, 1);
    }
}

// ====================================
// ====== Train system functions ======
// ====================================

EExecutionResult TrainSystem::executeCommands(const char* input, const int size){
    if(1 >= size){  // size is always >= 1, so 1 means input empty
        return EExecutionResult::EER_Empty;
    }

    // parse the input
	int init_size = size;
    char str[init_size];
    strcpy(str, (char*)input);

    // get tokens
    char *instruction = strtok(str, " ");
    instruction[strlen(instruction)] = '\0';
    char *value1 = strtok(NULL, " ");
    char *value2 = strtok(NULL, " ");
    char *value3 = strtok(NULL, " ");
    char *value4 = strtok(NULL, " "); // used for format checking

    if(0 == strcmp(instruction, "tr")){
        // check number of arguments
        if(NULL != value1 && NULL != value2 && NULL == value3){     // tr takes 2 arguments
            // check the value/type of arguments
            bool val_good = true;
            int train_number = a2d(value1, val_good);
            int train_speed = a2d(value2, val_good);
            if(!val_good || !inRange(train_number, 1, 80) || !inRange(train_speed, 0, 31)){
                return EExecutionResult::EER_InvalidValues;
            }
            Train* tr  = get_train(train_number);
            if (!tr) {return EExecutionResult::EER_TrainNotFound;}
            if(tr->get_train_state() == Train::State::TravellingToDest){return EExecutionResult::EER_TrainInDT;}
            // send command
            tr->train_stopped(this);
            return trSetTrainSpeed(train_number, train_speed);
        }else{
            return EExecutionResult::EER_UnknownFormat;
        }
    }else if(0 == strcmp(instruction, "re")){
        // re <train_number> <sensor name>
        // check number of arguments
        if(NULL != value1 && NULL != value2 && NULL == value3){     // re takes 2 arguments
            // check the value/type of arguments
            bool val_good = true;
            int train_number = a2d(value1, val_good);
            track_node* sensor_node = track.get_sensor_from_str(value2);
            if(!val_good || !inRange(train_number, 1, 80) || !sensor_node){
                return EExecutionResult::EER_InvalidValues;
            }
            
            Train* tr = get_train(train_number);
            if(!tr){return EExecutionResult::EER_TrainNotFound;}
            if(tr->get_train_state() == Train::State::TravellingToDest){return EExecutionResult::EER_TrainInDT;}
            if(sensor_node->reservedAndNotBy(train_number)){ return EExecutionResult::EER_SensorReserved;}
            if(sensor_node->reverse->reservedAndNotBy(train_number)){ return EExecutionResult::EER_ReverseSensorReserved;}
            unsigned int cur_time = Time(clock_tid)*10;
            unsigned int stop_delay;
            tr->attribute_sensor(this, sensor_node, track, cur_time, stop_delay, true);
        }else{
            return EExecutionResult::EER_UnknownFormat;
        }
    }else if(0 == strcmp(instruction, "dt")){
        // check number of arguments
        if(NULL != value1 && NULL != value2 && (NULL == value3) || (NULL != value3 && NULL == value4)){     // dt takes 3 arguments
            // <train #> <landmark> <offset>
            // check the value/type of arguments
            bool val_good = true;
            int train_number = a2d(value1, val_good);
            track_node* dest_node = track.get_landmark_from_str(value2);
            int offset = (NULL == value3)? 0 : a2d(value3, val_good);

            if(!val_good || !inRange(train_number, 1, 80) || !dest_node){
                return EExecutionResult::EER_InvalidValues;
            }
            Train* tr = get_train(train_number);
            if(!tr){return EExecutionResult::EER_InvalidValues;}
            if(!tr->get_last_sensor()){
                // train is not started, ask for registering a location
                return EExecutionResult::EER_TrainNotStarted;
            }

            // allow changes of destinations, i.e., allow dt during dt
            Location dest(dest_node, offset);
            dest.caliberate();
            
            /*
                * receives dt command
                    * calculate the distance between src node and dest node
                    * get distance d of the total path (begin_offset + dijkstra result + dest offset)
                * get 2 different distances (ideally 4 if we consider reverse)
                * TODO: start checking from the lowest distance (at this moment we first check positive, then reverse dest)
                    * if d is bigger than lowest possible stopping distance + acceleration distance,
                        * then we can accelerate train to highest possible speed that satisfy the contraint
                        * and calculate the stop sensor
                            * special case: already passed stopping sensor: but we are guaranteed to have not passed
                            the stopping location yet, so we can send a delayed stoping command now
                    * TODO: if train is stationary, use hardocde stop command delay to move over this distance
                    * if not, check next distance
            */
            // ignore other instructions if this command is in progress

            // try to find a path
            EExecutionResult eer_res = train_path_planning(tr, dest, false, 0);
            if(EExecutionResult::EER_OK != eer_res){
                eer_res = train_path_planning(tr, dest, true, 0);
            }
            return eer_res;
        }else{
            return EExecutionResult::EER_UnknownFormat;
        }
    }else if(0 == strcmp(instruction, "rv")){
        if(NULL != value1 && NULL == value2){                       // rv takes 1 argument
            // check the value/type of arguments
            bool val_good = true;
            int train_number = a2d(value1, val_good);
            if(!val_good || !inRange(train_number, 1, 80)){
                return EExecutionResult::EER_InvalidValues;
            }
            Train* tr  = get_train(train_number);
            if (!tr) {return EExecutionResult::EER_TrainNotFound;}
            if(tr->get_train_state() == Train::State::TravellingToDest){return EExecutionResult::EER_TrainInDT;}
            // send command
            return reverseTrainAndRun(train_number);
        }else{
            return EExecutionResult::EER_UnknownFormat;
        }
    }else if(0 == strcmp(instruction, "sw")){
        for (auto & tr : trains) {
            if(tr.get_train_state() == Train::State::TravellingToDest){return EExecutionResult::EER_TrainInDT;}
        }
        if(NULL != value1 && NULL != value2 && NULL == value3){     // sw takes 2 arguments
            // check the value/type of arguments
            bool val_good = true;
            int switch_number = a2d(value1, val_good);
            char direction  = value2[0];
            if(!val_good || strlen(value2) != 1 || (direction != 'c' && direction != 's') || switch_number < 1 || 
            (switch_number > 18 && switch_number < 153) || switch_number > 156){
                return EExecutionResult::EER_InvalidValues;
            }

            // send command
            if(!switchTurnout(switch_number, direction)){
                return EExecutionResult::EER_Empty;
            }
        }else{
            return EExecutionResult::EER_UnknownFormat;
        }
    }else if(0 == strcmp(instruction, "q")){
        return (NULL == value1)?                    // quit doesn't have parameters
        EExecutionResult::EER_Quit:
        EExecutionResult::EER_UnknownFormat;
    }else if(0 == strcmp(instruction, "ran")){     // add this train into the random playground
        // <ran> <tr>
        if(NULL != value1 && NULL == value2){
            // check the value/type of arguments
            bool val_good = true;
            int train_number = a2d(value1, val_good);
            if(!val_good || !inRange(train_number, 1, 80)){
                return EExecutionResult::EER_InvalidValues;
            }
            Train* tr  = get_train(train_number);
            if (!tr) {return EExecutionResult::EER_TrainNotFound;}
            if(tr->get_train_state() == Train::State::TravellingToDest){return EExecutionResult::EER_TrainInDT;}
            if(!tr->get_last_sensor()){
                // train is not started, ask for registering a location
                return EExecutionResult::EER_TrainNotStarted;
            }
            // basically dt, but whenever it stops it will automatically choose another destionation

            // here we will only assign it, the logic happens when the clock is updated every 1 second
            tr->set_random_version(Track::RandomVersion::v1);
            tr->set_need_wait_time(true);
            tr->clear_reserve(track);
            if(tr->get_last_sensor()) {tr->get_last_sensor()->setReservedBy(train_number);}
            tr->clear_path_buf();
            tr->train_stopped(this);
        }else{
            return EExecutionResult::EER_UnknownFormat;
        }
    }else if(0 == strcmp(instruction, "ran2")){     // add this train into the random playground
        // <ran> <tr>
        if(NULL != value1 && NULL == value2){
            // check the value/type of arguments
            bool val_good = true;
            int train_number = a2d(value1, val_good);
            if(!val_good || !inRange(train_number, 1, 80)){
                return EExecutionResult::EER_InvalidValues;
            }
            Train* tr  = get_train(train_number);
            if (!tr) {return EExecutionResult::EER_TrainNotFound;}
            if(tr->get_train_state() == Train::State::TravellingToDest){return EExecutionResult::EER_TrainInDT;}
            if(!tr->get_last_sensor()){
                // train is not started, ask for registering a location
                return EExecutionResult::EER_TrainNotStarted;
            }
            // basically dt, but whenever it stops it will automatically choose another destionation

            // here we will only assign it, the logic happens when the clock is updated every 1 second
            tr->set_random_version(Track::RandomVersion::v1);
            tr->set_need_wait_time(true);
            tr->clear_reserve(track);
            if(tr->get_last_sensor()) {tr->get_last_sensor()->setReservedBy(train_number);}
            tr->clear_path_buf();
            tr->train_stopped(this);
        }else{
            return EExecutionResult::EER_UnknownFormat;
        }
    }else if(0 == strcmp(instruction, "ranf")){     // add this train into the random playground
        // <ran> <tr>
        if(NULL != value1 && NULL == value2){
            // check the value/type of arguments
            bool val_good = true;
            int train_number = a2d(value1, val_good);
            if(!val_good || !inRange(train_number, 1, 80)){
                return EExecutionResult::EER_InvalidValues;
            }
            Train* tr  = get_train(train_number);
            if (!tr) {return EExecutionResult::EER_TrainNotFound;}
            if(tr->get_train_state() == Train::State::TravellingToDest){return EExecutionResult::EER_TrainInDT;}
            if(!tr->get_last_sensor()){
                // train is not started, ask for registering a location
                return EExecutionResult::EER_TrainNotStarted;
            }
            // basically dt, but whenever it stops it will automatically choose another destionation

            // here we will only assign it, the logic happens when the clock is updated every 1 second
            tr->set_random_version(Track::RandomVersion::v2);
            tr->set_need_wait_time(true);
            tr->clear_reserve(track);
            if(tr->get_last_sensor()) {tr->get_last_sensor()->setReservedBy(train_number);}
            tr->clear_path_buf();
            tr->train_stopped(this);
        }else{
            return EExecutionResult::EER_UnknownFormat;
        }
    }else if(0 == strcmp(instruction, "udt")){
        // <udt> <tr>
        if(NULL != value1 && NULL == value2){
            // check the value/type of arguments
            bool val_good = true;
            int train_number = a2d(value1, val_good);
            if(!val_good || !inRange(train_number, 1, 80)){
                return EExecutionResult::EER_InvalidValues;
            }
            Train* tr  = get_train(train_number);
            if (!tr) {return EExecutionResult::EER_TrainNotFound;}
            if(tr->get_train_state() == Train::State::TravellingToDest){
                tr->set_train_state(Train::State::StandBy);
            }
        }else{
            return EExecutionResult::EER_UnknownFormat;
        }
    }else if(0 == strcmp(instruction, "stop")){     // stop the train, used to stop a train
        // <stop> <tr>
        if(NULL != value1 && NULL == value2){
            // check the value/type of arguments
            bool val_good = true;
            int train_number = a2d(value1, val_good);
            if(!val_good || !inRange(train_number, 1, 80)){
                return EExecutionResult::EER_InvalidValues;
            }
            Train* tr  = get_train(train_number);
            if (!tr) {return EExecutionResult::EER_TrainNotFound;}
            tr->clear_reserve(track);
            tr->clear_path_buf();
            tr->train_stopped(this);
            tr->path_node_buf.push_front_rbuf_item(Train::PathNode(tr->get_current_estimated_location().get_landmark()));
            tr->get_last_sensor()->setReservedBy(train_number);
            tr->set_random_version(Track::RandomVersion::na);
        }else{
            return EExecutionResult::EER_UnknownFormat;
        }
    }else if(0 == strcmp(instruction, "r")){
        if(NULL != value1 && NULL == value2){                       // r takes 1 argument
            // check the value/type of arguments
            bool val_good = true;
            int train_number = a2d(value1, val_good);
            if(!val_good || !inRange(train_number, 1, 80)){
                return EExecutionResult::EER_InvalidValues;
            }
            Train* tr  = get_train(train_number);
            if (!tr) {return EExecutionResult::EER_TrainNotFound;}
            if(tr->get_train_state() == Train::State::TravellingToDest){return EExecutionResult::EER_TrainInDT;}
            // send command
            return reverseTrainAndStop(train_number);
        }else{
            return EExecutionResult::EER_UnknownFormat;
        }
    }else if(0 == strcmp(instruction, "clr")){     // clear everything about the train, could take the train away after this command
        // <clr> <tr>
        if(NULL != value1 && NULL == value2){
            // check the value/type of arguments
            bool val_good = true;
            int train_number = a2d(value1, val_good);
            if(!val_good || !inRange(train_number, 1, 80)){
                return EExecutionResult::EER_InvalidValues;
            }
            Train* tr  = get_train(train_number);
            if (!tr) {return EExecutionResult::EER_TrainNotFound;}
            tr->clear_reserve(track);
            tr->clear_path_buf();
            tr->train_stopped(this);
            tr->set_random_version(Track::RandomVersion::na);
            tr->clear_location();
        }else{
            return EExecutionResult::EER_UnknownFormat;
        }
    }else{      // unknown instruction
        return EExecutionResult::EER_InvalidInstruction;
    }
    return EExecutionResult::EER_OK;
}

TrainSystem::TrainSystem(): switchCounter(0), is_switch_on(false), delay_workers(delay_work_arr, NUM_DELAY_WORKER) {
    curTicks = 0;
}

void TrainSystem::flushCommands(){
    char msg_buf[] = "m";
    msg_buf[0] = UARTServerProtocol::USP_TRAIN_FLUSH;
    Send(uart_tid, msg_buf, 1, msg_buf, 1);
}

void TrainSystem::initTrainSystem(){
    char track_char = 'e';
    char bytes[2];
    while(!(track_char == 'A' || track_char == 'a' || track_char == 'B'  || track_char == 'b')){
        cprintf(uart_tid, COM2, (char*)"\x1b[HWhich track: A or B?...");
        track_char = Getc(uart_tid, COM2);
    }
    cprintf(uart_tid, COM2, (char*)"\x1b[HInitializing track %c...", track_char);
    
    if (track_char == 'A' || track_char == 'a') {
        track.is_track_a = true;
    } else {
        track.is_track_a = false;
    }
    track.init();

    // seed the trains
    // only doing 79 for now
    unsigned int train_numbers[NUM_TRAINS] = {1, 2, 24, 58, 74, 78};
    for (auto n : train_numbers) {
        Train* tr = get_train(n);
        assert(tr, (char*)"train does not exist");
        tr->set_train_number(n);
        tr->seed_stop_distance();
        tr->seed_velocity();
        tr->seed_accleration();
    }

    // start the train system
    Putc(uart_tid, COM1, TRAIN_SYSTEM_START);

    //  set switches
    for(int i = 1; i <= 18; i++){
        setSwitch( i, 's', false);
    }
    setSwitch(154, 's', false);
    setSwitch(156, 's', false);
    setSwitch(153, 'c', false);
    setSwitch(155, 'c', false);
    turnOffSwitch();

    // set train speed
    // (hard-coded version) set existing trains to speed zero
    bytes[0] = 16; // speed level
    bytes[1] = 1;
    sendCommand(bytes, 2);
    bytes[1] = 2;
    sendCommand(bytes, 2);
    bytes[1] = 24;
    sendCommand(bytes, 2);
    bytes[1] = 58;
    sendCommand(bytes, 2);
    bytes[1] = 74;
    sendCommand(bytes, 2);
    bytes[1] = 78;
    sendCommand(bytes, 2);

    // (Not used) set speed for all possible trains [1, 80]
    // char bytes[2];
    // bytes[0] = 0; // speed level
    // for(int i = 1; i <= 80; i++){
    //     bytes[1] = i;   // train number
    //     sendCommand(bytes, 2);
    // }

    // TODO: could add some code to make sure the direction of the train don't matter
    // i.e., let the train run and use sensors to see its direction


    // wait until the initialization is done
    flushCommands();

    // clear screen (clear "Initializing..."")
    cprintf(uart_tid, COM2, (char*)"\x1b[2J");
}

// -- entry for train control --
void TrainSystem::start(){
    // variables
    char input_buf[TC_INPUT_BUF_SIZE];
    short input_size = 0;
    char input_char;
    EExecutionResult executionState;
    
    // Send - Receive variables
    int sender_tid;
    char msg_buffer[TC_REQUEST_BUF_SIZE + 1];                 // extra space for argument
    ETrainRequests request;

    // for printing the wall clock
	unsigned int time = 0;
    volatile short idle_percentage = 0;
    char timeString[] = "\x1b[s\x1b[H00:00:0   idle:00%%\x1b[u";

    // Sensor - recent sensor buffer
    SensorPrintData sensor_arr[SENSOR_BUF_SIZE];
    RBuf<SensorPrintData> sensorQueue(sensor_arr, SENSOR_BUF_SIZE);
    
    // used to parse sensor sent data
    char sensorData[] = "A16";
    short sensor_loop_index;
    short sensor_print_index;

    unsigned int sensor_measurement;

    // get tid s
    uart_tid = WhoIs(UART_SERVER_NAME);
    assert(0 < uart_tid);
    clock_tid = WhoIs(CLOCK_SERVER_NAME);
    assert(0 < clock_tid);

    // some flags
    bool buf_full = false;

    // clear screen
    cprintf(uart_tid, COM2, (char*)"\x1b[22;1f\x1b[2J");

    // create sub server
    delayed_command_task_tid = Create(ETP_High, task_delayed_command_server);
    assert(0 < delayed_command_task_tid);

    // initialize switches and set train speed
    initTrainSystem();

    // init the clitriggered_sensor
    initUI();
    // print prompt
    cprintf(uart_tid, COM2, (char*)"\x1b[20;1fInput>");

    // create sub notifiers
    wall_clock_notifier_tid = Create(ETP_High, task_wall_clock_notifier);
    assert(0 < wall_clock_notifier_tid);

    sensor_notifier_tid = Create(ETP_High, task_sensor_notifier);
    assert(0 < sensor_notifier_tid);

    input_notifier_tid = Create(ETP_High, task_input_notifier);
    assert(0 < input_notifier_tid);

    for (int count = 0; count < NUM_DELAY_WORKER; count++) {
        assert(0 < Create(ETP_High, task_delay_worker));
    }

    // for track reservation
    unsigned int total_actice_train_count = 0;
    unsigned int total_waiting_train_count = 0;
    unsigned int waiting_count = 0;

    // get input
    while(true){
        Receive(&sender_tid, msg_buffer, TC_REQUEST_BUF_SIZE);
        request = (ETrainRequests)msg_buffer[0];

        switch(request){
        case ETrainRequests::ETR_ClockUpdate:
        {
            Reply(sender_tid, "r", 1);
            time++;
            if(++switchCounter == 10){
                turnOffSwitchNow();
            }

            for(int i = 0; i < NUM_TRAINS; i++){
                trains[i].reserved_num = 0;
            }
            total_actice_train_count = 0;
            total_waiting_train_count = 0;
            ReservedSensorData reserved_nodes_arr[NUM_RESERVABLE_NODES];
            RBuf<ReservedSensorData> reserved_nodes(reserved_nodes_arr, NUM_RESERVABLE_NODES);
            for(int i = 0; i < NUM_RESERVABLE_NODES; i++){
                if(track[i].reservedBy){
                    get_train(track[i].reservedBy)->reserved_num++;
                    reserved_nodes.push_rbuf_item(ReservedSensorData{track[i].name, track[i].reservedBy});
                }
            }

            // refresh train information
            if (0 == time%SENSOR_LOOP_CYCLE_PERIOD) { // update printing every 1 sec, printing eats up resources
                for (int idx = 0; idx < NUM_TRAINS; idx++) {
                    Train* tr = &(trains[idx]);

                    updateTrainStatus(idx, tr, reserved_nodes);   // print info

                    // trains with last sensor means they exist on the track
                    if(tr->get_last_sensor()){ 
                        total_actice_train_count++;

                        switch(tr->get_train_state()){
                            case Train::State::WaitingForOthers:
                                total_waiting_train_count++;
                            case Train::State::TravellingToDest:
                                tr->reserve_track_in_the_front(this);
                                if((!tr->destination_exists()) || 
                                (tr->get_current_estimated_location().get_landmark() && tr->get_current_physical_state_snapshot().location.same_landmark(tr->destination))){
                                    // if destination is reset, stop the train
                                    // or if reached destination, stop (should be optimized)
                                    tr->train_stopped(this);
                                }
                                break;
                            case Train::State::StandBy:
                                {
                                    if(tr->is_random_player()){
                                        // we want the trains to stop for around 2 seconds before setting another destination
                                        if(!tr->is_random_wait_counter_enough()){
                                            // did not wait for 2 second, increment here
                                            tr->increment_random_wait_counetr();
                                        }else{
                                            // randomly choose a destination and assign
                                            track_node* dest_node;
                                            while(true){
                                                Location dest(track.get_random_destination(tr->get_random_version()), 0);
                                                // TODO: currently, we do not allow reverse
                                                if(EExecutionResult::EER_OK == train_path_planning(tr, dest, false, RANDOM_PATH_MIN_LENGTH)){                                                
                                                    // reset counter
                                                    tr->reset_random_wait_counter();
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }

                // TODO: trying to solve the dead-lock situation
                // periodically check for dead lock
                if(total_waiting_train_count == total_actice_train_count){
                    // it's possible that we met a dead-lock situation
                    // all the trains are waiting => dead-lock
                    waiting_count++;
                    // TODO: if doesn't work, try stop all trains
                    // TODO: also, this might only work for random now
                    if(waiting_count >= DEAD_LOCK_CHECK_CYCLE){ // 4*0.4 = 1.6 s, dead-lock for 1.6 second, we fix it
                        // stop all the trains
                        for (int idx = 0; idx < NUM_TRAINS; idx++) {
                            Train* tr = &(trains[idx]);
                            if(tr->get_last_sensor()){ 
                                tr->train_stopped(this);
                            }
                        }
                    }
                }else{
                    waiting_count = 0;
                }
            }

            // set time and print
            timeString[6] = ((time % 36000) / 60 / 100) + '0';
            timeString[7] = ((time % 36000) / 60 % 100 / 10) + '0';
            timeString[9] = ((time % 600) / 100) + '0';
            timeString[10] = ((time % 36000) % 100 / 10) + '0';
            timeString[12] = (time % 10) + '0';

            idle_percentage = enter_kernel(ESwiNumber::ESN_Get_Idle_Time);

            if(idle_percentage >= 10){
                timeString[21] = (idle_percentage / 10) + '0';
                timeString[22] = (idle_percentage % 10) + '0';
                timeString[23] = '%';
                timeString[24] = '%';
            }else{
                timeString[21] = (idle_percentage % 10) + '0';
                timeString[22] = '%';
                timeString[23] = '%';
                timeString[24] = ' ';
            }

            cprintf(uart_tid, COM2, timeString, 28);
            break;
        }
        case ETrainRequests::ETR_InputReceived:
        {
            Reply(sender_tid, "r", 1);
            input_char = msg_buffer[1];
            
            // allow capital letters 
            if(inRange(input_char, 'A', 'Z')){
                input_char = input_char - 'A' + 'a';
            }else{
                // limit possible input
                // The offset can be negative. So user is allowed to type '-'
                if(!('-' == input_char || ' ' == input_char || 8 == input_char || '\r' == input_char || inRange(input_char, '0', '9') || inRange(input_char, 'a', 'z'))){
                    continue;
                }
            }
            
            if ('\r' == input_char){ // end of the input
                // execute the command
                clearPrompt();
                input_buf[input_size++] = '\0';
                executionState = executeCommands(input_buf, input_size);
                switch(executionState){
                case EExecutionResult::EER_Quit:
                    // stop the train system and flush all the train commands before exit
                    char bytes[2];
                    bytes[0] = 0; // speed level
                    bytes[1] = 1;
                    sendCommand(bytes, 2);
                    bytes[1] = 2;
                    sendCommand(bytes, 2);
                    bytes[1] = 24;
                    sendCommand(bytes, 2);
                    bytes[1] = 58;
                    sendCommand(bytes, 2);
                    bytes[1] = 74;
                    sendCommand(bytes, 2);
                    bytes[1] = 78;
                    sendCommand(bytes, 2);
                    Putc(uart_tid, COM1, TRAIN_SYSTEM_STOP); 
                    flushCommands();
                    enter_kernel(ESN_Terminate);
                case EExecutionResult::EER_OK:
                    printPrompt((char*)"Executed");
                    break;
                case EExecutionResult::EER_TrainInDT:
                    // dt command prevents all other commands
                    printPrompt((char*)"DT command is in execution");
                    break;
                case EExecutionResult::EER_UnknownFormat:
                    printPrompt((char*)"Format is wrong");
                    break;
                case EExecutionResult::EER_InvalidInstruction:
                    printPrompt((char*)"Invalid instruction");
                    break;
                case EExecutionResult::EER_InvalidValues:
                    printPrompt((char*)"Invalid arguments");
                    break;
                case EExecutionResult::EER_TrainInProcess:
                    printPrompt((char*)"Train is in process");
                    break;
                case EExecutionResult::EER_TrainNotStarted:
                    printPrompt((char*)"Need to start/register the train first");
                    break;
                case EExecutionResult::EER_PathNotFound:
                    printPrompt((char*)"No feasible path exists");
                    break;
                case EExecutionResult::EER_TrainNotFound:
                    printPrompt((char*)"Train does not exist");
                    break;
                case EExecutionResult::EER_TrainDead:
                    printPrompt((char*)"Train is at dead end, has to reverse! Cannot find a valid middle point.");
                    break;
                case EExecutionResult::EER_SensorReserved:
                    printPrompt((char*)"Sensor is reserved by other trains.");
                    break;
                case EExecutionResult::EER_ReverseSensorReserved:
                    printPrompt((char*)"The reverse sensor is reserved by other trains.");
                    break;
                case EExecutionResult::EER_DestinationSameAsSource:
                    printPrompt((char*)"The destination is too close.");
                    break;
                case EExecutionResult::EER_TrainAlreadyDelayedReversing:
                    printPrompt((char*)"Train is in process of reversing.");
                    break;
                case EExecutionResult::EER_Empty:
                default:
                    break;
                }
                // clean cli and reset buffer
                input_size = 0;
                buf_full = false;
                refreshCli();
            }else{                  // still inputing
                if(8 == input_char){    // backspace - delete
                    if (input_size >= 1){
                        cprintf(uart_tid, COM2, (char*)"\b\x20\b");
                        input_size--;
                        buf_full = false;
                    }
                }else if(input_size < TC_INPUT_BUF_SIZE){
                    input_buf[input_size++] = input_char;
                    Putc(uart_tid, COM2, input_char);
                }else{
                    if(!buf_full){
                        cprintf(uart_tid, COM2, (char*)"\x1b[21;1fMaximal input size reached\x1b[20;%df", 7 + TC_INPUT_BUF_SIZE);
                        buf_full = true;
                    }
                }
            }
            break;
        }
        case ETrainRequests::ETR_SensorUpdate:
        {
            Reply(sender_tid, "r", 1);
            if(sensorQueue.is_buf_full()){
                sensorQueue.pop_rbuf_item();
            }
            // attribute sensor
            track_node* curr_sensor = track.get_sensor_from_num(msg_buffer[1]);
            Train* tr = nullptr;

            unsigned int min_time = UINT32_MAX;
            for (auto& train : trains) {
                /*
                    Sensor attribution strategy right now: 
                        * select trains whose next sensor is the sensor we got
                            * if multiple trains have the correct next sensor, select the one with least time to travel to the sensor
                        * if no trains whose next sensor is the sensor we got, discard it
                */
                Train::PhysicalStateSnapshot s = train.get_current_physical_state_snapshot();
                Location curr_location = s.location;
                if (
                    curr_location.get_landmark() && 
                    curr_location.get_landmark()->next_sensor()  == curr_sensor &&
                    train.estimate_time_ms(curr_location.distance_to_next_sensor()) < min_time){
                    // if multiple train could attribute the same sensor
                    if(nullptr != tr){
                        if(Train::State::StandBy == tr->get_train_state()){
                            // if previous train is standing by, choose this train
                            tr = &train;
                            min_time = tr->estimate_time_ms(curr_location.distance_to_next_sensor()) ;
                        }
                    }else{
                        tr = &train;
                        min_time = tr->estimate_time_ms(curr_location.distance_to_next_sensor()) ;
                    }
                }
            }
            
            // reserved nodes will be given to its owner if cannot decide who is the owner
            if(!tr && curr_sensor->reserved()){
                tr = get_train(curr_sensor->reservedBy);
            }

            if (tr) {
                unsigned int cur_time = Time(clock_tid)*10;
                int time_diff = tr->calc_sensor_trigger_estimation_time_diff_with_actual(cur_time);
                int distance_diff = tr->estimate_distance_mm(time_diff);
                printSensorTriggerPrediction(time_diff, distance_diff);

                #ifdef DEBUG_LOCATION_ESTIMATION
                // use 23 because we are rlready using 22 for DEBUG_LOCATION_ESTIMATION
                Location l = tr->get_current_estimated_location();
                if (l.get_landmark()) {
                    debugprintf("\x1b[23;1f%s %u ToNext: %u Current: %s", l.get_landmark()->name, l.get_offset_mm(), l.get_landmark()->distance_to_next_sensor(), curr_sensor->name);
                }
                #endif  
                unsigned int stop_delay;
                if (tr->attribute_sensor(this, curr_sensor, track, cur_time, stop_delay, false)) {
                    // will delay set train speed
                    delayedStopTrain(*tr, stop_delay, -1);
                }
                #ifdef DEBUG_SENSOR_ATTRIBUTION
                debugprintf("attribute sensor %s to %d", curr_sensor->name, tr->get_train_number());
                #endif
                sensorQueue.push_rbuf_item(SensorPrintData{msg_buffer[1], tr->get_train_number()});
            } else {
                #ifdef DEBUG_SENSOR_ATTRIBUTION
                debugprintf("cannot attribute sensor %s", curr_sensor->name);
                #endif
                sensorQueue.push_rbuf_item(SensorPrintData{msg_buffer[1], -1});
            }

            // print triggered sensor on the screen (always printing from the end)
            #ifdef SENSOR_MEASUREMENT
            sensor_print_index = 4;
            #else
            sensor_print_index = 3;
            #endif
            sensor_loop_index = sensorQueue.get_end() - 1;
            if(sensor_loop_index < sensorQueue.get_start()){
                for(; sensor_loop_index >= 0; sensor_loop_index--, sensor_print_index++){
                    decode_sensor(sensor_arr[sensor_loop_index].sensor, sensorData);
                    cprintf(uart_tid, COM2, (char*)"\x1b[s\x1b[%d;25f%s %d \x1b[u",sensor_print_index, sensorData, sensor_arr[sensor_loop_index].train_num);
                }
                sensor_loop_index = SENSOR_BUF_SIZE - 1;
            }
            for(; sensor_loop_index >= sensorQueue.get_start(); sensor_loop_index--, sensor_print_index++){
                decode_sensor(sensor_arr[sensor_loop_index].sensor, sensorData);
                cprintf(uart_tid, COM2, (char*)"\x1b[s\x1b[%d;25f%s %d \x1b[u",sensor_print_index, sensorData, sensor_arr[sensor_loop_index].train_num);
            }
            break;
        }
        case ETrainRequests::ETR_WorkerRequestDelay:
        {
            delay_workers.push_rbuf_item(sender_tid);
            break;
        }
        case ETrainRequests::ETR_WorkerDelayDone:
        {
            Reply(sender_tid, "r", 1);
            for (auto& tr: trains) {
                if (tr.delayed_command_setspeed_worker_tid == sender_tid) {
                    // set new speed
                    setTrain(tr, tr.delayed_command_setspeed_speed);

                    if(TRAIN_SPEED_STOP == tr.delayed_command_setspeed_speed){
                        // it's a stop command, reset destination when stopped
                        tr.clear_destination();
                    }

                    // reset worker status
                    tr.delayed_command_setspeed_worker_tid = -1;
                    break;
                } else if (tr.delayed_command_reverse_worker_tid == sender_tid) {
                    char bytes[2];
                    bytes[1] = tr.get_train_number();
                    
                    // reverse
                    tr.reverse_heading_direction();
                    bytes[0] = (tr.lightOn)? TRAIN_SPEED_LIGHT_OFFSET + TRAIN_SPEED_REVERSE : TRAIN_SPEED_REVERSE;
                    sendCommand(bytes, 2);
                    
                    // the train should be stopped right now

                    // set train state
                    if(tr.destination_exists()){
                        // if destination exists (i.e., reverse due to collision avoidance)
                        // need to find new path

                        if(EExecutionResult::EER_OK != train_path_planning(&tr, tr.destination, false, 0)){  
                            // if no path found, reverse back and stop
                            stopTrain(tr);
                            // TODO: reverse back
                        }
                    }else{
                        setTrain(tr, tr.delayed_command_reverse_speed);
                    }

                    // reset worker status and reset train state
                    tr.delayed_command_reverse_worker_tid = -1;
                    break;
                }
            }
            break;
        }
        #ifdef SENSOR_MEASUREMENT
            case ETrainRequests::ETR_SensorMeasurement1:
                Reply(sender_tid, "r", 1);
                // print sensor measurement
                sensor_measurement = chars2Uint(&msg_buffer[1]);    // unit in ticks
                cprintf(uart_tid, COM2, (char*)"\x1b[s\x1b[3;42f%d ms \x1b[u", sensor_measurement*10);
                break;
            case ETrainRequests::ETR_SensorMeasurement2:
                Reply(sender_tid, "r", 1);
                // print sensor measurement
                sensor_measurement = chars2Uint(&msg_buffer[1]);    // unit in ticks
                cprintf(uart_tid, COM2, (char*)"\x1b[s\x1b[3;50f%d ms \x1b[u", sensor_measurement*10);
                break;
        #endif
        default:
            break;
        }
    }
}

// -- entry wrapper --
void task_train_control(){
    TrainSystem ts = TrainSystem();
    ts.start();
}


/**
Reserved edges (will continue working on this later)
TODO: in tc2
current granularity:
    - from a sensor to an adjacent sensor
1. A train will be always on the track that's reserved by it
    - Need to occupy front edges (from current sensor to the next sensor)
        - If the track/edges in the front are reserved by other trains, stop immediately
            1. continue running until those edges are not reserved anymore
            2. find another path (at least if the track is not emptied for a while)
        - If the track/edges in the front are not reserved, occupy them
    - Need to un-occupy edges behind (from previous sensor to current sensor)        - done, in attribute sensor
2. Path planning:
    - Path planning should not use edges that are not reserved by the very train
    - Path planing will reserve the tracks for the train automatically
        - need to switch first and then reserve (direction matters)

Question (need to make sure):
- will current implementation 100% make sure the reserved tracks will be un-reserved 
after the train left the track?

First:
- reserve tracks that the train is running on
- stop a train if next track is reserved

Current Implementation:
- A Track Node can set reserved field or clear reserved field
- Clear reserved will be used by the train, triggered every time the train goes across a sensor
- (not active) Set reserved will be used by the train, triggered every time the train goes across a sensor- Clear reserved will be used by the train, triggered every time the train goes across a sensor
- (not active) Set reserved will be used by the path finder to reserve the path




Done:
    - set broken switches to just normal (merge) nodes
    
Path Planning
    - will find two paths to the target location 
        - one to the node as the input
        - one to the reverse node of the node in the input (offset will be handled)
    - if the no path is longer than TC_DT_SAFE_DISTANCE (currently 400cm), then we will set a mid point
    - nevetheless, we will pick a path longer than TC_DT_SAFE_DISTANCE (400cm) to run

Assumptions:
    - we should register a sensor to the train before starting running the train (register command only works for sensors)
        - do tr <train_number> <speed> will also work (as long as a sensor is triggered)
    - if using track a: switch 3 (BR3) should be switched to straight manually
    - if using trakc b: switch 5 (BR5) should be switched to straight manually
    - BR 155 is unreachable on the track
    - BR 156 is always pointing to E2 on both tracks

Issues:
    - accuracy?

test:
1. track b direction to the br 5, should have no path
    if reversed, should be able to find path
2. train does not stop for some reasons, but dt command is finished


Caveats:
1. Need to make sure the broken switches are in the correct direction
2. The last sensor is very important, the direction of the train and the direction of the last sensor must be consistent


- waiting train should reserve nodes
- how exactly do we check when to reserve nodes? and how?


Issue:
- current location is not correct when train is stopped, need it to be the destination node 




*/

