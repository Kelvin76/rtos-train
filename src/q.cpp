#include "q.h"
#include "global.h"
extern "C" {
  // import c libs here
  #include "bwio.h"
}

Queue::Node::Node(Node* next) : next{next} {}

void Queue::Node::set_next(Node* next) {
    this->next = next;
}

Queue::Node* Queue::Node::get_next() {
    return this->next;
}

Queue::Queue() : count{0}, head{nullptr}, tail{nullptr} {}   

void Queue::push(Node* elem) {
    if (this->count == 0) {
        this->head = elem;
        this->tail = elem;
        elem->set_next(nullptr);
    } else {
        this->tail->set_next(elem);
        this->tail = elem;
    }
    this->count++;
}

void Queue::push_front(Node* elem) {
    if (this->count == 0) {
        this->head = elem;
        this->tail = elem;
        elem->set_next(nullptr);
    } else {
        elem->set_next(head);
        this->head = elem;
    }
    this->count++;
}

Queue::Node* Queue::pop() {
    if (count == 0) {
        return nullptr;
    } else {
        Node* ret = this->head;
        this->head = head->get_next();
        if (!head) {
            this->tail = nullptr;
        }
        this->count--;
        return ret;
    }
}

unsigned int Queue::size(){
    return this->count;
}

void Queue::increment_size(){
    this->count += 1;
}

bool Queue::is_empty() {
    return this->count == 0;
}

Queue::Node* Queue::get_head(){
    return this->head;
}

Queue::Node* Queue::get_tail(){
    return this->tail;
}
