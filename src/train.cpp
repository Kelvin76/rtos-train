#include "train.h"
#include "trainControl.h"
#include "track_node.h"
#include "global.h"
#include "math_lib.h"
#include "io_lib.h"

Train::PhysicalStateSnapshot::PhysicalStateSnapshot(): v{0}, a{0}, last_refresh_time_mm{0} {};

Train::NextSensorExpection::NextSensorExpection(): next_sensor{nullptr} {};

Train::Train():
    lightOn{true},
    reserved_num{0},
    waitingOnNode{nullptr},
    path_node_arr{},
    path_node_buf{RBuf<PathNode> (path_node_arr, MAX_PATH_CAPACITY)},
    delayed_command_setspeed_worker_tid{-1},
    delayed_command_reverse_worker_tid{-1},
    speed{0},
    normalized_speed{0},
    granular_speed{GranularSpeed::high},
    activation_time{0},
    state{Train::State::StandBy},
    need_wait{false},
    random_version{Track::RandomVersion::na},
    random_waited_counter{RANDOM_WAIT_TIME},
    heading_direction{true},
    stop_distances{},
    velocities{},
    estimated_time_for_next_sensor{0},
    last_sensor{nullptr},
    last_sensor_set_time{0},
    speed_to_zero_acceleration{}
{
    clock_tid = WhoIs(CLOCK_SERVER_NAME);
}

void Train::set_train_number(int train_number){
    this->train_number = train_number;
}

int Train::get_train_number(){
    return this->train_number;
}

unsigned int Train::set_speed(unsigned int target_speed) {
    unsigned int time_required = 0;
    unsigned int normalized_target_speed = (target_speed > 15)? target_speed - 16 : target_speed;
    if (normalized_target_speed != normalized_speed) {
        if (normalized_speed == 0) {
            update_current_physical_state_snapshot_accleration(zero_to_speed_acceleration[normalized_target_speed].a);
            time_required = get_converged_velocity(normalized_target_speed) * 10000 / zero_to_speed_acceleration[normalized_target_speed].a;
        } else if (normalized_target_speed == 0) {
            update_current_physical_state_snapshot_accleration(speed_to_zero_acceleration[normalized_speed]);
            time_required = get_converged_velocity(normalized_speed) * 10000 / ((-1)*speed_to_zero_acceleration[normalized_speed]);
        } else if (normalized_speed == 7 && normalized_target_speed == 12) {
            update_current_physical_state_snapshot_accleration(a_7_to_12.a);
            time_required = (get_converged_velocity(normalized_target_speed) - get_converged_velocity(normalized_speed)) * 10000 / a_7_to_12.a;
        } else if (normalized_speed == 12 && normalized_target_speed == 7) {
            update_current_physical_state_snapshot_accleration(a_12_to_7.a);
            time_required = (get_converged_velocity(normalized_target_speed) - get_converged_velocity(normalized_speed)) * 10000 / ((-1)*a_12_to_7.a);
        }
    }
    
    if (normalized_target_speed < normalized_speed) { // from a high speed to low speed, granular speed level is high
        granular_speed = GranularSpeed::high;
    } else if (normalized_target_speed > normalized_speed) {
        granular_speed = GranularSpeed::low;
    } // if speed is the same, then do not change granular_speed
    speed = target_speed;
    normalized_speed = normalized_target_speed; // only deal with lights right now
    return time_required;
}

unsigned int Train::get_speed() const{
    return speed;
}

unsigned int Train::get_normalized_speed() const {
    return normalized_speed;
}

Train::GranularSpeed Train::get_granular_speed() {
    return granular_speed;
}

void Train::set_activation_time(unsigned int activation_time) {
    this->activation_time = activation_time;
}

unsigned int Train::get_activation_time() {
    return activation_time;
}

void Train::reverse_heading_direction() {
    heading_direction = !heading_direction;
    // also change the last sensor to be the reverse sensor
    if (last_sensor) last_sensor = last_sensor->reverse;
    reverse_current_physical_state_snapshot_location();
}
bool Train::get_heading_direction() {
    return heading_direction;
}

Train::State Train::get_train_state() {
    return state;
}

void Train::set_train_state(Train::State s) {
    if(state != s){
        state = s;
    }
}

void Train::clear_location(){
    snapshot.location.set_location(nullptr, 0);
}

void Train::refresh_physical_state_snapshot() {
    /*
    We only keep track of most recent acceleration, so we must refresh snapshot everytime 
    acceleration has changed, otherwise we will lose previous state
    If we want to update any other field, eg v, location, we must also refresh, then update,
    otherwise we will lose previous state
    Refrain from calling this function too frequently, because error will accumulate before sensor hit,
    we should never call this directly, only in snapshot update functions like update_current_physical_state_snapshot_accleration()
    */
    get_current_physical_state_snapshot(true);
}

Train::PhysicalStateSnapshot Train::get_current_physical_state_snapshot(bool refresh) {
    // use int for intermediate results because unsigned int will mess up negative values
    if (!snapshot.location.get_landmark()) {
        // don't do anything if we haven't started yet
        return snapshot;
    }
    int distance_travelled = 0;
    int new_acceleration = snapshot.a;
    int curr_time_ms = Time(clock_tid)*10;
    int t = curr_time_ms - snapshot.last_refresh_time_mm; // time since last refresh
    int new_velocity;
    if (snapshot.a != 0) {
        new_velocity = (int) snapshot.v + snapshot.a * t / 10000;
    } else {
        new_velocity = get_converged_velocity(); // due to dynamic caliberation, converged velocity might change
    }

    PhysicalStateSnapshot return_val;

    if ( // velocity converged, should stop accelerating
        (snapshot.a > 0 && new_velocity > (int)get_converged_velocity()) ||
        (snapshot.a < 0 && new_velocity < (int)get_converged_velocity())
        ) {
        new_velocity = get_converged_velocity(); // velocity has converged
        new_acceleration = 0; // do not accelerate after velocity coonverges

        int t_acclerating = ((int)get_converged_velocity() - (int)snapshot.v)*10000/snapshot.a; // not super precise, but should be enough
        int t_rest = t - t_acclerating;

        distance_travelled = 
            // good old D = v*t + 1/2*a*t^2
            // bring everyting up to 8 fixed point
            ((int)(snapshot.v)*t_acclerating + (snapshot.a*t_acclerating/10000*t_acclerating)/2
            +
            // rest of the distance is travelled by constant velocity s = vt
            t_rest*new_velocity)/10000;
            // this shouldn't over flow, largest supported value is 214748 mm
        // if the accleration has changed, we need to refresh
        refresh = true;
    } else {
        // velocity haven't converged yet, or it already converged,
        // in either case the acceleration over the last t ms is constant, we can use good old D = v*t + 1/2*a*t^2
        // current_velocity is 4 fixed point
        // accleeration is 8 fixed point
        distance_travelled = ((int)(snapshot.v)*t + snapshot.a*t/10000*t/2)/10000;
    }

    if ((unsigned int)distance_travelled >= snapshot.location.get_landmark()->distance_to_next_sensor() - snapshot.location.get_offset_mm()) {// should only cross sensor if we have been attributed
        distance_travelled = snapshot.location.get_landmark()->distance_to_next_sensor() - snapshot.location.get_offset_mm() - 1;
        if(distance_travelled < 0){ distance_travelled = 0;}
        // assert(distance_travelled >= 0, (char*)"overflow location");
    }
    return_val.location.set_location(snapshot.location.get_landmark(), snapshot.location.get_offset_mm() + distance_travelled);
    return_val.location.caliberate();
    return_val.v = new_velocity;
    return_val.a = new_acceleration;
    return_val.last_refresh_time_mm = curr_time_ms;
    if (refresh) {
        snapshot.location = return_val.location;
        snapshot.v = new_velocity;
        snapshot.a = new_acceleration;
        snapshot.last_refresh_time_mm = curr_time_ms;
    }
    return return_val;
}

void Train::update_current_physical_state_snapshot_accleration(unsigned int new_accleration) {
    refresh_physical_state_snapshot();
    snapshot.a = new_accleration;
}

void Train::caliberate_current_physical_state_snapshot_location(track_node* lm, int offset_mm) {
    refresh_physical_state_snapshot();
    snapshot.location.set_location(lm, offset_mm);
}

void Train::reverse_current_physical_state_snapshot_location() {
    refresh_physical_state_snapshot();
    if (snapshot.location.get_landmark()) snapshot.location = snapshot.location.get_reverse_location(); // TODO: delay this to the time when the train stops
}

Train::AccelerationMeasurements::AccelerationMeasurements() : a{0}, d{0} {}

void Train::seed_accleration() {
    switch (train_number)
    {
    // TODO: get the actual data for train 2, or never use 2 in demo
    case 2:
    case 1:
    {
        zero_to_speed_acceleration[7].a = 6508;
        zero_to_speed_acceleration[7].d = 183;

        zero_to_speed_acceleration[12].a = 9483;
        zero_to_speed_acceleration[12].d = 1021;

        zero_to_speed_acceleration[14].a = 9610;
        zero_to_speed_acceleration[14].d = 1726;

        speed_to_zero_acceleration[7] = -8500;
        speed_to_zero_acceleration[14] = -12960;
        speed_to_zero_acceleration[12] = -14112;

        a_7_to_12.a = 13129;
        a_7_to_12.d = 683; // we are not considering granular speed, doesn't seem like it will make much differece 

        a_12_to_7.a = -11385;
        a_12_to_7.d = 760;
        break;
    }
    case 24:
    {
        zero_to_speed_acceleration[7].a = 6288;
        zero_to_speed_acceleration[12].a = 11792;

        speed_to_zero_acceleration[7] = -7821;
        speed_to_zero_acceleration[12] = -12474;

        a_7_to_12.a = 15071;
        a_12_to_7.a = -9918;
        break;
    }
    case 58:
    {
        zero_to_speed_acceleration[7].a = 6707;

        zero_to_speed_acceleration[12].a = 9771;

        speed_to_zero_acceleration[7] = -7200;
        speed_to_zero_acceleration[12] = -12380;

        a_7_to_12.a = 10044;
        a_12_to_7.a = -10828;
        break;
    }
    case 74:
    {
        zero_to_speed_acceleration[7].a = 7591;

        zero_to_speed_acceleration[12].a = 9931;

        speed_to_zero_acceleration[7] = -11755;
        speed_to_zero_acceleration[12] = -21835;

        a_7_to_12.a = 22380;
        a_12_to_7.a = -18609;
        break;
    }
    case 78:
    {
        zero_to_speed_acceleration[7].a = 3632;

        zero_to_speed_acceleration[12].a = 5956;

        speed_to_zero_acceleration[7] = -6469;
        speed_to_zero_acceleration[12] = -11765;

        a_7_to_12.a = 6801;
        a_12_to_7.a = -11650;
        break;
    }
    case 79:
    {
        zero_to_speed_acceleration[7].a = 6814;

        zero_to_speed_acceleration[12].a = 11740;

        speed_to_zero_acceleration[7] = -12538;
        speed_to_zero_acceleration[12] = -15417;

        a_7_to_12.a = 9742;

        a_12_to_7.a = -10923;
        break;
    }
    default:
        break;

    }
}

void Train::seed_stop_distance() {
    unsigned int stop_distances_seed[STOP_DISTANCE_ARR_SIZE] = {};
    switch (train_number)
    {
    // TODO: get the actual data for train 2, or never use 2 in demo
    case 2:
    case 1:
    {
        stop_distances_seed[14] = 137;
        stop_distances_seed[15] = 160;

        stop_distances_seed[16] = 220;
        stop_distances_seed[17] = 275;

        stop_distances_seed[18] = 330;
        stop_distances_seed[19] = 395;

        stop_distances_seed[20] = 465;
        stop_distances_seed[21] = 525;

        stop_distances_seed[22] = 615;
        stop_distances_seed[23] = 720;

        stop_distances_seed[24] = 785;
        stop_distances_seed[25] = 910;

        stop_distances_seed[26] = 1010;
        stop_distances_seed[27] = 1155;

        stop_distances_seed[28] = 1280;
        break;
    }
    case 24:
    {
        stop_distances_seed[12] = 107;
        stop_distances_seed[13] = 115;

        stop_distances_seed[14] = 160;
        stop_distances_seed[15] = 165;

        stop_distances_seed[16] = 240;
        stop_distances_seed[17] = 270;

        stop_distances_seed[18] = 325;
        stop_distances_seed[19] = 400;

        stop_distances_seed[20] = 467;
        stop_distances_seed[21] = 520;

        stop_distances_seed[22] = 620;
        stop_distances_seed[23] = 720;

        stop_distances_seed[24] = 820;
        stop_distances_seed[25] = 930;

        stop_distances_seed[26] = 1030;
        stop_distances_seed[27] = 1150;

        stop_distances_seed[28] = 1285;
        break;
    }
    case 58:
    {
        stop_distances_seed[12] = 95;
        stop_distances_seed[13] = 115;

        stop_distances_seed[14] = 145;
        stop_distances_seed[15] = 180;

        stop_distances_seed[16] = 260;
        stop_distances_seed[17] = 245;

        stop_distances_seed[18] = 295;
        stop_distances_seed[19] = 360;

        stop_distances_seed[20] = 415;
        stop_distances_seed[21] = 470;

        stop_distances_seed[22] = 550;
        stop_distances_seed[23] = 660;

        stop_distances_seed[24] = 760;
        stop_distances_seed[25] = 830;

        stop_distances_seed[26] = 935;
        stop_distances_seed[27] = 1055;

        stop_distances_seed[28] = 1245;
        break;
    }
    case 74:
    {
        stop_distances_seed[10] = 275;
        stop_distances_seed[11] = 280;

        stop_distances_seed[12] = 355;
        stop_distances_seed[13] = 340;

        stop_distances_seed[14] = 435;
        stop_distances_seed[15] = 420;

        stop_distances_seed[16] = 485;
        stop_distances_seed[17] = 500;

        stop_distances_seed[18] = 540;
        stop_distances_seed[19] = 555;

        stop_distances_seed[20] = 605;
        stop_distances_seed[21] = 595;

        stop_distances_seed[22] = 680;
        stop_distances_seed[23] = 690;

        stop_distances_seed[24] = 760;
        stop_distances_seed[25] = 760;

        stop_distances_seed[26] = 835;
        stop_distances_seed[27] = 835;

        stop_distances_seed[28] = 840;
        break;
    }
case 78:
    {
        stop_distances_seed[12] = 85;
        stop_distances_seed[13] = 95;

        stop_distances_seed[14] = 120;
        stop_distances_seed[15] = 150;

        stop_distances_seed[16] = 175;
        stop_distances_seed[17] = 200;

        stop_distances_seed[18] = 250;
        stop_distances_seed[19] = 290;

        stop_distances_seed[20] = 330;
        stop_distances_seed[21] = 400;

        stop_distances_seed[22] = 475;
        stop_distances_seed[23] = 530;

        stop_distances_seed[24] = 590;
        stop_distances_seed[25] = 670;

        stop_distances_seed[26] = 750;
        stop_distances_seed[27] = 905;

        stop_distances_seed[28] = 955;
        break;
    }
    case 79:
    {
        stop_distances_seed[10] = 55;
        stop_distances_seed[11] = 70;

        stop_distances_seed[12] = 95;
        stop_distances_seed[13] = 115;

        stop_distances_seed[14] = 160;
        stop_distances_seed[15] = 190;

        stop_distances_seed[16] = 240;
        stop_distances_seed[17] = 280;

        stop_distances_seed[18] = 340;
        stop_distances_seed[19] = 400;

        stop_distances_seed[20] = 460;
        stop_distances_seed[21] = 530;

        stop_distances_seed[22] = 625;
        stop_distances_seed[23] = 710;

        stop_distances_seed[24] = 795;
        stop_distances_seed[25] = 905;

        stop_distances_seed[26] = 1015;
        stop_distances_seed[27] = 1120;

        stop_distances_seed[28] = 1270;
        break;
    }
    default:
        break;
    }

    int it = 0;
    for(auto &sd : stop_distances_seed) {
        stop_distances[it++] = sd;
    }
}

void Train::seed_velocity() {
    unsigned int velocities_seed[STOP_DISTANCE_ARR_SIZE] = {};
    switch (train_number)
    {
    // TODO: get the actual data for train 2, or never use 2 in demo
    case 2:
    case 1:
    {
        velocities_seed[14] = 1526;
        velocities_seed[15] = 1788;

        velocities_seed[16] = 2042;
        velocities_seed[17] = 2293;

        velocities_seed[18] = 2616;
        velocities_seed[19] = 2904;

        velocities_seed[20] = 3199;
        velocities_seed[21] = 3479;

        velocities_seed[22] = 3816;
        velocities_seed[23] = 4167;

        velocities_seed[24] = 4707;
        velocities_seed[25] = 4727;

        velocities_seed[26] = 5109;
        velocities_seed[27] = 5463;

        velocities_seed[28] = 5760;
        break;
    }
    case 24:
    {
        velocities_seed[12] = 1117;
        velocities_seed[13] = 1113;

        velocities_seed[14] = 1582;
        velocities_seed[15] = 1839;

        velocities_seed[16] = 2081;
        velocities_seed[17] = 2361;

        velocities_seed[18] = 2705;
        velocities_seed[19] = 2986;

        velocities_seed[20] = 3263;
        velocities_seed[21] = 3597;

        velocities_seed[22] = 3861;
        velocities_seed[23] = 4280;

        velocities_seed[24] = 4523;
        velocities_seed[25] = 4727;

        velocities_seed[26] = 5192;
        velocities_seed[27] = 5461;

        velocities_seed[28] = 5760;
        break;
    }
    case 58:
    {
        velocities_seed[12] = 1033;
        velocities_seed[13] = 1198;

        velocities_seed[14] = 1445;
        velocities_seed[15] = 1674;

        velocities_seed[16] = 1872;
        velocities_seed[17] = 2183;

        velocities_seed[18] = 2473;
        velocities_seed[19] = 2752;

        velocities_seed[20] = 3044;
        velocities_seed[21] = 3298;

        velocities_seed[22] = 3724;
        velocities_seed[23] = 4008;

        velocities_seed[24] = 4338;
        velocities_seed[25] = 4728;

        velocities_seed[26] = 5029;
        velocities_seed[27] = 5372;

        velocities_seed[28] = 5871;
        break;
    }
    case 74:
    {
        velocities_seed[10] = 2173;
        velocities_seed[11] = 2138;

        velocities_seed[12] = 2637;
        velocities_seed[13] = 2638;

        velocities_seed[14] = 3198;
        velocities_seed[15] = 3176;

        velocities_seed[16] = 3640;
        velocities_seed[17] = 3726;

        velocities_seed[18] = 4225;
        velocities_seed[19] = 4224;

        velocities_seed[20] = 4727;
        velocities_seed[21] = 4728;

        velocities_seed[22] = 5285;
        velocities_seed[23] = 5282;

        velocities_seed[24] = 5761;
        velocities_seed[25] = 5760;

        velocities_seed[26] = 6094;
        velocities_seed[27] = 6220;

        velocities_seed[28] = 6217;
        break;
    }
    case 78:
    {
        velocities_seed[12] = 871;
        velocities_seed[13] = 1073;

        velocities_seed[14] = 1246;
        velocities_seed[15] = 1566;

        velocities_seed[16] = 1683;
        velocities_seed[17] = 1941;

        velocities_seed[18] = 2245;
        velocities_seed[19] = 2398;

        velocities_seed[20] = 2683;
        velocities_seed[21] = 2986;

        velocities_seed[22] = 3167;
        velocities_seed[23] = 3640;

        velocities_seed[24] = 3726;
        velocities_seed[25] = 4062;

        velocities_seed[26] = 4400;
        velocities_seed[27] = 4594;

        velocities_seed[28] = 4948;
        break;
    }
    case 79:
    {
        velocities_seed[10] = 1051;
        velocities_seed[11] = 1336;

        velocities_seed[12] = 1340;
        velocities_seed[13] = 1692;

        velocities_seed[14] = 2003;
        velocities_seed[15] = 2214;

        velocities_seed[16] = 2492;
        velocities_seed[17] = 2803;

        velocities_seed[18] = 3074;
        velocities_seed[19] = 3368;

        velocities_seed[20] = 3683;
        velocities_seed[21] = 4010;

        velocities_seed[22] = 4339;
        velocities_seed[23] = 4730;

        velocities_seed[24] = 4951;
        velocities_seed[25] = 5464;

        velocities_seed[26] = 5763;
        velocities_seed[27] = 6096;

        velocities_seed[28] = 6470;
        break;
    }
    default:
        break;
    }

    int it = 0;
    for(auto &sd : velocities_seed) {
        velocities[it++] = sd;
    }
}

void Train::train_stopped(void* ptr){
    set_train_state(State::StandBy);
    ((TrainSystem*)(ptr))->stopTrain(*this);
    // TODO: should we set current location to be the destination?
    clear_destination();
}

// returns the reserve result and modify the node to point to a proper track node
//      node will become the last of front tracks that needed to be checked
//      or the node / reverse node that's reserved
Train::EReserveResult Train::front_tracks(void* ptr, track_node *&node){
    // first sensor
    int index = path_node_buf.get_start();
    TrainSystem *tc = (TrainSystem*)(ptr);

    #ifdef DEBUG_RESERVATION
    cprintf(5, COM2, "\x1b[s\x1b[23;1fTrain %d path:", train_number);
    do{
        cprintf(5, COM2, "%d:%s ", index, path_node_arr[index].node->name);
    } while(!path_node_buf.get_next_index(index));
    cprintf(5, COM2, "                                      ");
    cprintf(5, COM2, "\x1b[u");
    index = path_node_buf.get_start();
    #endif

    // TODO: delete debuging infor with DEBUG_RESERVATION

    #ifdef DEBUG_RESERVATION
    debugprintf("\x1b[24;1f                                 ");
    debugprintf("\x1b[25;1f                                 ");
    debugprintf("\x1b[26;1f                                 ");
    #endif

    // check at least 3 sensor nodes
    for(int i = 0; i < NUM_SENSOR_LOOK_AHEAD; i++){
        // make sure current node is a sensor
        while (NODE_BRANCH == path_node_arr[index].get_node_type()){
            node = path_node_arr[index].node;
            if(node->notReservedBy(train_number)){
                if(node->reserved()) return EReserveResult::Collision_Same_Direction;
                if(node->reverse->reserved()) return EReserveResult::Collision_Opposing_Direction;
                if(path_node_arr[index].node->current_dir != path_node_arr[index].dir){
                    tc->setSwitch(path_node_arr[index].node->num, (DIR_STRAIGHT == path_node_arr[index].dir)?'s':'c', true);
                }
            }
            if(path_node_buf.get_next_index(index)){ return EReserveResult::No_Collision; }
        }

        // next one will be a sensor
        node = path_node_arr[index].node;
        if(node->notReservedBy(train_number)){
            if(node->reserved()) return EReserveResult::Collision_Same_Direction;
            if(node->reverse->reserved()) return EReserveResult::Collision_Opposing_Direction;
        }
        if(path_node_buf.get_next_index(index)){ return EReserveResult::No_Collision; }
    }
    
    // then check all the path nodes before reaching the stop distance
    int distance = get_stop_distance((0 == normalized_speed)? LOW_SPEED : normalized_speed);

    #ifdef DEBUG_RESERVATION
    debugprintf("\x1b[25;1fnode 1 %d:%s d:%d      ", index, node->name, distance);
    #endif

    while(distance > 0){
        node = path_node_arr[index].node;
        if(node->notReservedBy(train_number)){
            if(node->reserved()) return EReserveResult::Collision_Same_Direction;
            if(node->reverse->reserved()) return EReserveResult::Collision_Opposing_Direction;
        }
        if(NODE_SENSOR == path_node_arr[index].get_node_type()){
            distance -= path_node_arr[index].node->distance_to_next_sensor();
        }else if(NODE_BRANCH == path_node_arr[index].get_node_type()){
            if(path_node_arr[index].node->current_dir != path_node_arr[index].dir){
                tc->setSwitch(path_node_arr[index].node->num, (DIR_STRAIGHT == path_node_arr[index].dir)?'s':'c', true);
            }
        }
        if(path_node_buf.get_next_index(index)){ return EReserveResult::No_Collision; }
    }

    #ifdef DEBUG_RESERVATION
    debugprintf("\x1b[26;1fnode final %d:%s   ", index, node->name);
    #endif

    return EReserveResult::No_Collision;
}

void Train::clear_path_buf(){
    path_node_buf.clear();
}

void Train::clear_reserve(Track &track){
    // cancel the reservations on the previous path
    for(int i = 0; i < NUM_RESERVABLE_NODES; i++){
        if(track[i].reservedBy == train_number){
            track[i].clearReserve();
        }
    }
}

// whenever set to waiting, must stop first
void Train::set_waiting(void* ptr, track_node* node){
    if(destination_exists()){
        waitingOnNode = node;
        ((TrainSystem*)(ptr))->stopTrain(*this);
        set_train_state(State::WaitingForOthers);
    }else{
        waitingOnNode = nullptr;
        train_stopped(ptr);
    }
}

// whenever set to moving, must set state, either stop or moving to the destination
void Train::set_train_moving(void* ptr, int speed){
    if(speed == 0){
        train_stopped(ptr);
    }else{
        ((TrainSystem*)(ptr))->setTrain(*this, speed);
        set_train_state(State::TravellingToDest);
    }
}

void Train::reserve_tracks(void* ptr, track_node* target_node){
    if(!path_node_buf.is_buf_empty()){
        TrainSystem *tc = (TrainSystem*)(ptr);
        int index = path_node_buf.get_start();

        #ifdef DEBUG_RESERVATION
        debugprintf("\x1b[29;1f%d try reserve %s  ", train_number, target_node->name);
        #endif

        do{
            // if(NODE_BRANCH == path_node_arr[index].get_node_type()){
                
            //     #ifdef DEBUG_RESERVATION
            //     debugprintf("\x1b[30;1f%s d:%s    ", path_node_arr[index].node->name, (DIR_STRAIGHT == path_node_arr[index].dir)?"s":"c");
            //     #endif
                
            //     if(path_node_arr[index].node->current_dir != path_node_arr[index].dir){
            //         tc->setSwitch(path_node_arr[index].node->num, (DIR_STRAIGHT == path_node_arr[index].dir)?'s':'c', true);
            //     }
            // }

            if(path_node_arr[index].node->notReservedBy(train_number)){
                if(path_node_arr[index].node->reserved() || path_node_arr[index].node->reverse->reserved()){
                    break;      // track reserved by other trains
                }
                path_node_arr[index].node->setReservedBy(train_number);
            }       
        } while((target_node != path_node_arr[index].node) && (!path_node_buf.get_next_index(index)));

        // reserve the target node
        if(target_node == path_node_arr[index].node && (!target_node->reserved()) && (!target_node->reverse->reserved())){
            target_node->setReservedBy(train_number);
        }

        #ifdef DEBUG_RESERVATION
        debugprintf("\x1b[31;1f%d try reserve %s end  ", train_number, path_node_arr[index].node->name);
        #endif
    }
}

void Train::clear_reserved_track_on_the_back(track_node* sensor){
    while(!path_node_buf.is_buf_empty() && path_node_arr[path_node_buf.get_start()].node != sensor){
        path_node_arr[path_node_buf.get_start()].node->clearReserve();
        path_node_buf.pop();
    }
}

unsigned int Train::time_to_sensor(track_node* target, int &distance, unsigned int speed){
    // pre-condition: sensor should be in the path
    // calculate the distance from current train to the target
    bool found = false;
    if(!path_node_buf.is_buf_empty()){
        int index = path_node_buf.get_start();
        do{ if(path_node_arr[index].node == target){
                found = true;
                break;
            }
            if(NODE_SENSOR == path_node_arr[index].get_node_type()){
                distance += path_node_arr[index].node->distance_to_next_sensor();
            }
        } while(!path_node_buf.get_next_index(index));
    }
    return (found)? estimate_time_ms(distance, speed) : 0;
}

bool Train::will_slow_down_solve_conflicts(Train* other_train, track_node* target, bool sameDirection){
    int distance_other = 4*TRAIN_LENGTH, distance_this = 0;
    unsigned int other_time_consumption, this_time_consumption; 
    // calculate time consumptions
    other_time_consumption = other_train->time_to_sensor((sameDirection)? target : target->reverse, distance_other);
    this_time_consumption = time_to_sensor(target, distance_this, velocities[2*LOW_SPEED + 1]);

    if(other_train->get_train_state() != State::TravellingToDest || other_train->normalized_speed == 0){
        // if the other train is not moving, then if the distance from this train to the target is large enough,
        // then this train could still go
        return (distance_this - distance_other) > MIN_SAFE_DISTANCE_TO_MOVE;   
    }

    return this_time_consumption > other_time_consumption;
}

void Train::reserve_track_in_the_front(void* ptr){
    if(path_node_buf.is_buf_empty()){
        return;
    }

    // Note: could optimize the code in this function, but currently it can list all possible situations in a better way

    TrainSystem *tc = (TrainSystem*)(ptr);
    // look ahead a certain distance
    // distance is at least to the next sensor + stop distance of current speed
    track_node* target_node;
    EReserveResult res = front_tracks(ptr, target_node);

    #ifdef DEBUG_RESERVATION
    debugprintf("\x1b[26;1fTarget Node: %s  ", (target_node)? target_node->name : "None");
    #endif

    bool could_proceed = true, need_new_path = false, reversed = false;
    Train *other_train;
    switch(res){
        // collision with the same direction will slow down or stop
        case EReserveResult::Collision_Same_Direction:
            #ifdef DEBUG_RESERVATION
            debugprintf("\x1b[27;1fSame Collision: %d with %d %s waiting  ", train_number, other_train->train_number, target_node->name);
            #endif
            
            // TODO: make sure this brings no problem
            if(State::TravellingToDest != state){break;}

            other_train = tc->get_train(target_node->reservedBy);   
            could_proceed = will_slow_down_solve_conflicts(other_train, target_node, true);            
            if(could_proceed){
                
                #ifdef DEBUG_RESERVATION
                debugprintf("\x1b[27;1fSame Collision: %d with %d, slow down  ", train_number, other_train->train_number);
                #endif
                
                // slow down the train
                set_train_moving(tc, LOW_SPEED);
            }else{
                
                #ifdef DEBUG_RESERVATION
                debugprintf("\x1b[27;1fSame Collision: %d with %d %s must stop  ", train_number, other_train->train_number, target_node->name);
                #endif
                
                set_waiting(tc, target_node);
                could_proceed = true;
            }
            break;
        // collision with different directions will always stop, if possible, will reverse and find a new path
        case EReserveResult::Collision_Opposing_Direction:

            other_train = tc->get_train(target_node->reverse->reservedBy);  

            #ifdef DEBUG_RESERVATION
            debugprintf("\x1b[28;1f\x1b[2KDiff Collision: %d with %d %s waiting  ", train_number, other_train->train_number, target_node->name);
            #endif

            if(State::TravellingToDest != state){break;}

            // check the state of the other train
            switch(other_train->get_train_state()){
                case State::WaitingForOthers:       // reverse
                case State::StandBy:                // reverse
                case State::RegisteredToStop:       // reverse
                    // if 2 trains are approaching each other, only one train should reach this situation
                    // since the first train getting into this situation will upatde its path and reservation

                    tc->reverseTrainAndStop(train_number, false);
                    break;
                case State::TravellingToDest:       // either stop or reverse
                    // check if have to reverse
                    // a case is that the other train is not coming towards this train
                    // the only possibility is when target_node is a node next to a merge/branch
                    // i.e., traget_node->reverse->next_node() is a branch
                    if((nullptr != target_node->reverse->next_node()) && (NODE_BRANCH == target_node->reverse->next_node()->type)){
                        // track_node* next_sensor = get_current_physical_state_snapshot().location.get_landmark()->next_sensor()->reverse;
                        // could_proceed = will_slow_down_solve_conflicts(other_train, target_node, true);            

                        // if so, will only stop and set to waiting, if the other train is coming, that it should reverse
                        // however, if both train did the same thing, there is a dead-lock
                        // then, we set a rule, the second train that sees a situation like this, will reverse
                        // the pre-condition is simple to check, if the first train is waiting, then this train should reverse
                        
                        // TODO: might want to add more information, e.g., is that train waiting on this train?
                        if(State::WaitingForOthers != other_train->get_train_state()){
                            
                            #ifdef DEBUG_RESERVATION
                            debugprintf("\x1b[28;1fDiff Collision: %d with %d, other is not waiting   ", train_number, other_train->train_number);
                            #endif
                            
                            set_waiting(tc, target_node);
                            break;
                        }
                    }
                    tc->reverseTrainAndStop(train_number, false);
                    break;
                case State::Reversing:              // stop will be enough
                case State::FreeRunning:            // doesn't matter (collision will happen)
                default:

                    #ifdef DEBUG_RESERVATION
                    debugprintf("\x1b[29;1fDiff Collision: %d with %d, other is reversing or free run   ", train_number, other_train->train_number);
                    #endif

                    set_waiting(tc, target_node);
                    break;
            }

            break;
        case EReserveResult::No_Collision:  
            {
                unsigned int path_distance = get_path_distance();
                int safe_speed = -1;
                switch(state){
                    case State::WaitingForOthers:
                    {
                        // pre-condition: the train is stopped

                        #ifdef DEBUG_RESERVATION
                        debugprintf("\x1b[27;1fNo Colli: %d wait  ", train_number);
                        #endif

                        safe_speed = (path_distance > 3*TRAIN_LENGTH)? is_dynamic_stop_possible(path_distance - 3*TRAIN_LENGTH) : -1;
                        if(-1 != safe_speed){
                            // first test if (distance to the destination - a safe distance) is long enough for stop
                            could_proceed = true;
                            need_new_path = false;
                            set_train_moving(tc, safe_speed);
                        }else{
                            // if (distance to the destination - a safe distance) is not long enough
                            if(destination_exists() && destination.get_landmark()->canReserved(train_number) && destination.get_landmark()->reverse->canReserved(train_number)){
                                // if no one is at the destination, test the true distance to the destination
                                safe_speed = is_dynamic_stop_possible(path_distance);
                                if(-1 != safe_speed){
                                    // if the train can safely go there, go
                                    could_proceed = true;
                                    need_new_path = false;
                                    set_train_moving(tc, safe_speed);

                                    // if stop sensor is not in the path (stop sensor is the last sensor), we need to delay stop the train
                                    if(get_current_estimated_location().get_landmark() == stop_sensor.get_landmark()){
                                        tc->delayedStopTrain(*this, estimate_time_ms(stop_sensor.get_offset_mm()), -1);
                                    }
                                }else{
                                    // stop if not enough distance to safely stop at the destination
                                    could_proceed = false;
                                    train_stopped(tc);
                                }
                            }
                        }
                        break;
                    }
                    case State::TravellingToDest:
                    case State::StandBy:
                    {

                        #ifdef DEBUG_RESERVATION
                        debugprintf("\x1b[27;1fNo Colli: %d travel  ", train_number);
                        #endif

                        safe_speed = is_dynamic_stop_possible(path_distance);
                        if(-1 != safe_speed){   // get a higher speed if possible
                            could_proceed = true;
                            need_new_path = false;
                            set_train_moving(tc, safe_speed);
                        }else{
                            could_proceed = false;
                            train_stopped(tc);
                        }
                        break;
                    }
                    default:
                        break;
                }
                break;
            }
        default:
            break;
    }

    if(could_proceed){
        // reserve the track nodes (sensors and branches)
        reserve_tracks(ptr, target_node);
    }else {
        // path is already cleared
        // need to reserve what is under this train
        if(!last_sensor->reserved() && last_sensor->notReservedBy(train_number)){
            if(path_node_arr[path_node_buf.get_start()].node != last_sensor){
                path_node_buf.push_front_rbuf_item(PathNode(last_sensor));
            }
            last_sensor->setReservedBy(train_number); 
        }
    }
}


/*
Some test cases: 

track a:
re 1 b9
re 24 a5
re 58 c7
dt 1 b15
dt 24 b15
dt 58 b15

track b:
re 78 c13
re 58 a1
re 24 a13
dt 58 a3
dt 24 a3
dt 78 a3

cases:
1. same direction: (should be ok)
    - same track, one in front of the other
    - on 2 sides of a branch
    - one on branch, and the other one in front of it
2. diff direction:
    - same track, two trains aim to go to each other's location (good)
        - only one will stop
    - same track, one is stopped (good)
    - one going to the other branch, one is on a merge (good)

currently:
- reverse is not sudden reverse (good)
- reverse does reverse the last sensor and so on (good)
- when reverse, is the state correct? (reversing, good)
- reverse should clear path (good)
- train seeing other train in front (diff direction) will reverse 
    - what I want: only one train reverse (good)
    - did the train that reverses clear the path? un-reserve all nodes in the original direction?
        - if so, I think the other train won't reverse
- train is now set to a speed and re-route after reverse (good)
- diff direction: try to use slow down or stop to avoid (good)
    - currently, if possible, will stop and wait
- if two trains going fast from 2 merges, might collide (good)
    - solution is, we will have regular checks for possible collisions (at every sensor trigger and every 1 second)

Not fixed explicitly (or cannot test all possible scenarios), however, do have some other solutions that will prevent them
- switch turnout could be be problematic, i.e., the front wheels are on the other branch while the branch is switched
    - currently, when planning a path, if the source is a switch, we won't consider directions other than the current direction
    - if a train is stopped before a branch (i.e., halt temporarily), then hopefully (we want to make sure) the switch is already switched
- consider adding emergency stop?
    - we don't have it right now and hopefully don't need it
- sensor attribution
    - since each train running to its destination will reserve nodes, i.e., theoretically, all sensors attributed should be reserved by a train. Then we simply give the sensor to whom reserved it
- switch the turnouts, should switch the turnouts asap
    - not strictly constrained by how far the look ahead of reservations
    - now as long as the switch is not reserved, any train will be able to switch it

- broken tracks
    - might want to set the same layout as other group
        - doesn't really matter, reason is that we will need to check all the broen switches everytime
        - special case: b16 in track a needs to be straight for random playground

TODO:
- dead lock, two trains waiting on each other
    - if we found 2 trains are waiting on each other, reverse one
    - can only happen when reverse is allowed (possible), currently random play should not have this issue
- reservation on reservation


other issues:
- two trains could have the same location (did not solve, but should be fine)
- location reverse when there is a branch (will choose the wrong way)

why does the train state switch between "travel" and "waiting"

special case:
- when finding path in reverse, should we check if it's possible to reach the target without colliding with current existing train?
    - currently, we will allow trains to reverse when there is a path from the reverse starting pont to the destination
    - e.g., 
        - a train blocked the only way to a node
        - another other train wants to go there, so on its way, it will reverse (since path exists)
        - however, whichever path it choose, it will go back to 58

        track b:
        re 58 e7 (standby)
        re 78 d8
        dt 78 a2

        - path exists, but 58 blocks the only way, so 78 will repeat doing the same thing forever
        - a solution is to check if the train blocking the path will be on the new path
        - however, this won't solve the issue (consider having two trains blocking one path)
        - another "solution" is for random play ground, since all nodes are in a cycle (i.e., at least 2 ways to the same location)

*/


bool Train::attribute_sensor(void* ptr, track_node* sensor, Track& track, unsigned int time_mm, unsigned int& delay, bool is_register) {
    /*
        returns whether we need to stop after this sensor (to stop at destination)
    */
    if (last_sensor && last_sensor != sensor) dynamic_caliberation(last_sensor, sensor, track, time_mm - last_sensor_set_time);
    last_sensor = sensor;
    last_sensor_set_time = time_mm;
    caliberate_current_physical_state_snapshot_location(sensor, 0); // Always do this after we finishing dynamic caliberation to get latest data

    // clear reservation
    clear_reserved_track_on_the_back(sensor);
    // special case
    if(State::FreeRunning == state){    // reserve tracks for tr command
        path_node_buf.push_front_rbuf_item(PathNode(sensor));
    }
    if(is_register){
        clear_path_buf();
        clear_reserve(track);
        train_stopped(ptr);
        path_node_buf.push_front_rbuf_item(PathNode(sensor));
    }
    // future reservation
    reserve_track_in_the_front(ptr);

    estimate_time_for_next_sensor(sensor->distance_to_next_sensor());
    // determine if the train should stop
    if (state == State::TravellingToDest && last_sensor == stop_sensor.get_landmark()) {
        delay = estimate_time_ms(stop_sensor.get_offset_mm());
        set_train_state(State::RegisteredToStop);
        return true;
    }
    return false;
}

track_node* Train::get_last_sensor() {
    return last_sensor;
}

unsigned int Train::get_last_sensor_set_time() {
    return last_sensor_set_time;
}

void Train::set_detination(Location &loc){
    #ifdef DEBUG_DESTINATION
    debugprintf((char *)"\x1b[25;1fset destination: %d %s", train_number, loc.get_landmark()->name);
    #endif
    destination.set_location(loc);
};

void Train::clear_destination(){
    #ifdef DEBUG_DESTINATION
    debugprintf((char *)"\x1b[26;1f%d %s, reset", train_number, (destination_exists())? destination.get_landmark()->name: "None");
    #endif
    destination.clear();
};

unsigned int Train::estimate_time_ms(unsigned int distance_mm, unsigned int speed) {
    /*
    how long in ms it takes for train to travel given distance in mm
    */
    PhysicalStateSnapshot s = get_current_physical_state_snapshot();

    speed = (U_INT_MAX == speed) ? get_converged_velocity() : speed;
    if(speed == 0){ speed = 1;}

    if (s.a == 0) {
        // not accleratiing === velocity converges
        return distance_mm*10000/speed;
    }
    // how much time needed for us to acclerate to constant velocity
    int time_to_constant_v = ((int)speed - (int)s.v)*10000/s.a;

    unsigned int distance_to_constant_v = ((int)s.v*time_to_constant_v + ((s.a*time_to_constant_v/10000*time_to_constant_v)>>1))/10000;

    if (distance_to_constant_v > distance_mm) {
        // travels distance_mm without reaching constant v
        int a = 1;
        int b = 2*(int)s.v*10000/s.a; //2v/a
        int c = -2*100000000/s.a*(int)distance_mm;  // -2d/a
        int result = math_lib::solve_quadratic_formula(a, b, c);
        #ifdef DEBUG_SOLVE_ROOT
        debugprintf("Solve root returns %d for distance %u, a %d v %u ", result, distance_mm, s.a, s.v);
        #endif
        return (result == -1)? distance_mm*10000/s.v: result; // if solve root fails, fall back to default
    } else {
        return time_to_constant_v + (distance_mm - distance_to_constant_v)*10000/speed;
    }

}

int Train::estimate_distance_mm(int time_diff_ms) {
    /*
    How much distance in mm this train can travel in given time in ms
    This is only used for sensor estimation, so we are using a very rough estimation
    */
    return time_diff_ms*(int)get_converged_velocity()/10000;
}

int Train::calc_sensor_trigger_estimation_time_diff_with_actual(unsigned int cur_time) {
    /*
        We estimated the time it takes to trigger next sensor: t_predicted
        this function is called when the next sensor is actually triggered at cur_time
        we calculate the difference between t_actual and t_redicted
    */
    int actual_time_passed_mm = cur_time - get_last_sensor_set_time();
    return actual_time_passed_mm - get_estimated_time_for_next_sensor();
}

unsigned int Train::get_estimated_time_for_next_sensor() {
    return estimated_time_for_next_sensor;
}

void Train::estimate_time_for_next_sensor(unsigned int next_sensor_dist) {
    estimated_time_for_next_sensor = estimate_time_ms(next_sensor_dist);
}

unsigned int Train::get_converged_velocity() const {
    return velocities[normalized_speed*2+((granular_speed == GranularSpeed::low)? 0 : 1)];
}

unsigned int Train::get_converged_velocity(unsigned int new_normalized_speed) const {
    if (new_normalized_speed == normalized_speed) {
        return get_converged_velocity();
    } else if (new_normalized_speed > normalized_speed) {
        // change to a higher speed
        return velocities[new_normalized_speed*2];
    } else {
        // change to a lower speed
        return velocities[new_normalized_speed*2 + 1];
    }
}

unsigned int Train::get_stop_distance() {
    return stop_distances[normalized_speed*2+((granular_speed == GranularSpeed::low)? 0 : 1)];
}

unsigned int Train::get_stop_distance(unsigned int new_normalized_speed) {
    /*
    What stop distance will be if we change to this new speed
    */
    if (new_normalized_speed == normalized_speed) {
        return get_stop_distance();
    } else if (new_normalized_speed > normalized_speed) {
        // change to a higher speed
        return stop_distances[new_normalized_speed*2];
    } else {
        // change to a lower speed
        return stop_distances[new_normalized_speed*2 + 1];
    }
}

unsigned int Train::get_accelerate_distance(unsigned int new_normalized_speed) {
    /*
    What stop distance we need to accelerate to new_normalized_speed
    */
    assert(new_normalized_speed == LOW_SPEED || new_normalized_speed == HI_SPEED, (char*)"Speed level not supported");
    int a = 0;
    int target_v = get_converged_velocity(new_normalized_speed);
    int current_v = get_current_physical_state_snapshot().v;
    if (normalized_speed == 0) {
        if (new_normalized_speed == LOW_SPEED) {
            a = zero_to_speed_acceleration[LOW_SPEED].a;
        } else if (new_normalized_speed == HI_SPEED) {
            a = zero_to_speed_acceleration[HI_SPEED].a;
        }
    } else if (normalized_speed == LOW_SPEED){
        if (new_normalized_speed == LOW_SPEED) {
            a = get_current_physical_state_snapshot().a;
        } else if (new_normalized_speed == HI_SPEED) {
            a = a_7_to_12.a;
        }
    } else if (normalized_speed == HI_SPEED) {
        if (new_normalized_speed == LOW_SPEED) {
            a = a_12_to_7.a;
        } else if (new_normalized_speed == HI_SPEED) {
            a = get_current_physical_state_snapshot().a;
        }
    }

    if (a == 0) return 0; // our current a is 0 and we are changing to the same speed
    int t = (target_v - current_v)*10000/a;
    return (current_v*t + a*t/10000*t/2)/10000;
}

int Train::is_dynamic_stop_possible(unsigned int d) {
    // if it is possible for us to calculate the delay of stop command on the fly (instead of using hardcoded values)
    // prefer higher speed, if hi speed doesn't work, try lower speed
    if (get_stop_distance(HI_SPEED) + get_accelerate_distance(HI_SPEED) <= d) {
        return HI_SPEED;
    } else if (get_stop_distance(LOW_SPEED) +  get_accelerate_distance(LOW_SPEED) <= d) {
        return LOW_SPEED;
    }
    return -1;
}

int Train::get_max_speed_for_distance(unsigned int distance) {
    // get max speed that allows us stop accurately for this distance
    // right now assume train is stationary
    for (int s = TOP_NORMALIZED_SPEED; s >= LOWEST_FUNCTIONAL_SPEED; s--) {
        if ((get_stop_distance(s) + get_accelerate_distance(s)) <= distance) {

            return s;
        }
    }
    assert(false, (char*)"is_dynamic_stop_possible and get_max_speed_for_distance are not synchronized");
    return 0;
}

void Train::stop_at(Location stop_sensor) {
    this->stop_sensor = stop_sensor;
    state = State::TravellingToDest;
}

unsigned int Train::get_path_distance(){
    unsigned int distance = 0;
    // distance of the path
    if(!path_node_buf.is_buf_empty()){
        int index = path_node_buf.get_start();
        do{ if(NODE_BRANCH == path_node_arr[index].get_node_type()){
                if(path_node_buf.get_next_index(index)){ break; }
            }else{
                distance += path_node_arr[index].node->distance_to_next_sensor();
            }
        }while(!path_node_buf.get_next_index(index));
        distance = (distance < TRAIN_LENGTH)? 0: distance - TRAIN_LENGTH;
    }

    #ifdef DEBUG_RESERVATION
    debugprintf("\n\nDistance: %u", distance);
    #endif

    return distance;
}

int Train::path_safe_speed(int dist_off){
    return is_dynamic_stop_possible(get_path_distance() - dist_off);
}

Location Train::get_current_estimated_location() {
    return get_current_physical_state_snapshot().location;
}

void Train::dynamic_caliberation(track_node* last_sensor, track_node* this_sensor, const Track& track, unsigned int time_passed_mm) {
   if (
        snapshot.a != 0
    ) {
        /*
        Don't update velocity if we are accelerating (implies that a speed level change has happened)
        */
        return;
    }
    
    /*
    calculate and update velocity, to do this, we need distance between the two sensors
    one way to do that is call last_sensor->distance_to_next_sensor()
    however, it may not work in some edge cases where the switch between these two sensors
    changes right before we get a read from this_sensor, then distance_to_next_sensor will get 
    the distance from last_sensor to some different sensor
    if we want to do this super correctly, we would use bfs or dfs to find all neighbour sensors                distance += path_node_arr[index].node->distance_to_next_sensor(path_node_arr[index].dir);
    of last_sensor and keep track of distance, this is what I am doing now using track.distance_to_next_sensor
    */
    unsigned int distance = track.distance_to_next_sensor(last_sensor, this_sensor);
    unsigned int estimated_vel = distance*10000/time_passed_mm;
    unsigned int actual_vel = get_converged_velocity();
    if (estimated_vel <= 2*actual_vel && estimated_vel >= actual_vel/2) {
        velocities[normalized_speed*2+((granular_speed == GranularSpeed::low)? 0 : 1)] = (actual_vel + estimated_vel) >> 1;
    }
}
