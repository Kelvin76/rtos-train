#include "glob.h"
#include "global.h"
#include "task_lib.h"
#include "ts7200.h"
#include <string.h>
#include <io_lib.h>
#include "td.h"
#include "memconf.h"
#include "timer.h"
#include "interrupt.h"
#include "RBuf.h"
#include "trainControl.h"

// ---- kernel functions ----

TD *schedule()
{

#ifdef DEBUG_TASK
    for (int priority_num = 0; priority_num < NUM_PRIORITIES; priority_num++)
    {
        bwprintf(COM2, (char *)"[DEBUG\tscheduler] Priority %d, size: %d\n\r", priority_num, ready_queue_by_priority[priority_num].size());
    }
#endif

    /*
    schedule a new task from highest priority ready queue
    if no task is ready, return nullptr
    */

    for (int priority_num = 0; priority_num < NUM_PRIORITIES; priority_num++)
    {
        // search from high (0) to low (NUM_PRIORITIES - 1) priorities
        if (!ready_queue_by_priority[priority_num].is_empty())
        {
            return (TD *)ready_queue_by_priority[priority_num].pop();
        }
    }

    return nullptr;
}

void k_unblock_event_queue(Event event) {
    while (!g_event_queue_by_event[event].is_empty()) {
        auto td = (TD*)g_event_queue_by_event[event].pop();
        td->state = EState::ES_Ready;
        k_make_ready(td);
    }
}

extern "C" {
    /*
    if is_swi, value1 is swi number, value 2 is undefined
    if not is_swi, then it is interrupt, value1 is vic1 status, value2 is vic2 status
    */
void k_main_loop(int value1, int value2, bool is_swi)
{
#ifdef DEBUG_TASK
    bwprintf(COM2, (char *)"[Debug] Kernel Enter, value1 %d, is_swi: %d\r\n", value1, is_swi);
#endif

    if (g_cur_task_td && is_swi) // what is changed here?
    {
        switch ((ESwiNumber)value1){
        case ESwiNumber::ESN_Interrupt: // 1
        default:
            break;
        case ESwiNumber::ESN_MyTid:     // 2
            k_MyTid();
            break;
        case ESwiNumber::ESN_MyParentTid: // 3
            k_MyParentTid();
            break;
        case ESwiNumber::ESN_Schedule:  // 4
            k_make_ready(g_cur_task_td);
            break;
        case ESwiNumber::ESN_Exit:      // 5
            {   
                // do not add back to ready queue
                // activate senders blocked on this receiver (clean send queue and reply queue)
                TD* blockedSender;
                while(!g_cur_task_td->senders_blocked_on_Receive.is_empty()){
                    blockedSender = (TD*)g_cur_task_td->senders_blocked_on_Receive.pop();
                    blockedSender->state = EState::ES_ReplyBlocked;
                    k_Reply(blockedSender->tid, (char*)"Q", 1);
                    k_save_return_value(-2, blockedSender);         // transaction failed
                }
                g_cur_task_td->tid = -1;    // so the task cannot be found by other tasks
    #ifdef DEBUG_TASK
                bwprintf(COM2, (char *)"[Debug] Task %d exited with priority %d.\r\n", g_cur_task_td->tid, g_cur_task_td->priority);
    #endif
                break;
            }
        case ESwiNumber::ESN_Destroy:   // 6
            // TODO: later assignments
            break;
        case ESwiNumber::ESN_Create:    // 7
            {
                // get the address where we stored sp, then dereference it to get value of
                // saved user sp
                u32 *user_sp = (u32 *)*(g_cur_task_td->sp + SP_OFFSET - 2);
                // we pushed priority, then function
                // so we pop function first
                void (*function)() = (void (*)()) * user_sp;
                user_sp += 1;
                int priority = (int)*user_sp;
                k_Create(priority, function);
                break;
            }
        case ESwiNumber::ESN_Send:      // 8
            {
                SendParams* p = (SendParams*)*(g_cur_task_td->sp);
                k_Send(p->tid, p->msg, p->msglen, p->reply, p->replylen);             
                break;
            }
        case ESwiNumber::ESN_Receive:   // 9
            {
                ReceiveParams* p = (ReceiveParams*)*(g_cur_task_td->sp);
                k_Receive(p->tid, p->msg, p->msglen);             
                break;
            }
        case ESwiNumber::ESN_Reply:     // 10
            {
                ReplyParams* p = (ReplyParams*)*(g_cur_task_td->sp);
                k_Reply(p->tid, p->reply, p->rplen);             
                break;
            }
        case ESwiNumber::ESN_Await_Event:   // 11
            {
                k_AwaitEvent(*(int*)(g_cur_task_td->sp));         
                break;
            }
        case ESwiNumber::ESN_Read_Timer:    // 12
            {
                k_save_return_value(k_ReadTimer());
                k_make_ready(g_cur_task_td);
                break;
            }
        case ESwiNumber::ESN_Read_Ticks:    // 13
            {
                k_save_return_value(g_ticks);
                k_make_ready(g_cur_task_td);
                break;
            }
        case ESwiNumber::ESN_Delay_task:    // 14
            {
                k_make_ready(g_cur_task_td);
                break;
            }
        case ESwiNumber::ESN_Get_Idle_Time: // 15
            {
                k_save_return_value(g_idle_task_running_time/(k_ReadTimer() / 100));
                k_make_ready(g_cur_task_td);
                break;
            }
        case ESwiNumber::ESN_Terminate: // 16
            {
                // be a good citizen and clean up after ourself
                auto reg = (volatile int *)(VIC1_BASE + VIC_INT_ENABLE_OFFSET);
                *reg = 0; // disable vic 
                reg = (volatile int *)(VIC2_BASE + VIC_INT_ENABLE_OFFSET);
                *reg = 0; // disable vic 
                auto line = (int *)(TIMER1_BASE + CRTL_OFFSET); // disable timer
                *line &= ~ENABLE_MASK;
                disable_uart_tx_interrupt(COM2);
                disable_uart_tx_interrupt(COM1);
                disable_uart_1_CTS_interrupt();
                disable_uart_1_rx_interrupt();

                _my_exit();
                break;
            }
        }
    } else if (!is_swi) { // HW interrupt handler
        k_make_ready(g_cur_task_td);
        if (value1 & IRQ_STATUS_TC1UI) { // tc1 underflow
            // confirm interupt
            volatile int* timer_clear_register = (volatile int*) (TIMER1_BASE + CLR_OFFSET);
            *timer_clear_register = 1; // write anything will do
            // increment tick counter
            g_ticks++;
            // unblock tasks waiting on this
            k_unblock_event_queue(Event::EVT_TIMER_TICK);
        } else if (value2 & INT_UART2_MASK) { //uart 2 combined
            volatile unsigned int * uart2_int_id_reg = (volatile unsigned int *) (UART2_BASE + UART_INTR_OFFSET);
            if (*uart2_int_id_reg & RTIS_MASK) {
                disable_uart_2_timeout(); // uart server will turn it back on
                k_unblock_event_queue(Event::EVT_UART2_TIMEOUT);
            }
            if (*uart2_int_id_reg & TIS_MASK) { // uart 2 tx ready
                disable_uart_tx_interrupt(COM2);
                k_unblock_event_queue(Event::EVT_UART2_TX);
            }  
        } else if (value2 & INT_UART1_MASK) { //uart 1 combined
            volatile unsigned int * uart1_int_id_reg = (volatile unsigned int *) (UART1_BASE + UART_INTR_OFFSET);
            if (*uart1_int_id_reg & TIS_MASK) { // uart 1 tx ready
                disable_uart_tx_interrupt(COM1);
                k_unblock_event_queue(Event::EVT_UART1_TX);
            }
            if (*uart1_int_id_reg & MIS_MASK) { // uart 1 CTS changed
                disable_uart_1_CTS_interrupt();
                k_unblock_event_queue(Event::EVT_UART1_CTS);
            }
            if (*uart1_int_id_reg & RIS_MASK) { // uart 1 rx interrupt
                disable_uart_1_rx_interrupt();
                k_unblock_event_queue(Event::EVT_UART1_RX);
            }
        }
    }

    if (g_cur_task_td->tid == g_idle_task_tid) {
        g_idle_task_running_time += k_ReadTimer() - g_idle_task_start_at;
    }
    // schedule the next task
    g_cur_task_td = schedule();

    // leave kernel if a task is scheduled
    if (g_cur_task_td)
    {
        g_cur_space = g_cur_task_td->sp;
        if (g_cur_task_td->tid == g_idle_task_tid) {
            g_idle_task_start_at = k_ReadTimer();
        }

#ifdef DEBUG_TASK
        bwprintf(COM2, (char *)"[Debug] Schedule, next task: %d\r\n", g_cur_task_td->tid);
#endif
        leave_kernel();
    } else {
        // no task is ready
        // see https://student.cs.uwaterloo.ca/~cs452/S22/assignments/kernel.html No Ready Tasks

#if defined(DEBUG_TASK) || defined(DEBUG_TASK_AE)
        bwprintf(COM2, (char *)"[Debug] Program exits\r\n");
#endif

        // however, since we are not blocking any task (at this moment), we should terminate kernel here
        asm volatile("LDR pc, [%0]"
                       :
                       : "r"(&redboot_return_addr));
    }

    bwprintf(COM2, (char *)"[Exception] Unreachable Kernel\r\n");
}
}

// add a svm before this, save registers to stack
// save sp to r0
// branch to svc_handler
// svc handler read 0 as sp, save registers on stack to space
// save sp with an offset
asm(R"(
    .text
    .align 2
    .global	svc_handler
    .type svc_handler, %function
    svc_handler:
    // -- 1. save context
    // figure out if we are triggered by interrupt
    mrs sp, cpsr
    and sp, sp, #0b11111  // get the last 5 digits representing processor mode 
    cmp sp, #0b10010       // comapre with irq mode
    bne save_context        // TODO: test this doesn't override user stack

    // we are in irq mode
    sub lr, lr, #4

    save_context:
    ldr sp, =g_cur_space   // sp is banked, so we can safely override this
    ldr sp, [sp]
    // sp now has the address where we store r0

    // save r0 to r14_user
    STMIA sp, {r0-r14}^
    // save svc lr as pc
    str lr, [sp, #60]
    // save psr
    // since r0 is already saved, we can override it
    mrs r0, spsr
    STR r0, [sp, #64]

    //restore sp
    LDR sp, =g_kernel_sp
    ldr sp, [sp]

    // -- 2. prepare k_main_loop() args
    // are we triggered by interrupt?
    mrs r0, cpsr
    and r0, r0, #0b11111  // get the last 5 digits representing processor mode 
    cmp r0, #0b10010       // comapre with irq mode
    bne get_swi_num

    // if triggered by interrupt
    ldr r0, =0b11010011 // change to svc mode
    msr cpsr, r0
    //restore sp again after changing mode
    LDR sp, =g_kernel_sp
    ldr sp, [sp]
    ldr r0, =0x800B0000 // vic1 status register
    ldr r0, [r0]
    ldr r1, =0x800C0000 // vic2 status register
    ldr r1, [r1]
    MOV r2, #0
    b k_main_loop

    // if not triggered by interrupt
    // get last instruction
    get_swi_num:
    LDR r0, =g_cur_space
    ldr r0, [r0]        // r0 = address we store r0
    ldr r0, [r0, #60]   //r0 =  pc 
    LDR r0, [r0, #-4]   // r0 = swi instr
    AND r0, r0, #0xffffff // get swi number
    MOV r2, #1
    b k_main_loop
)");

int enter_kernel(ESwiNumber n)
{
    int res = 0;
    switch (n)
    {
    default:
        asm volatile("SWI 0");
        break;
    case ESwiNumber::ESN_Interrupt:
        asm volatile("SWI 1");
        break;
    case ESwiNumber::ESN_MyTid:
        asm volatile("SWI 2");
        break;
    case ESwiNumber::ESN_MyParentTid:
        asm volatile("SWI 3");
        break;
    case ESwiNumber::ESN_Schedule:
        asm volatile("SWI 4");
        break;
    case ESwiNumber::ESN_Exit:
        asm volatile("SWI 5");
        break;
    case ESwiNumber::ESN_Destroy:
        asm volatile("SWI 6");
        break;
    case ESwiNumber::ESN_Read_Timer:
        asm volatile("SWI 12");
        break;
    case ESwiNumber::ESN_Read_Ticks:
        asm volatile("SWI 13");
        break;
    case ESwiNumber::ESN_Get_Idle_Time:
        asm volatile("SWI 15");
        break;
    case ESwiNumber::ESN_Terminate:
        asm volatile("SWI 16");
        break;
    }
    asm volatile("MOV %[result], r0"
                   : [result] "=r"(res));
    return res;
}


asm(R"(
    .text
    .align 2
    .global	leave_kernel
    .type leave_kernel, %function
    leave_kernel:

    ldr r12, =g_cur_space
    ldr r12, [r12]
    // r12 now has the address where we store r0

    ldr r1, [r12, #64] //load saved msr to r1
    msr spsr, r1       // load saved msr to spsr register

    add r1, r12, #52 //load address where we save saved r13 to r1
    LDMIA r1, {r13, r14}^ // load user banked registers

    LDMIA r12, {r0-r15}^ // load saved value to registers, note that this is LDM (3), which automatically moves spsr to cpsr
)");

// kernel helper functions

void k_save_return_value(unsigned int val, TD* td)
{
    // save kernel function result
    // to where r0 would be saved
    // if g_cur_task_td is null, we are initialized by kernel
    // then we don't need to return anything
    if (!td){
        td = g_cur_task_td; // default save to current td
    } 
    if (td) // it is still possible that curr td is null (inited by kernel)
    {
        *td->sp = val;
    }
}

// this kernel function actually returns (in contrast to other k_ functions that return void)
// because this function is called by kernel directly during initialization as well
// so only return to task r0 is not enough
int k_Create(int priority, void (*function)())
{
    // invalid priority
    if (priority < 0)
    {
        k_save_return_value(-1);
        return -1;
    }

    // get a slab
    void *addr = slab_allocator.get_slab();

    #ifdef TRAIN_SYSTEM_ON
        // special case: give train control system double the size
        if((u32)function == (u32)task_train_control){
            addr = slab_allocator.get_slab();          
        }
    #endif

    // not enough slabs left
    if (nullptr == addr)
    {
        k_save_return_value(-2);
        return -2;
    }

    if( task_uart_server == function ){
        g_uart_server_tid = tid_counter;
    }

    // init task descriptor
    TD *td = (TD *)((char *)addr + SLAB_SIZE - sizeof(TD));
    u32 *sp = (u32 *)(td)-2;
    // sp is now on top of TD, grow up (from High addr to Low addr)
    // sp now points to psr value, i.e., (u32*)td - 2
    *sp = 0x60000050; // default psr for user mode, enable interrupt
    sp -= 1;          // sp points to pc value
    TD tmp(tid_counter++, nullptr, priority, sp - SP_OFFSET);
    *td = tmp;

    // set parent if exists
    if (nullptr != g_cur_task_td)
    {
        td->parent = g_cur_task_td;
    }

    // init exception stack
    *(sp) = (u32)function;              // pc value
    *(sp - 1) = (u32)&Exit;             // lr value set to Exit()
    *(sp - 2) = (int)(sp - SP_OFFSET);  // stack value for R13

    g_task_pool[(u32)g_task_pool_size++] = td; // push into the task pool

    // push into priority queue (priority >= 0)
    ready_queue_by_priority[priority].push((Queue::Node *)td);

#if defined(DEBUG_MEM) || defined(DEBUG_TASK)
    bwprintf(COM2, (char *)"[Debug-Create] func addr: %d; mem start: %u, end: %u; td: %u, sp: %u, tid: %d, pri: %d\r\n", function, addr, (u32)addr + SLAB_SIZE, td, td->sp, td->tid, priority);
#endif
    if (g_cur_task_td) {
        // if called by another task, make it ready again
        k_make_ready(g_cur_task_td);
    }
    k_save_return_value(td->tid);
    return td->tid;
}

void k_make_ready(TD* td) {
    if (td->tid != -1) { // make ready only if task haven't exited
        ready_queue_by_priority[td->priority].push((Queue::Node *)td);
    }
}

// return task id of the calling task
void k_MyTid()
{
    if (nullptr != g_cur_task_td)
    {
        k_save_return_value(g_cur_task_td->tid);
    }
    else
    {
        // unreachable code
        bwprintf(COM2, (char *)"[Exception] Unreachable: cur task non-exist when MyTid is called \n\r");
        k_save_return_value(-1);
    }
    k_make_ready(g_cur_task_td);
}

// return task id of the parent for the calling task, return -1 if parent does not exist
void k_MyParentTid()
{
    if (nullptr != g_cur_task_td)
    {
        k_save_return_value((nullptr != g_cur_task_td->parent) ? g_cur_task_td->parent->tid : -1);
    }
    else
    {
        // unreachable code
        bwprintf(COM2, (char *)"[Exception] Unreachable: cur task non-exist when MyParentTid is called \n\r");
        k_save_return_value(-2);
    }
    k_make_ready(g_cur_task_td);
}

TD* k_find_td_by_tid(int tid) {
    if(tid >= 1){
        for(const auto &td : g_task_pool) {
            if (td->tid == tid){
                return td;
            }
        }
    }
    return nullptr;
}

/*
Message passing stuff
TODO: separate this into a diff file in the future
TODO: is this TODO done?
see https://student.cs.uwaterloo.ca/~cs452/S22/assignments/kernel.html
    Send-blocked. The task has executed Receive(), and is waiting for a task to sent to it.
    Receive-blocked. The task has executed Send(), and is waiting for the message to be received.
    Reply-blocked. The task has executed Send() and its message has been received, but it has not received a reply.
    Event-blocked. The task has executed AwaitEvent(), but the event on which it is waiting has not occurred. 
*/

/*
k_Send
Send a msg to another task and receives a reply.
TODO: use memcpy
return:
>-1	the size of the message returned by the replying task. The actual reply is less than or equal to the size of the reply buffer provided for it. Longer replies are truncated.
-1	tid is not the task id of an existing task.
-2	send-receive-reply transaction could not be completed. 
*/
void k_Send(int tid, const char *msg, int msglen, char *reply, int replylen) {
    auto receiver = k_find_td_by_tid(tid);
    if (!receiver || receiver == g_cur_task_td) {
        // receiver does not exist or self
        k_save_return_value(-1);
        k_make_ready(g_cur_task_td);
    }else if (receiver->state != EState::ES_SendBlocked) {
        // sender first
        g_cur_task_td->state = EState::ES_ReceiveBlocked;
        // put this on block queue for receive
        receiver->senders_blocked_on_Receive.push((Queue::Node*)g_cur_task_td);

        // do not put this back to ready queue
        // so it will not get scheduled
    } else {
        // receiver already blocked
        assert(receiver->state == EState::ES_SendBlocked);
        // unblock receiver
        receiver->state = EState::ES_Ready;
        k_make_ready(receiver);
        // block sender, do not add back to ready queue
        g_cur_task_td->state = EState::ES_ReplyBlocked;

        // copy data
        ReceiveParams* receive_params = (ReceiveParams*)*(receiver->sp);
        int* tid = receive_params->tid;
        *tid = g_cur_task_td->tid;
        int receiver_msg_len = receive_params->msglen;
        char* receiver_buffer = receive_params->msg;
        {
            int i = 0;
            for (; i < receiver_msg_len && i < msglen; i++) {
                receiver_buffer[i] = msg[i];
            }
            receiver_buffer[i] = '\0';
        }
        // set up receiver return value
        k_save_return_value(msglen, receiver);
    }
    // no need to save_return value, reply() will do it
}

asm(R"(
    .text
    .align 2
    .global	_Send
    .type _Send, %function
    _Send:
    swi 8
    bx lr
)");

int Send(int tid, const char *msg, int msglen, char *reply, int replylen) {
    // what we are doing here is to pack all arguments into a struct and pass the ddress of struct r0
    // which will be saved to location pointed by g_curr_task->sp
    // so in kernel function we can obtaine these arguments
    SendParams p = {.tid=tid, .msg = msg, .msglen=msglen, .reply=reply, .replylen=replylen};
    int res = _Send(&p);
    return res;
}

/*
https://student.cs.uwaterloo.ca/~cs452/S22/assignments/kernel.html
k_Receive
receive a message

Return Value
>-1	the size of the message sent by the sender (stored in tid). 
The actual message is less than or equal to the size of the message buffer supplied. Longer messages are truncated.  
*/

void k_Receive(int *tid, char *msg, int msglen) {
    auto sender = (TD*)g_cur_task_td->senders_blocked_on_Receive.pop();
    if (sender) {
        // sender first scenario
        assert(sender->state == EState::ES_ReceiveBlocked);
        sender->state = EState::ES_ReplyBlocked;

        // copy data
        *tid = sender->tid;

        SendParams* send_params = (SendParams*)*(sender->sp);
        int sender_msg_len = send_params->msglen;

        // data transfer (copying data)
        {
            int i = 0;
            for (; i < sender_msg_len && i < msglen; i++) {
                msg[i] = send_params->msg[i];
            }
            msg[i] = '\0';
        }

        // make sure receiver is still ready! it needs to unblock sender
        k_make_ready(g_cur_task_td);
        // see https://student.cs.uwaterloo.ca/~cs452/S22/assignments/kernel.html
        // the size of the message sent by the sender 
        // not the actual msg
        k_save_return_value(send_params->msglen);
    } else {
        // receiver first
        // no sender found
        // blocked by send
        // do not add back to ready queue
        g_cur_task_td->state = EState::ES_SendBlocked;
    }
}

asm(R"(
    .text
    .align 2
    .global	_Receive
    .type _Receive, %function
    _Receive:
    swi 9
    bx lr
)");

int Receive(int *tid, char *msg, int msglen){
    ReceiveParams p = {.tid=tid, .msg=msg, .msglen=msglen};
    int res = _Receive(&p);
    return res;
}

/*
https://student.cs.uwaterloo.ca/~cs452/S22/assignments/kernel.html
k_Reply
reply 

Return Value
>-1	the size of the reply message transmitted to the original sender task.
If this is less than the size of the reply message, the message has been truncated.
-1	tid is not the task id of an existing task.
-2	tid is not the task id of a reply-blocked task.
*/

void k_Reply(int tid, const char *reply, int rplen) {
    TD* sender = k_find_td_by_tid(tid);
    if (!sender) {
        k_save_return_value(-1);
        k_make_ready(g_cur_task_td);
        return;
    }
    if (sender->state != EState::ES_ReplyBlocked) {
        k_save_return_value(-2);
        k_make_ready(g_cur_task_td);
        return;
    }

    // copy data
    SendParams* send_params = (SendParams*)*(sender->sp);
    int sender_len = send_params->replylen;
    char* sender_buffer = send_params->reply;
    {
        int index = 0;
        for (; index < sender_len && index < rplen; index++) {
            sender_buffer[index] = reply[index];
        }
        sender_buffer[index] = '\0';
    }
    // sender gets reply length
    k_save_return_value(rplen, sender);

    // unblock sender
    // must do this before unblocking receiver
    // because "If they are of the same priority, the sender runs first."
    sender->state = EState::ES_Ready;
    k_make_ready(sender);

    // receiver is still ready
    k_make_ready(g_cur_task_td);
    // receiver gets actual reply length
    k_save_return_value((rplen <= sender_len) ? rplen : sender_len);
}

asm(R"(
    .text
    .align 2
    .global	_Reply
    .type _Reply, %function
    _Reply:
    swi 10
    bx lr
)");

int Reply(int tid, const char *reply, int rplen){
    ReplyParams p = {.tid=tid, .reply=reply, .rplen=rplen};
    int res = _Reply(&p);
    return res;
}

// to disableCache, just reset arm box
asm(R"(
    .text
    .align 2
    .global	disableCache
    .type disableCache, %function
    disableCache:
    //MRC p15, 0, R0, c7, c10, 0      // clean d-cache
    //ORR r0, r0, #0x2                // set second bit to 1
    //ORR r0, r0, #0x800              // set 12th bit to 1
    //MCR p15, 0, r0, , <CRm>{, <opcode2>}
    bx lr
)");

// enableCache
asm(R"(
    .text
    .align 2
    .global	enableCache
    .type enableCache, %function
    enableCache:
    MCR p15, 0, r0, c7, c6, 0       // invalidate d cache
    MCR p15, 0, r0, c7, c5, 0       // invalidate i cache
    MRC p15, 0, r0, c1, c0, 0       // enable both cache
    ORR r0, r0, #0x1 << 2           // set second bit to 1
    ORR r0, r0, #0x1 << 12          // set 12th bit to 1
    MCR p15, 0, r0, c1, c0, 0
    bx lr
)");

/*
set up interrupt handler
    * disable interrupt in kernel/irq mode (done?) it seems like it is disabled by default, need to double check
    * enable interrupt in user mode (done)
    * switch to user mode after restoring registers! (done)
    * enable VIC interrupt (done)
    * make sure irq registers (sp) is correct (inspect) -- we are not using this
    * setup interrupt handler in vic (change address in 0x38) (done)
    * interrupt handler save return address and maybe some other stuff (done)
    * interrupt handler somehow traps into kernel (done)
    * confirm timer interrupt (done)
set up timer so that it sends interrupt every 10ms (done)
timer tick event has a queue
when kernel receives timer interupt, check this queue and unblock all tasks?

*/
// blocks until the event identified by eventid occurs then returns with volatile data, if any
void k_AwaitEvent(int eventType){
    if (eventType == Event::EVT_UART1_CTS) {
        enable_uart_1_CTS_interrupt();
        // to avoid await after interrupt
    }
    switch (eventType)
    {
    case Event::EVT_TIMER_TICK:
    case Event::EVT_UART2_TIMEOUT:
    case Event::EVT_UART2_TX:
    case Event::EVT_UART1_TX:
    case Event::EVT_UART1_CTS:
    case Event::EVT_UART1_RX:
        g_cur_task_td->state = EState::ES_EventBlocked;
        g_event_queue_by_event[eventType].push((Queue::Node*)g_cur_task_td);
        // do not add back to ready queue
        break;
    default:
        k_save_return_value(-1);
        // don't block if event invalid
        k_make_ready(g_cur_task_td);
        break;
    }
}

// _AwaitEvent
asm(R"(
    .text
    .align 2
    .global	AwaitEvent
    .type AwaitEvent, %function
    AwaitEvent:
    swi 11
    bx lr
)");


unsigned int k_ReadTimer() {
    volatile unsigned int *line = (volatile unsigned int *)(TIMER3_BASE + VAL_OFFSET);
    return TIMER_MAX - (*line);
}

// ---- user level functions ----

// Create a task
// use actual asm code (instead of inline asm)
// because we are dealing with sp directly
// and I don't wanna mess it up
// ref: https://en.cppreference.com/w/cpp/language/asm
// ref: https://developer.arm.com/documentation/100748/0616/Using-Assembly-and-Intrinsics-in-C-or-C---Code/Calling-assembly-functions-from-C-and-C--
asm(R"(
    .text
    .align 2
    .global	Create
    .type Create, %function
    Create:
    str r0, [sp, #-4]! // push parameters to stack
    str r1, [sp, #-4]!
    swi 7
    add sp, sp, #8 // pop stack
    // r0 is already restored to return value in leave_kernel()
    // according to ABI, we don't need to do anything!
    // the caller will interpret r0 as return value
    bx lr
)");

// return task id of the calling task
int MyTid()
{
    return enter_kernel(ESwiNumber::ESN_MyTid);
}

// return task id of the parent for the calling task, return -1 if parent does not exist
int MyParentTid()
{
    return enter_kernel(ESwiNumber::ESN_MyParentTid);
}

// give up the processor
void Yield()
{
    enter_kernel(ESwiNumber::ESN_Schedule);
}

// quit current task
void Exit()
{
    #if defined(DEBUG_TASK_IDLE) && (!defined(TRAIN_SYSTEM_ON))
        printf(COM2, (char*)"Idle: %d%%\n\r", enter_kernel(ESwiNumber::ESN_Get_Idle_Time));
    #endif
    enter_kernel(ESwiNumber::ESN_Exit);
}

// delete current task (Exit() + give up mem slab + delete td)
void Destroy()
{
    // TODO: later assignments

    // Call Exit() and reclaim mem and td
}

// read current timer 3 value
unsigned int ReadTimer(){
    return enter_kernel(ESwiNumber::ESN_Read_Timer);
}

// read time in microseconds
unsigned int ReadTimerUS(){
    return ReadTimer() * 49 / 25 ;  // 508kHz is around 1.9685 microseconds for each tick
}

// returns a number in [low, high]
unsigned int rand(int low, int high){
    return ReadTimer() % (1 + high - low) + low;
}

// read the number of ticks so far
unsigned int ReadTicks(){
    return enter_kernel(ESwiNumber::ESN_Read_Ticks);
}

// ---- servers ----

// TODO: We can make servers classes

// name server

/*
if name already exists, update.
protocol:
	Send this string <type char><name<\0>>
	- for type char, see marco REGISTER_CHAR and WHO_IS_CHAR
		used to specify whether calling registeras or whois

*/

void task_name_server() {
	NameTidMapping name_mappings[NAME_SERVER_BUFFER_SIZE];
	int max_exclusive_index = 0;
    int request_tid, len;
    char msg_buffer[TASK_NAME_LENGTH+2]; // extra space for argument
    char type;

	#ifdef DEBUG_TASK_NS
	bwprintf(COM2, (char*)"Name server initializing\r\n");
	#endif

	while (true) {
        len = Receive(&request_tid, msg_buffer, TASK_NAME_LENGTH+1);
        // protocol: first char specifies register/whois call
        type = msg_buffer[0];

		#ifdef DEBUG_TASK_NS
		bwprintf(COM2, (char*)"=== Name server received request type %c ===\r\n", type);
		for (int i = 0; i < max_exclusive_index; i++) {
			bwprintf(COM2, (char*)"-name: ");
			bwprintf(COM2, name_mappings[i].name);
			bwprintf(COM2, (char*)" tid: ");
			bwprintf(COM2, (char*)"%d\r\n", name_mappings[i].tid);
		}
		#endif
		if (type == REGISTER_CHAR) {
			// register() will make sure a null char exists at the end
			bool update = false;
			for (int i = 0; i < max_exclusive_index; i++) {
				if (strcmp(name_mappings[i].name, msg_buffer + 1) == 0) {
					// msg_buffer + 1 because first char is argument
					// string already exists
					// update existing entry
					#ifdef DEBUG_TASK_NS
					bwprintf(COM2, (char*)"String already exists, updating\r\n");
					#endif
					update = true;
					name_mappings[i].tid = request_tid;
				#ifdef DEBUG_TASK_NS
					bwprintf(COM2, (char*)"=== Name server updated mapping: ");
					bwprintf(COM2, name_mappings[i].name);
					bwprintf(COM2, (char*)"=%d ===\r\n", name_mappings[i].tid);
				#endif
					break;
				}
			}

			if (!update) {
				// no name collision
				// add a new item in array
				assert(len <= TASK_NAME_LENGTH+1);

				for(int idx = 1; idx < len; idx++) {
					name_mappings[max_exclusive_index].name[idx-1] = msg_buffer[idx];
				}
				name_mappings[max_exclusive_index].tid = request_tid;

				#ifdef DEBUG_TASK_NS
				bwprintf(COM2, (char*)"=== Name server registered mapping: ");
				bwprintf(COM2, name_mappings[max_exclusive_index].name);
				bwprintf(COM2, (char*)"=%d ===\r\n", name_mappings[max_exclusive_index].tid);
				#endif

				max_exclusive_index++;
			}
			Reply(request_tid, msg_buffer, 1); // reply msg is not used currently
		} else if (type == WHO_IS_CHAR) {
			#ifdef DEBUG_TASK_NS
			bwprintf(COM2, (char*)"who is\r\n");
			#endif
			int found_tid = -1;
			for (int i = 0; i < max_exclusive_index; i++) {
				if (strcmp(name_mappings[i].name, msg_buffer + 1) == 0) {
					found_tid = name_mappings[i].tid;
					#ifdef DEBUG_TASK_NS
					bwprintf(COM2, (char*)"found %d\r\n", found_tid);
					#endif
				}
			}

			#ifdef DEBUG_TASK_NS
			bwprintf(COM2, (char*)"=== Name server who is send reply ===\r\n");
			#endif
			Reply(request_tid, (const char*)&found_tid, 4);
		} else {
            bwprintf(COM2, (char*)"Name server received invalid request.\r\n");
			assert(false);
		}
	}
}

/*
    truncate if name exceeds max length
    must include null character
    return -1 when nameserver tid is bad
    return 0 if success
*/
int RegisterAs(const char *name) {
    int cursor = 0;
    // TODO: make TASK_NAME_LENGTH the actual length of input name
    char msg_buffer[TASK_NAME_LENGTH+1]; // extra space for argument

    msg_buffer[cursor++] = REGISTER_CHAR;

    while (name[cursor-1] != '\0' && cursor < TASK_NAME_LENGTH) {
        // only copy the regular characters && at most copy 9 regular characters
        // eg: 012345678\0 will copy 012345678 and append \0
        //  0123456789\0 will copy 012345678 and append \0
        // then append \0 to the string
        msg_buffer[cursor] = name[cursor-1]; // idx start with 1, so subtract one here
        cursor++;
    }
    msg_buffer[cursor] = '\0';

    char reply_buffer[] = "r"; // we are not using this

    #ifdef DEBUG_TASK_NS
    bwprintf(COM2, (char*)"RegisterAs msg buffer: ");
    bwprintf(COM2, msg_buffer);
    bwprintf(COM2, (char*)"\r\n");
    #endif

    int result = Send(name_server_tid, msg_buffer, cursor+1, reply_buffer, 1);

    if(result == -1){
        #ifdef DEBUG_TASK_NS
        bwprintf(COM2, (char*)"RegisterAs failed\n\r");
        #endif
        return -1;
    }
    return 0;
}

/*
    must include null character
    return -1 nameserver tid does not exist
    return -2 if doesn't exist
    should return tid normally 
*/
int WhoIs(const char *name) {
    int cursor = 0;
    char msg_buffer[TASK_NAME_LENGTH+1]; // extra space for argument

    msg_buffer[cursor++] = WHO_IS_CHAR;

    while (name[cursor-1] != '\0' && cursor < TASK_NAME_LENGTH) {
        // same as RegisterAs
        msg_buffer[cursor] = name[cursor-1]; // idx start with 1, so subtract one here
        cursor++;
    }
    msg_buffer[cursor] = '\0';

    char reply_buffer[] = "1234";       // int is 4 bytes

    #ifdef DEBUG_TASK_NS
    bwprintf(COM2, (char*)"WhoIs msg buffer: ");
    bwprintf(COM2, msg_buffer);
    bwprintf(COM2, (char*)"\r\n");
    #endif

    int result = Send(name_server_tid, msg_buffer, cursor+1, reply_buffer, 4);

    if (result == -1){
        return -1;
    }if (*(int*)reply_buffer == -1){
        return -2;
    }
    return *(int*)reply_buffer;
}


// notifies the clock server whenver a tick is triggered, can only be created by the clock server
void task_clock_notifier(){
    char msg_buffer[] = "k";
    char reply_buffer[] = "r";
    msg_buffer[0] = TICK_CHAR;
    unsigned int clock_server_tid = MyParentTid();

    while (true) {
        AwaitEvent(Event::EVT_TIMER_TICK);
        Send(clock_server_tid, msg_buffer, 1, reply_buffer, 1);
    }
}

// helper functions for clock server

/**
    @param dt includes the unblock time described as a number of ticks (from the creation of the clock server)
*/
// push a DelayedTask struct into an ordered queue
int pushQueueInOrder(Queue &q, DelayedTask *dt){
    if (q.is_empty() || ((DelayedTask *)q.get_tail())->unblockAtTick <= dt->unblockAtTick){              
        // empty queue or the unblock time of the last element is less (or equal)
        q.push((Queue::Node*)dt);
    } else if(((DelayedTask *)q.get_head())->unblockAtTick >= dt->unblockAtTick){
        // the unblock time of the the first element is greater (or equal)
        q.push_front((Queue::Node*)dt);
    } else {
        // otherwise, we find the corresponding place for the new element and insert it
        DelayedTask* node = (DelayedTask *)q.get_head();

        while(nullptr != node->next){
            if (node->next->unblockAtTick >= dt->unblockAtTick){
                dt->next = node->next;
                node->next = dt;
                q.increment_size();
                break;
            }
            node = node->next;
        }
    }
    return 0;
}

// reply to the elements that can be unblocked (tick number <= curTicks)
int replyQueueInOrder(Queue &q, unsigned int curNTicks){
    DelayedTask* prevNode, *node = (DelayedTask *)q.get_head();

    while(nullptr != node && node->unblockAtTick <= curNTicks){
        Reply(node->tid, (const char*)&curNTicks, 4);
        node = node->next;

        // pop and reset the node
        prevNode = (DelayedTask *)q.pop();
        prevNode->next = nullptr;
        prevNode->used = false;
    }

    return 0;
}

// get a node
DelayedTask* getFreeNode4delayedTask(DelayedTask dt_buffer[]){
    for (int index = 0; index < NUM_TASK; index++){
        if(false == dt_buffer[index].used){
            dt_buffer[index].used = true;
            return &dt_buffer[index];
        }
    }
    bwprintf(COM2, (char*)"[Exception] out of empty space for dt nodes.\n\r");
    assert(0);
    return nullptr;
}

// TODO: could use memcpy or something later instead of hardcoding
void uint2Chars(char* cs, unsigned int num){
    for(int i = 0; i < 4; i++){
        cs[i] = (char)(num % 256);
        num /= 256;
    }
}

unsigned int chars2Uint(char* cs){
    unsigned int res = 0;
    for(int i = 3; i >= 0; i--){
        res = res*256 + (unsigned int)(cs[i]);
    }
    return res;
}

// clock server, one clock tick is 10ms
void task_clock_server(){

    int response = 1;
    while(response){            // make sure register is successful
        response = RegisterAs(CLOCK_SERVER_NAME);
    }

    Create(ETP_Highest, task_clock_notifier);   // tid: 9 in k3

    unsigned int nTicks = 0, delayNTicks;
    int request_tid, reply;
    char msg_buffer[6];                 // extra space for argument
    char volatile type;

    Queue delayedTasks;
    DelayedTask dt_buffer[NUM_TASK];    // used to assign memory for the queue

    #ifdef DEBUG_TASK_AE
    bwprintf(COM2, (char*)"Clock server initializing...\r\n");
    #endif

    while (true) {
        Receive(&request_tid, msg_buffer, 5);
        type = msg_buffer[0];

        #ifdef DEBUG_TASK_AE
        bwprintf(COM2, (char*)"[DEBUG] Clock server received request %c from tid: %d.\n\r", type, request_tid);
        #endif

        if (TICK_CHAR == type){
            nTicks++;
            Reply(request_tid, "k", 1);     // reply not used

            // awake the tasks that should be awaken
            replyQueueInOrder(delayedTasks, nTicks);
        } else if (TIME_CHAR == type) {
            reply = nTicks;
            Reply(request_tid, (const char*)&reply, 4);
        } else if (DELAY_CHAR == type) {
            // calculate the unblock time
            delayNTicks = nTicks + chars2Uint(&msg_buffer[1]);

            DelayedTask *dt = getFreeNode4delayedTask(dt_buffer);
            dt->tid = request_tid;
            dt->unblockAtTick = delayNTicks;

            // put task into ordered link-list
            pushQueueInOrder(delayedTasks, dt);

        } else if (DELAYUNTIL_CHAR == type){
            // calculate the unblock time
            delayNTicks = chars2Uint(&msg_buffer[1]);

            if (delayNTicks < nTicks){
                reply = -2;
                Reply(request_tid, (const char*)&reply, 4);
            }else{
                DelayedTask *dt = getFreeNode4delayedTask(dt_buffer);
                dt->tid = request_tid;
                dt->unblockAtTick = delayNTicks;
                // put task into ordered link-list
                pushQueueInOrder(delayedTasks, dt);
            }
        }else {
            bwprintf(COM2, (char*)"Clock server received invalid request: %c\r\n", type);
            assert(false);
        }
    }
}

// returns the number of ticks since the clock was initialized
int Time(const int tid){
    char msg_buffer[] = "t";
    char reply_buffer[] = "1234";
    msg_buffer[0] = TIME_CHAR;

    // making sure the tid is correct
    if (tid <= 0 || tid != WhoIs(CLOCK_SERVER_NAME)){
        return -1;
    }

    int result = Send(tid, msg_buffer, 1, reply_buffer, 4);

    if(-1 == result){
        return -1;
    }
    return *(int*)reply_buffer;
}

// returns after the given number of ticks has elapsed.
int Delay(int tid,int ticks){
    if (ticks < 0){
        return -2;
    }
    if (tid <= 0 || tid != WhoIs(CLOCK_SERVER_NAME)){
        return -1;
    }

    char msg_buffer[] = "c1234";
    char reply_buffer[] = "1234";
    msg_buffer[0] = DELAY_CHAR;
    uint2Chars(&msg_buffer[1], ticks);

    int result = Send(tid, msg_buffer, 5, reply_buffer, 4);

    if (-1 == result){
        return -1;
    }
    return *(int*)reply_buffer;
}

// returns when the time since clock server initialization is greater or equal than the given number of ticks
int DelayUntil(int tid, int ticks){
    if (tid <= 0 || tid != WhoIs(CLOCK_SERVER_NAME)){
        return -1;
    }

    char msg_buffer[] = "c1234";
    char reply_buffer[] = "1234";
    msg_buffer[0] = DELAYUNTIL_CHAR;
    uint2Chars(&msg_buffer[1], ticks);

    int result = Send(tid, msg_buffer, 5, reply_buffer, 4);

    if (-1 == result){
        return -1;
    }
    return *(int*)reply_buffer;
}


// uart server

/*
protocol:
	receive string <type><args>
    

*/
void task_uart_server() {
    int response = 1;
    while(response){            // make sure register is successful
        response = RegisterAs(UART_SERVER_NAME);
    }

    Create(ETP_Highest, task_uart2_timeout_notifier); 
    Create(ETP_Highest, task_uart2_tx_notifier);
    Create(ETP_Highest, task_uart1_tx_notifier);
    Create(ETP_Highest, task_uart1_cts_notifier);
    Create(ETP_Highest, task_uart1_rx_notifier);

    // char buffers
    char uart2_receive_arr[UART2_RX_BUF_CAPACITY];
    RBuf<char> uart2_receive_buf(uart2_receive_arr, UART2_RX_BUF_CAPACITY);

    char uart1_receive_arr[UART1_RX_BUF_CAPACITY];
    RBuf<char> uart1_receive_buf(uart1_receive_arr, UART1_RX_BUF_CAPACITY);

    char uart2_transmit_arr[UART2_TX_BUF_CAPACITY];
    RBuf<char> uart2_transmit_buf(uart2_transmit_arr, UART2_TX_BUF_CAPACITY);

    char uart1_transmit_arr[UART1_TX_BUF_CAPACITY];
    RBuf<char> uart1_transmit_buf(uart1_transmit_arr, UART1_TX_BUF_CAPACITY);

    // blcok queues
    int blocked_on_getc_uart2_arr[GETC_QUEUE_UART2_CAPACITY];
    RBuf<int> blocked_on_getc_uart2_buf(blocked_on_getc_uart2_arr, GETC_QUEUE_UART2_CAPACITY);

    int blocked_on_getc_uart1_arr[GETC_QUEUE_UART1_CAPACITY];
    RBuf<int> blocked_on_getc_uart1_buf(blocked_on_getc_uart1_arr, GETC_QUEUE_UART1_CAPACITY);

    int notifier_tid_blocked = -1;  // only one CTS notifier, so use an int
    // flags
    /*
    if uart 1 tx up
    didn't include 'uart1' in variable name because we don't use flag for uart2
    life cycle of this flag:
        default to false
        wants to send
        enable  tx interrupt
        tx interupt
        set this flag to true
        ...
        send
        set flag to false
    */
    bool tx_up = true;
    /*
        ready_to_send (can send a char now), (default)
        sent_not_lowered (after sending a char and confirming the interrupt),
        sent_lowered(CTS is off when in sent_not_lowered), 
        if a new CTS interrupt happens, goto ready_to_send

    */
    CTSState cts_state = CTSState::READY;

#ifdef DEBUG_TASK_AE
    bwprintf(COM2, "uart tid %d\r\n", MyTid());
#endif

    int request_tid, train_task_tid = 0;
    unsigned int buf_index;
    char msg_buffer[UART2_MSG_BUFFER_CAPACITY+1];
    char reply_buffer[10];
    char type;

    volatile int *uart2_flags_reg = (int *)( UART2_BASE + UART_FLAG_OFFSET );
    volatile int *uart2_data_reg = (int *)( UART2_BASE + UART_DATA_OFFSET );

    volatile int *uart1_flags_reg = (int *)( UART1_BASE + UART_FLAG_OFFSET );
    volatile int *uart1_data_reg = (int *)( UART1_BASE + UART_DATA_OFFSET );


    #ifdef DEBUG_UART_BUF
    int max = 0, count = 0;
    #endif

    while (true) {
        Receive(&request_tid, msg_buffer, UART2_MSG_BUFFER_CAPACITY);

        type = msg_buffer[0];
        switch (type)
        {
        case UARTServerProtocol::USP_UART2_RX:
            Reply(request_tid, "k", 1);
            while(!(*uart2_flags_reg & RXFE_MASK) ) { // keep popping as long as buffer is not empty
                unsigned char c = *uart2_data_reg;
                uart2_receive_buf.push_rbuf_item(c);
            }
            enable_uart_2_timeout();
            break;
        case UARTServerProtocol::USP_UART2_GET_C: // get char from UART 2, block if no char available
            blocked_on_getc_uart2_buf.push_rbuf_item(request_tid);
            break;
        case UARTServerProtocol::USP_UART1_RX:
        {
            Reply(request_tid, "k", 1);
            if (!(*uart1_flags_reg & RXFE_MASK)) {
                unsigned char c = *uart1_data_reg; // no FIFO, so only need to get one char
                uart1_receive_buf.push_rbuf_item(c);
            }
            // need a buffer here since it is possible that rx comes before someone calls getc
            enable_uart_1_rx_interrupt();
            break;
        }
        case UARTServerProtocol::USP_UART1_GET_C: // get char from UART 1, block if no char available
            blocked_on_getc_uart1_buf.push_rbuf_item(request_tid);
            break;
        case UARTServerProtocol::USP_UART2_TX: // uart 2 available for send
            Reply(request_tid, "k", 1);
            while(!(*uart2_flags_reg & TXFF_MASK) && !uart2_transmit_buf.is_buf_empty()) {
                *uart2_data_reg = uart2_transmit_buf.pop_rbuf_item();
            }
            if (*uart2_flags_reg & TXFF_MASK) {
                enable_uart_tx_interrupt(COM2);
            }
            break;
        case UARTServerProtocol::USP_UART2_PUT_C:
            uart2_transmit_buf.push_rbuf_item(msg_buffer[1]);
            enable_uart_tx_interrupt(COM2); // only because we need to transmit
            Reply(request_tid, "k", 1); // put is non-blocking
            break;
        case UARTServerProtocol::USP_UART2_PUT_STR:
            {
                buf_index = 1;
                while('\0' != msg_buffer[buf_index]){
                    uart2_transmit_buf.push_rbuf_item(msg_buffer[buf_index++]);
                }
                enable_uart_tx_interrupt(COM2); // only because we need to transmit
            }
            Reply(request_tid, "k", 1); // put is non-blocking
            break;
        case UARTServerProtocol::USP_UART1_TX:
            Reply(request_tid, "k", 1);
            tx_up = true;
            break;
        case UARTServerProtocol::USP_UART1_CTS:
        {   
            int cts_status = get_cts_status();
            if (cts_status == 1) {
                cts_state = CTSState::READY;
                confirm_cts_interrupt();
                notifier_tid_blocked = request_tid;
            } else {
                assert(cts_state == CTSState::SENT_NOT_LOWERED);
                cts_state = CTSState::SENT_LOWERED;
                confirm_cts_interrupt();
                Reply(request_tid, "k", 1);
            }
            break;
        }
        case UARTServerProtocol::USP_UART1_PUT_C:
            Reply(request_tid, "k", 1); // put is non-blocking
            uart1_transmit_buf.push_rbuf_item(msg_buffer[1]);
            break;
        case UARTServerProtocol::USP_UART1_PUT_STR:
            Reply(request_tid, "k", 1); // put is non-blocking
            {
                buf_index = 1;
                do{
                    uart1_transmit_buf.push_rbuf_item(msg_buffer[buf_index++]);
                }while('\0' != msg_buffer[buf_index]);  // allow the first char to be \0
            
                #ifdef DEBUG_UART_BUF
                bwprintf(COM2, "\x1b[s\x1b[29;1f\x1b[2KCur size: %d, max: %d\x1b[u", uart1_transmit_buf.size(), max);

                if(uart1_transmit_buf.size() > max){
                    max = uart1_transmit_buf.size();
                }
                if(uart1_transmit_buf.size()!= max && uart1_transmit_buf.size()>12){
                    bwprintf(COM2, "\x1b[30;1f");
                    int val;
                    while(!uart1_transmit_buf.is_buf_empty()){
                        val = uart1_transmit_buf.pop_back();
                        bwprintf(COM2, "%d ", val);
                    }
                    assert(false);
                }
                #endif
            }
            break;
        case UARTServerProtocol::USP_TRAIN_FLUSH:
            train_task_tid = request_tid;
            break;
        default:
            break;
        }

        while (!uart2_receive_buf.is_buf_empty() && !blocked_on_getc_uart2_buf.is_buf_empty()) {
            // deal with getc for uart 2
            char c = uart2_receive_buf.pop_rbuf_item();
            reply_buffer[0] = c;
            Reply(blocked_on_getc_uart2_buf.pop_rbuf_item(), reply_buffer, 1);
        }

        while (!uart1_receive_buf.is_buf_empty() && !blocked_on_getc_uart1_buf.is_buf_empty()) {
            // deal with getc for uart 1
            char c = uart1_receive_buf.pop_rbuf_item();
            reply_buffer[0] = c;
            Reply(blocked_on_getc_uart1_buf.pop_rbuf_item(), reply_buffer, 1);
        }

        if (tx_up && cts_state == CTSState::READY && !uart1_transmit_buf.is_buf_empty()) {
            assert(*uart1_flags_reg & TXFE_MASK); // tx_up should imply the flag is good
            *uart1_data_reg = uart1_transmit_buf.pop_rbuf_item(); // write char
            tx_up = false;
            confirm_cts_interrupt(); // need an interrupt to goto SENT_LOWERED state
            cts_state = CTSState::SENT_NOT_LOWERED;
            Reply(notifier_tid_blocked, "k", 1);
            notifier_tid_blocked = -1;
            enable_uart_tx_interrupt(COM1);
        }

        if (train_task_tid && uart1_transmit_buf.is_buf_empty()){
            Reply(train_task_tid, reply_buffer, 1);
            train_task_tid = 0;
        }
    }

}



// notifies uart server about uart2 timeout event
void task_uart2_timeout_notifier(){
    char msg_buffer[] = "0";
    char reply_buffer[] = "0";
    msg_buffer[0] = UARTServerProtocol::USP_UART2_RX;
    unsigned int server_tid = WhoIs(UART_SERVER_NAME);

    while (true) {
        AwaitEvent(Event::EVT_UART2_TIMEOUT);
        Send(server_tid, msg_buffer, 1, reply_buffer, 1);
    }
}

// notifies uart server about uart2 tx event
void task_uart2_tx_notifier(){
    char msg_buffer[] = "0";
    char reply_buffer[] = "0";
    msg_buffer[0] = UARTServerProtocol::USP_UART2_TX;
    unsigned int server_tid = WhoIs(UART_SERVER_NAME);

    while (true) {
        AwaitEvent(Event::EVT_UART2_TX);
        Send(server_tid, msg_buffer, 1, reply_buffer, 1);
    }
}

// notifies uart server about uart1 tx event
void task_uart1_tx_notifier(){
    char msg_buffer[] = "0";
    char reply_buffer[] = "0";
    msg_buffer[0] = UARTServerProtocol::USP_UART1_TX;
    unsigned int server_tid = WhoIs(UART_SERVER_NAME);

    while (true) {
        AwaitEvent(Event::EVT_UART1_TX);
        Send(server_tid, msg_buffer, 1, reply_buffer, 1);
    }
}

// notifies uart server about uart1 cts changed
void task_uart1_cts_notifier(){
    char msg_buffer[] = "0";
    char reply_buffer[] = "0";
    msg_buffer[0] = UARTServerProtocol::USP_UART1_CTS;
    unsigned int server_tid = WhoIs(UART_SERVER_NAME);
    while (true) {
        AwaitEvent(Event::EVT_UART1_CTS);
        Send(server_tid, msg_buffer, 1, reply_buffer, 1);
    }
}

// notifies uart server about uart1 rx
void task_uart1_rx_notifier(){
    char msg_buffer[] = "0";
    char reply_buffer[] = "0";
    msg_buffer[0] = UARTServerProtocol::USP_UART1_RX;
    unsigned int server_tid = WhoIs(UART_SERVER_NAME);
    while (true) {
        AwaitEvent(Event::EVT_UART1_RX);
        Send(server_tid, msg_buffer, 1, reply_buffer, 1);
    }
}

/*
returns next unreturned character from the given UART. 
The first argument is the task id of the appropriate server. 
How communication errors are handled is implementation-dependent.
Getc() is actually a wrapper for a send to the appropriate server. 

return -1 if tid is not a valid uart server task. 
*/
int Getc(int tid, int uart) {
    char msg_buffer[] = "0";
    char reply_buffer[] = "0";

    if (uart == COM2) {
        msg_buffer[0] = UARTServerProtocol::USP_UART2_GET_C;
    } else {
        msg_buffer[0] = UARTServerProtocol::USP_UART1_GET_C;
    }

    if (tid != WhoIs(UART_SERVER_NAME)) return -1;
    int result = Send(tid, msg_buffer, 1, reply_buffer, 1);
    assert(result > 0);
    return reply_buffer[0];
}

/*
queues the given character for transmission by the given UART. On return the only guarantee is that the character has been queued. Whether it has been transmitted or received is not guaranteed. How communication errors are handled is implementation-dependent. Putc() is actually a wrapper for a send to the appropriate server.

Return Value
0	success.
-1	tid is not a valid uart server task. 
*/
int Putc(int tid, int uart, char ch) {
    char msg_buffer[] = "00";
    char reply_buffer[] = "0";

    if (uart == COM2) {
        msg_buffer[0] = UARTServerProtocol::USP_UART2_PUT_C;
    } else {
        msg_buffer[0] = UARTServerProtocol::USP_UART1_PUT_C;
    }
    msg_buffer[1] = ch;
    if (tid != WhoIs(UART_SERVER_NAME)) return -1;
    int result = Send(tid, msg_buffer, 2, reply_buffer, 1);
    assert(result > 0);
    return 0;
}

int Putstr(int tid, int uart, char* str){
    short size = strlen(str);
    char msg_buffer[size + 2];  // add 1 for identifier, add 1 for \0
    char reply_buffer[] = "0";

    if (uart == COM2) {
        msg_buffer[0] = UARTServerProtocol::USP_UART2_PUT_STR;
    } else {
        msg_buffer[0] = UARTServerProtocol::USP_UART1_PUT_STR;
    }
    memcpy(&msg_buffer[1], str, size);
    if (tid != WhoIs(UART_SERVER_NAME)) return -1;
    int result = Send(tid, msg_buffer, size + 1, reply_buffer, 1);
    assert(result > 0);
    return 0;
}

int Putstr(int tid, int uart, char* str, int size){
    char msg_buffer[size + 2];  // add 1 for identifier, add 1 for \0
    char reply_buffer[] = "0";

    if (uart == COM2) {
        msg_buffer[0] = UARTServerProtocol::USP_UART2_PUT_STR;
    } else {
        msg_buffer[0] = UARTServerProtocol::USP_UART1_PUT_STR;
    }
    memcpy(&msg_buffer[1], str, size);
    msg_buffer[size + 1] = '\0';
    if (tid != WhoIs(UART_SERVER_NAME)) return -1;
    int result = Send(tid, msg_buffer, size + 1, reply_buffer, 1);
    assert(result > 0);
    return 0;
}

int PutstrPartition(int tid, int uart, char* str, int partition_size){
    int size = strlen(str);  // add 1 for identifier
    char msg_buffer[partition_size];
    int i = partition_size;
    while(i <= size){
        strncpy(msg_buffer, (str + i - partition_size), partition_size);
        Putstr(tid, uart, msg_buffer, size);
        i += partition_size;
    }
    memcpy(msg_buffer, (str + i - partition_size), i - size);
    Putstr(tid, uart, msg_buffer, i - size);
    return 0;
}
