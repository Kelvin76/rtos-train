#include "glob.h"
#include "global.h"
#include "slab.h"
#include "td.h"
extern "C" {
    #include "bwio.h"
}

// DO NOT ASSIGN VALUE HERE
// see https://piazza.com/class/l2g73ho4t8f61q?cid=44
SlabAllocator slab_allocator;

// ---- tasks related ----
u32 *g_cur_space;       // = g_cur_task_td->sp

// pool for task descriptors
TD *g_task_pool[NUM_TASK];
int g_task_pool_size;
int tid_counter;        // unique positive number indicating a task/process

// priority queue for ready tasks
Queue ready_queue_by_priority[NUM_PRIORITIES];

// kernel variables
TD *g_cur_task_td;      // init td for the current process to null
int *g_kernel_sp;
void* redboot_return_addr;

void assert(bool a, char* str) {
    if (!a) {
        #ifndef TRAIN_SYSTEM_ON
        bwprintf(COM2, (char*)"Assertion failed! %s\n\r", str);
        #else
        bwprintf(COM2, (char*)"\x1b[22;1fAssertion failed! %s\n\r", str);
        #endif
        _my_exit();
    }
}

asm(R"(
    .text
    .align 2
    .global	_my_exit
    .type _my_exit, %function
    _my_exit:
    ldr r0, =redboot_return_addr
    ldr pc, [r0]
)");

int name_server_tid;
unsigned int g_ticks;
int g_idle_task_tid;
int g_uart_server_tid;

// system measurement helper variables
unsigned int g_idle_task_start_at; // start at, in actual hw timer value
unsigned int g_idle_task_end_at; // end at, in actual hw timer value
unsigned int g_idle_task_running_time; // running time of idle task, in actual hw timer value

Queue g_event_queue_by_event[NUM_EVENTS];