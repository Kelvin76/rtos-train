#include "slab.h"
#include "memconf.h"
#include "glob.h"

// void* assert_exit = 0;

extern "C" {
  // import c libs here
  #include "bwio.h"
}

SlabAllocator::SlabAllocator() {
	for (int i = 0; i < SLAB_AMOUNT; i++) {
		Queue::Node* addr = (Queue::Node*) slab_addr(i);

		#ifdef DEBUG_MEM
		bwprintf(COM2, (char*)"[Debug] slab allocated: %u\n\r", addr);
		#endif

		free_list.push(addr);
	}
}

void* SlabAllocator::get_slab() {
    return (void*) free_list.pop();
}