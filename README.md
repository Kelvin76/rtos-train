# RTOS - Train control

## Introduction

- This project implemented a **Real-Time Operating System** that **controls** trains and tracks built by Märklin.
- This code in this project is built specifically for [TS-7200](https://www.embeddedts.com/products/TS-7200) single-board computer based on the [EP9302](https://www.cirrus.com/products/eol/) system-on-a-chip (SoC) that uses an ARM 920T processor. The processor implements ARM architecture version ARMv4T.
- The track and trains are provided by Märklin and they can be controlled by sending commands with the serial interface. 
- The boot loader used for the kernel in this project is [RedBoot](http://ecos.sourceware.org/redboot/).

#### Main development flow

This project was completed by 2 people and took 11 weeks.  
There are 3 parts to this project, each with several phases (7 phases in total, between 1 and 2 weeks each).

1. Kernel
    - The underlying Operating System
    - Phases: k1, k2, k3, k4
2. Train Control
    - Building a user application based on the Kernel that can communicate to the train/track system
    - Phases: tc0 (preparation phase, primarily for data gathering), tc1, tc2
3. A "game" using the trains
    - only one phase

Note that each part/phase depends on the completion of the previous part. 

## Table of Contents

[[_TOC_]]

## Make executable
First clone the repo:
```
git clone https://gitlab.com/Kelvin76/rtos-train.git
```
Then, cd into the repo:
```
cd rtos-train/
```
If you wish to run the final project code, please checkout the `final_and_tc2_debug` branch.
```
git checkout final_and_tc2_debug
```
Checkout the SHA:
```
git checkout bbf415d1718d0b4c1d04c0e48829ea0845d6cc39
```
Our makefile is located in `src`, so please cd into src:
```
cd src
```
Then run `make`:
```
make
```
This should generate `main.elf`, the desired executable.

## Operate the executable

If you have the proper setup for both the software (RedBoot) and the hardware (ARM) mentioned in the [Introduction](#introduction), then you can run the following command in RedBoot environment to launch the program:
```
load -h main.elf
go
```

## Train control commands
When the system is started, it will first go through an initialization phase (will ask for the track selected because this project supports 2 specific track layout). After that, we shall see the a command line interface. This interface will support all the following commands/instructions:
- game \<prey\> \<hunter 1\> \<hunter 2\> Starts a game with three trains, one prey and two hunters.
- tr \<train number\> \<train speed\> – Set train speed. Valid `train_number` is in [1, 80] and `speed_level` is in [0, 31]
- rv \<train number\> – reverse a train and let it run in the same speed as before this instruction
- sw \<switch number\> \<switch direction\> – throw a switch to straight ('s') or curved ('c'). Valid `switch_number` is in [1, 18] & [153, 156]. Valid direction is either `s` or `c`.
- re \<train number\> \<sensor name\> - register a sensor manually for a train. Should use a sensor close to the train to register. Note that, just set a non-zero speed for the train will lead to the same result.
- dt \<train number\> \<target location\> \<offset\> - ask the train to goto the target location. The offset is in unit of millimeter (default zero if not provided). Note that, the train has to have triggered/registered a sensor before executing this command because the system cannot know where the train is automatically. This can be done with either set a speed (tr command) to the train and wait for it to trigger a sensor or register a sensor to it manually.
- stop \<train number\> - stop a train as soon as possible
- q – stop the system (also stops all the trains) and quit

## File structure

- All header files are stored in `rtos-train/include`
- All c/cpp files are stored in `rtos-train/src`
- All tasks are in `rtos-train/src/tasks.cpp`, to add a new task, please add it as a function defination to `rtos-train/src/tasks.cpp` and add the signature to `rtos-train/src/tasks.h`. You will also need to increment the `NUM_TASK` macro in `global.h` to reflect the correct number of tasks running.
- `Makefile` is in `rtos-train/src`
- `linker.ld` is in `rtos-train/src`
- `performance.txt` is in `rtos-train/`

### File explanation
- `track.h/cpp`:
    - these files contains the data for each track provided from [`track data`](https://student.cs.uwaterloo.ca/~cs452/S22/track/). 
    - it contains the track models for track A and B
- `track_node.h/cpp`:
    - each node in `track data` is of struct `track_node`, which is defined in this file
- `train.h/cpp`
    - this file contains a model of a train, all the information needed of a train will be found here
- `bwio.h/c`: (deprecated)
    - this file stores a library for busy-wait input/output (only used in k1 - k3, not going to use this at the same time with the UART server)
- `config.h`:
    - this is a configuration file
- `global.h/cpp`:
    - this file stores many shared global variables, structures, and some helper functions used for all other files
    - the pool for task descriptors (`g_task_pool`) and the priority queue for ready tasks(`ready_queue_by_priority`) are both here
- `io_lib.h/cpp`:
    - this file stores the serial I/O library for UART server, provides function to printed formated strings
    - implemented based on Send-Receive-Reply model and `Putc` from `task_lib`
    - tight closely with the UART server
- `interrupt.h/cpp`:
    - this file stores the code to set enable interrupt in vic, enable halt, and start a halt.
- `math_lib.h/cpp`:
    - this file stores some math functions that will help in calculating/approximating physics equations
- `memconf.h`:
    - this file stores the configurations for memory management
- `q.h/cpp`:
    - this file stores a library for `Queue`, implemented queue as an intrusive linked-list
- `RBuf.h`:
    - this file stores a library for `RBuf` (ring buffer), implemented as a class that can push and pop to/from a buffer
- `slab.h/cpp`:
    - this file stores a library for `SlabAllocator`, which could assign slabs to tasks
- `task_lib.h/cpp`:
    - this file stores a library for functions called by all tasks (both kernel functions and user functions)
    - kernel task/function is also defined here, but not open to users
    - server tasks in user-level are alson defined here and are open to users
- `tasks.h/cpp`:
    - this file stores the declarations and definitions of all tasks (tasks with their own stacks/slabs)
    - tasked required by deliverables are defined here
- `td.h/cpp`:
    - this file stores the definitions of task descriptors, included a diagram for stack in this header file
- `timer.h/cpp`:
    - this file stores functions for timer initialization
- `trainContro.h/cpp`:
    - this file stores a class and functions for the train control system
    - the system will be started if the task `task_train_control` is created
- `ts7200.h`:
    - this file stores specific the macros for ts7200 related memory addresses
- `performance.txt`:
    - this file stores our measurement tests for the Send-Receive-Reply model with different parameters
    - Each line means \<Optimization on\/off\> \<Cache on\/off\> \<Receiver or Sender first\> \<Message size: 4, 64, 256\> \<Average time for single transaction\>

## Description of the structure of the project
### Final project
In our final project, we model a chasing game with one prey and two hunters: hunter_1 and hunter_2. At the start of the game, hunter_1 will try to stalk the prey by maintaining a constant short distance behind the prey. This is because when the prey has a head-on collision with the hunter, we count the prey as lost. So prey will be forced to move in the forward direction.  
Once hunter 1 starts stalking the prey, the second hunter will try to have a head on collision with the prey, thereby ending the game with a hunter_victory.  
There are several ways a prey can lose. The usual way is when it has collided with a hunter heads on. The prey also fails if it gets too close with the exit nodes.  
If the prey is lucky, it might be able to move in a way that causes a head on collision between the hunters, in this case it wins.  
However this is very rare, so we usually evaluate a player's skills using the survival time of the prey.

#### Algorithm
The prey is controlled by the user, so it doesn't require any special algorithm. The prey will always be at velocity 7, but player can steer the prey by changing the next turnout in the current path of prey. The users can also reverse the prey. These techniques help prey escape from the hunters. We try to avoid the situation where the user switches a turnout right underneath the prey, by comparing the current estimated location of the prey with the location of next turnout. If the distance is too short, turnout switching will be banned temporarily to avoid switching the turnout underneath the prey.  
The first hunter tries to stalk the prey. The main idea is that it acts like a state machine. At the beginning, it is in `capturing` state and tries to get close to the prey by routing (using tc1 feature) to the current location of the prey, this logic is in `reroute_hunter` function. We re-route everytime the prey triggered a sensor or switched a turnout or reversed, to make sure the first hunter is always going to the up-to-date location of the prey. When the first hunter is within certain distance with the prey, it switches to `captured` state and the main objective of the hunter changes from getting to the current location of prey to keeping a centain distance to the prey. This is done using location estimation and velocity estimation of the prey and hunter 1. We have a desired distance we want to keep. If the current estimated distance between hunter 1 and prey is smaller than this distance, we decelerates the velocity of the prey slightly so that it's slightly smaller than the velocity of prey. If the current estimated distance is bigger, we try to increase the velocity of the prey slightly to close the gap.  
The logic of the second hunter is much simpler, it tries to route itself to the reverse location of the prey in order to collide with it. The trains use collision avoidance mechanism to avoid colliding with each other. If the collision is a head-on collision, it will end the game, otherwise they will try to recover from it.

### Task Structure

We primarily use the server model in our system.  

There are in total 17 tasks in k4. 11 tasks are (can be seen as) the kernel: 1 idle task, 1 first user task used to initialize, 3 servers and 6 notifiers. 6 tasks are for the train control system  (user tasks): 1 task to run the train control system, 1 task to send delayed commands, and 4 notifiers for the train control system.

#### Kernel:
Tasks:
1. Idle task: scheduled when all other tasks are blocked
2. First user task: used to initialize the system (creating other tasks)

Servers:
1. Name Server: 
    - serves as a central name manager, user tasks can find other tasks by providing a name through this server
    - provides the functionalities of `RegisterAs(name)` and `WhoIs(name)`
2. Clock Server: 
    - serves as a time manager, allows user tasks to find current time or block (yield) for a given amount of time
    - provides the functionalities of `Time()`, `Delay()` and `DelayUntil()`.
3. UART server: 
    - serves as a I/O manager, user tasks can interact with different UART through this server (the system only support 2 UART right now)
    - provides the functionalities of `Putc(channel, char)` and `Getc(channel)`

Notifiers:
1. clock notifier:
    - notify the clock server every 1 tick (10 ms), so the clock server can increment the internal timer
2. UART1 RX notifier:
    - notify the uart server whenever an interrupt of RX for UART1 is triggered
3. UART1 TX notifier:
    - notify the uart server whenever an interrupt of TX for UART1 is triggered
4. UART1 CTS notifier:
    - notify the uart server whenever an interrupt of CTS for UART1 is triggered
5. UART2 TX notifier:
    - notify the uart server whenever an interrupt of TX for UART2 is triggered
6. UART2 timeout notifier:
    - notify the uart server whenever an interrupt of Timeout for UART2 is triggered

#### Train Control:

Main task - Train control task:
- This task is named to be `task_train_control`, it will create an instance of `TrainSystem` named `ts` and call `ts.start()` to start the train control system

Sub task - a server to send delayed commands (named `task_delayed_command_server`):
- This task/server provides the functionality to send delayed commands
- The Train control task sometimes will send messages to this task with a char string of commands and an activation time. This task will send the commands to the UART server when the activation time is reached

Notifiers:
1. Notifier for the delayed commands server
    - notify the delayed command server every 1 tick, so the clock server can increment the internal timer and check if commands should be sent to the UART server
2. Wall clock notifier:
    - notify the train control task whenever every 10 ticks (100 ms), so the train contron system can update the wall clock
3. Input notifier:
    - await on getting input from UART2 (COM2) and send the input to the train control task, so the train control task can parse the input
4. Sensor notifier:
    - frequently request sensor data from UART1 (COM1 - train) and send sensor number for triggered sensor to the train control task


### Algorithms and Designs
- Path finding (updated version / TC 2)
    - The path finding function is based on what we had in TC1. We add a few functionalities to it and refactored it into several functions. `TrainSystem::train_path_planning()` will find a feasible path by using `Track.path_planning(Location& src, const Location& dest, unsigned int dist[TRACK_MAX], int parent[TRACK_MAX])` for a train (might require the train to reverse at the beginning). Then it will call `TrainSystem::set_train_path()` to recover the path and save some crucial track nodes on the path in the train datastructure. Then the train will set a speed to start its journey.
    - Unlike in TC1, the turnouts on the path will be switched to the correct direction when the path was found. With the help of saved path stored in the train datastructure, we will now switch the turnouts just in time. This also allows us to have multiple trains running to gether.

- Track reservation system
    - Since multiple trains can be on the track at the same time, each train needs to know whether there are physical trains on the other track. Hence, we have a track reservation system.
    - The tracks will be reserved by each train dynamically and each train will only reserve at most 5 track nodes at once. The first track reserved by a train will the track under its wheel and other track nodes will be the nodes in front of it. In other words, the tracks that are reserved are guaranteed to have trains on them either now or future.
    - Everytime a sensor is triggered by a train, it will check if it can reserve a few track nodes in the front, if those tracks are already reserved by another train, then it means a collision is likely to happen. If the track nodes in the front are not reserved by other trains, it can just reserve them and go there freely. 

- Collision prevention
    - Collision prevention relies heavily on the track reservation system. As mentioned before, each time a sensor is triggered by a train, the train will look ahead a few track nodes. Based on those track nodes, we will know who reserved these nodes and what is going to happen between the other train and this train. This can be found easily, e.g., if the other train reserved the reverse nodes of the front track nodes, then we know that train will have a different direction with this train. This is because the train will only reserve track nodes based on their currention direction. Also, we will be able to know the speed of the other train as well. This will help in deciding what to do to prevent collisions.
    - We will show some incomplete solutions in some cases:
        - Two trains running in the same direction:
            - There is a merge in between (two trains will collide). Then we can calculate the time consumption for each train to reach the other end of the merge, then we will see if slowing down one train (or speed up one train) will prevent collision. If not, we can just stop one train
            - Two trains are running on the same track, solutions are either slowing down one train or stop one train.
        - Two trains running in different directions:
            - Two trains are running on the same track, solutions are
                - stop one train and reverse it if time allows
                - or, stop both train and reverse one of them
        - ...
    - This system needs a lot of calculation and sensor information to be accurate and in time. As a result, this system really needs some design decisions on the balance between what needs to be achieved and what can be done efficiently

- Physics engine
    - We store a snaphot of train's physical property `PhysicalStateSnapshot` at a timestamp. 
    This Snapshot includes acceleration, velocity and lcoation at a timestamp. Everytime we update the acceleration (due to change to speed levels) or the location (due
    to sensor attribution), we refresh this snapshot. This snapshot is used to estimate the location of the train accurately (using d = vt+0.5at^2). The location estimation is used for sensor attribution,
    whenever a new sensor is triggered, we find that train whose next sensor is this sensor
    and the estimated location is closet to the sensor.
- #### Path finding (original version / TC1)
    - The core path finding algorithm is in `Track.path_planning(Location& src, const Location& dest, unsigned int dist[TRACK_MAX], int parent[TRACK_MAX])`, which is basically a wrapper for running dijkstra from `src` location to `dest` location. In this implementation, each `track_node` is considered as a node in dijkstra, it finds the shortest distance between the `landmark` of `src` and `dest` (see explaination of `Location` class below). If `src` is the same as the `dest`, the current implementation will "let the train run" and consider the shortest distance from the next sensor that the train will pass to the original destination.
    - In `train_control` task, everytime we receive a `dt` command, after parsing the command, we call `Track.path_planning` with the current location of the train, we inspect the path returned by `Track.path_planning` , and there are several cases:
        - the algorithm did not find any path between the source and destination, in this case we tell the user that path finding has failed (this happens because we are currently not considering reversing the train).
        - We have a `TC_DT_SAFE_DISTANCE` defined. This is the distance we need to make sure the train will stop accelerating (a.k.a. reach a constant speed). So if the path from src to dest is smaller than the safe distance, we check the distance from src to reverse of dest `Location` (for definition of `reverse` please see documentation on `Location` below). If that didn't work either, we find a point that has a distance >= `TC_DT_SAFE_DISTANCE` from the train and travel to that point, then to the destination. This step is needed because we currently do not have acceleration esitmation.
        - When a path is eventually found, we augment the path by calling `TrainSystem::set_train_path()` on it. This function sets the train speed to `TC_DT_DEFAULT_SPEED` (speed level 12) if the speed of train is below 7 (train with speed below 7 might stuck). This function then takes the offset of src and dest into consideration and find the stop location, which is where we need to issue the stop command. After that, this function sets the switches so that the train is on track to the destination.
- Priority queue for scheduling:
    - To obtain the highest available task, we just do a simple linear search from highest priority to lowest priority.  

- Memory management(allocation)
    - Some simple addition/substraction/multiplication is used to allocate memory. We know kernel memory roughly starts from 0x2000000, we give our kernel roughly ~1mb memory, which means that we are limiting out kernel memory to at most grow to 0x1F00000. Starting from this address, we allocate 10 slabs of size 250000 bytes each. These slabs can be used to hold task memory and descriptors.

- Context switch (most important part)  
    - When entering kernel, we trigger a software interrupt, in the interrupt handler, we save current user context, restore kernel context and branch to main kernel loop.  
    - When leaving kernel, we first change cpsr to enter user mode, then restore user task context.  
    - Different user-level function calls to the system will trigger a different `n` value for the `SWI n` instruction. Inside the kernel (kernel task), the logic will change based on this value. This way, the kernel can act like a multiplexer and is able to handle different system calls from the users.
    - In a memory slab allocated to a task, we reserve some space at high address for the task descriptor. Going from high address to lower address, we reserve some space to store user task context: registers r0 to r15 and psr. We have a pointer pointing to the low address of this space (there is a more detailed diagram in `rtos-train/include/td.h`). Thus, every time we save the user context, we load this address to a resgiter and do a STMIA using the register as base register, every time we load user context, we do the same thing but use LDMIA. The rest of the memory after this space is reserved for task stack.
    ```
    Task slab
            +-------------------------+ Low Address
            |                         |
            |                         |
            |                         |
            |        Task Stack       |
            |                         |
            |                         |
            |                         |
            |-------------------------|
            |       Task Context      |
            |-------------------------|
            |     Task descriptor     |
            +-------------------------+ High Address

    ```

- Send-Receive-Reply:
    - The communication between tasks in the kernel is using a Send-Receive-Reply (SRR) model
    - Declarations and parameters (signatures):
        - `Send(int tid, const char *msg, int msglen, char *reply, int rplen)`
            - tid: the receiver (target)
            - msg: the input message provided by the sender (current task)
            - msglen: the length of `msg`
            - reply: place to store the reply from the receiver
            - rplen: maximal size of the reply (size of reply)
            - output:
                - `-1`: `tid` does not exist
                - `-2`: Send-Receive-Reply transaction cannot be completed
                - Otherwise, returns the size of the replied message
        - `Receive(int *tid, char *msg, int msglen`
            - tid: the sender task id
            - msg: the message sent by the sender
            - msglen: the length of the message sent by the sender
            - output: the size of the message sent by the sender
        - `Reply(int tid, const char *reply, int rplen)`
            - tid: the sender (also the target to be sent to)
            - reply: the message to be sent to the sender
            - rplen: maximal size of the reply (size of reply)
            - output:
                - `-1`: `tid` does not exist
                - `-2`: `tid` is not blocked on receiving repliess (not reply-blocked)
                - Otherwise, returns the size of the replied message
    - Send (from the user-level function call):
        - Stores the arguments for the parameters in a struct instance and save it on the stack (following the ABI rules)
        - Trigger a system call by using `swi n` instruction so the kernel will call the corresponding kernel functions for `Send` (i.e., `k_Send`)
        - (kernel-level function) Check if the state of the target task (receiver) is waiting for senders (`Send_block`)
            - If yes, this is a "Receiver-first" scenario. Then we unblock the receiver (make it ready) and block the sender so it will wait for replies (`Reply_block`). After that, we transfer the data from the sender to the receiver and exit kernel.
            - If no, this is a "Sender-first" scenario. Then we block the sender so it will wait for receivers (`Receive_block`) and exit kernel.
    - Receive (from the user-level function call):
        - Stores the arguments for the parameters in a struct instance and save it on the stack (following the ABI rules)
        - Trigger a system call by using `swi n` instruction so the kernel will call the corresponding kernel functions for `Receive` (i.e., `k_Receive`)
        - (kernel-level function) Check if there is any senders blocked on sending message to the current task
            - If yes, this is a "Sender-first" scenario. Then we ask the sender to  wait for reply (`Reply_block`). After that, we transfer the data from the sender to the receiver and make the receiver ready again. Then exit kernel.
            - If no, this is a "Receiver-first" scenario. Then we block the receiver (current task) so it will wait for senders (`Send_block`) and exit kernel.
    - Reply (from the user-level function):
        - Stores the arguments for the parameters in a struct instance and save it on the stack (following the ABI rules)
        - Trigger a system call by using `swi n` instruction so the kernel will call the corresponding kernel functions for `Reply` (i.e., `k_Reply`)
        - (kernel-level function) Make sure the sender (task to receive the reply) is waiting for replies to proceed.
        - Transfer the reply data from the receiver to the sender and make the sender and receiver ready. Then exit kernel.

- Name server:
    - This is a user task acting as a name server implemented in this system
    - If a name server is running (hopefully at the highest priority for user tasks), two wrapper functions: `RegisterAs(name)` and `WhoIs(name)` will be functional. For `RegisterAs(name)`, the name server will allow a task to register a name to itself (task id). For `WhoIs(name)`, the name server will return the task id related to this name. These two functions are just wrappers of a Send and Reply model.
        - `RegisterAs` will send the name with a specification character (`r` at the begining). So the name server knows this this a "register" request. Then the nameserver will put this mapping between the name and the taskid into its buffer. Note that the sender id is collected by using the `Receive` function in the Name server. After that, the name server will reply to the calling task so it can continue executing.  
        Note that if a name already exists in the name server, then the new mapping will overwrite the original one.  
        Outputs: 
            - `0` if success
            - `-1` if error happened in sending requests to the name server (e.g., task id for name server is wrong, or name server is not initialized)
        - `WhoIs` will send the name with a specification character (`w` at the begining). So the name server knows this this a "whois" request. Then the nameserver will iterate through its buffer for mappings and see if the name matches any entry. If found, the name server replies with the task id of the very task. Otherwise, the name server replies with -1. Then, the function of `WhoIs` will return this value to the calling task.  
        Outputs: 
            - `-1` if error happened in sending requests to the name server (e.g., task id for name server is wrong, or name server is not initialized)
            - `-2` if there is no task registered as the name provided
            - Otherwise, returns the task id for the name provided

- Clock server:
    - This is a user task that provides time information for other user tasks and it also provides an important `Delay` function for other user tasks. It has an important variable named `nTicks`, it stores the time elapsed since its creation. The unit of `nTicks` is ticks and each tick mean 10 ms.
    - General process of the clock server: the clock server is just like a normal server, it will loop forever and waiting on receives. It only receive 4 kinds of requests:
        - Time request: This is from the wrapper `Time()`, which is to return the number of ticks since the clock server was created and initialized. The clock server will simply reply `nTicks` to the caller.
        - Delay request: This is from the wrapper `Delay()`, which is to block the caller for a certain amount of time. In this case, ther server will put the task id of the caller into an ordered list (the order is based on the amount of time the tasks will be unblocked, from little to more).
        - DelayUntil request: This is from the wrapper `DelayUntil()`, which is almost the same to the Delay request. Find out the different in the below section.
        - Tick request: There is an independent task called `Clock Notifier`, which will wait on events of ticks and send message to the clock server, it will run forever. As a result, the clock server will receive the tick information every time a tick is triggered, i.e., every 10 ms. Whenever the clock server receives the tick information, it will increment `nTicks`.  
        Additionally, the clock server will unblock (reply to) the tasks that should be unblocked by now.
    - Interfaces: other user tasks will access to the clock server through 3 wrapper functions: `Time()`, `Delay()`, and `DelayUntil()`. Note that, currently one <u>tick</u> in the clock server means 10 ms (0.01 second).
    - `Time(tid)`:
        - Purpose: returns the number of ticks since the clock server was created and initialized
        - Input: the task id of the clock server
        - Outputs:
            - time in ticks since the clock server initialized.
            - `-1`: tid is not a valid clock server task.
    - `Delay(tid, ticks)`:
        - Purpose: returns after the given number of ticks has elapsed; i.e., <u>block</u> the caller for the given number of ticks
        - Input: the task id of the clock server and the number of ticks
        - Outputs:
            - time in ticks since the clock server initialized.
            - `-1`: tid is not a valid clock server task.
            - `-2`: negative delays
    - `DelayUntil(tid, ticks)`:
        - Purpose: returns when the time since clock server initialization is greater or equal than the given number of ticks; i.e., <u>block</u> the caller until the clock has been initialized for the given number of ticks
        - Input: the task id of the clock server and the number of ticks
        - Outputs:
            - time in ticks since the clock server initialized.
            - `-1`: tid is not a valid clock server task.
            - `-2`: negative delays

- Idle task:
    - Our idle task implementation is `task_idle()` in `tasks.cpp`.
    - The code is an infinite loop where in each loop we halt the CPU and wait for an interrupt to turn it back on.
    - The idle task is created at startup time of our kernel, with the lowest priority (so it won't starve any other task). We argue that since the user does not, and should not create or provide the idle task, it is not a user task. Therefore, it is acceptable for it to access the halt register (`0x80930008`) directly.

- AwaitEvent:
    - Awaiting for events is done using interrupts. We have only implemented "timer tick" event so far. For this event, we set up tc1 in a way that every 10ms it sends an interrupt. This interrupt will be caught by the interrupt handler, which saves registers and then enters kernel. In the kernel, we keep a queue of all the tasks blocked on this event (when a task calls `AwaitEvent()` it traps into kernel and kernel adds calling task to the queue). So whenever an interrupt happens we find the corresponding queue and unblock tasks blocked on this event.

- UART server
    - UART server constantly receives from other tasks (clients) in a loop. Two main classes of clients are notifiers and user tasks trying to interface with UART (using `Getc()` and `Putc()`). Each of the interrupts involved has a notifier to notify UART server that it happened. 
    - Interacting with UART2
        - For UART2, we enable FIFO and RX timeout interrupt and TX interrupt.
        - For receiving, when a user calls `Getc()`, they are blocked and stored inside a queue. When a RX timeout happens, a notifier notifies UART server. UART server will pop received chars from FIFO and store them into a queue. At the end of each loop of UART server, if there are characters received and blocked `Getc()` caller, we pop received characters and feed them to `Getc()` callers and unblock them using reply.
        - For sending, whenever a user calls `Putc()`, we get the char they are trying to send and store the char in a queue, without blocking user. Whenever a TX interrupt happens (FIFO at least half empty), we pop the chars from the queue and store them in UART2 data register in order to send them, we repeat this until the output FIFO is full or the queue that stores chars to sent is empty.
    - Interacting with UART1
        - For UART1, we enable TX interrupt, CTS interrupt and RX interrupt, but disable FIFO (because we want to receive interrupt right away).
        - For receiving, it is pretty much the same as UART1, except that instead of draining the FIFO, we only receive 1 char. So when a user tasks calls `Getc()`, it is blocked. When a RX interrupt happens, UART server is notified and receive one char from UART1 and store that char in a queue. At the end of each loop in UART server, we check is there exists any char in the queue and if any user is waiting to receive. If yes, we pop the char from the queue and feed it to the user as a reply.
        - Fro sending, it is much more complex. We only want to send when both ARM machine is ready to send (signalled by a TX interrupt) and train controller is ready to receive (signalled by a CTS 1). After putting a byte on UART 1 data register, CTS signal does not immediately go down. We can consider our program as a state machine with the following states:
        1. Ready to send (CTS 1, TX up)
        2. Sent, CTS haven't lowered yet (CTS 1, just sent a char)
        3. Sent, CTS haven't lowered yet (CTS 1, just sent a char, TX up)
        4. Sent, CTS lowered (CTS 0)  
        We start in state 1, where CTS is 1 (high) and TX is up. This is the state we can send a byte. After sending a byte, we change to state 2. Shortly after, TX interupt should arrive, signalling UART1 is ready to transmit again, we will go to state 3. We then wait for CTS to be lowered to go to state 4. After CTS is up again (we would be notified by a notifier and we will check a UART register to see the value of CTS), we go back to state 1 and are ready to send another char.   
        We cycle these three states. When a user calls `Putc()`, we store the char in a queue, without blocking user. Whenever we are in state 1 and there are chars in the queue ready to be sent, we sent a char and adjust the state.



### Data structures

#### Path buffer for each train
- A ring buffer `path_node_buf` for `PathNode` is defined in `train.h`, it is how we store the path information for each train. `path_node_buf` contains only 2 types of track nodes: `NODE_BRANCH` and `NODE_SENSOR`. By having branch nodes in the buffer, we can store the desired direction for each node for each train without affecting other trains. By having the sensor nodes in the buffer, we will be able to know the "progress" of the train when it's completing its path.
- As a result, this buffer is used when switching the turnouts on demand

#### Location
- `Location` is defined in `track_node.h`, it is how we specify a location on the track.
- A location is just a landmark (any `track_node`) plus an offset in millimeter.
- This raises a question: what if I specify a location with a super long offet like `Location`(A4, 100000000)? This location will be useless because we have to trace 100000 meters from sensor A4 to figure out the exact location of this `Location`. We provide a `caliberate()` method that normalizes this location and travels as far as we can from the starting landmark without exceeding the offset. The end result is a `Location` that have minimal positive offset. In other words, after `caliberating`, the landmark will be the closest `track_node` to the desired location with a **positive** offset and the same direction with the input `track_node`.
- Some may wonder if a `Location `is unique. There could be two `Location`s (after calling `caliberate()`) that could point to the same location on the track. Consider this scenario: the track segment looks like `A -> x <- B`, where `A` and `B` are landmarks pointing to `x`, which is a location. Thus, `x` can be expressed as both `A` plus and offset or `B` plus an offset. We provide a method called `get_reverse_location()` that converts the location between these two representations.
#### Train
- `Train` is a class defined in `train.h`
- It is a model of a real life model train, we use this to store the status of a single train.
- It stores status like the current speed of the train, the velocity estimate of each speed level that is caliberated dynamically, the state of this train (for example, is it travelling to a destinaton), the last sensor it triggerd and when it triggered it, etc.
- It offers several helper functions that are very useful for stopping the train correctly:
- `bool attribute_sensor(track_node* sensor, const Track& track, unsigned int time_mm, unsigned int& delay)` is called whenever we attribute a sensor to this train (e.g., we will call this function for a train when it triggered a sensor). It does several tasks:
    - It compares the current time with the time the last sensor is triggered. We know the distance from last trasensorin to this train. With this, we can calculate the velocity. We then update the estimated velocity by taking an average of the old estimate and the new estimate we just calculated. We know that during the class, our Professor warned us about the potential problems of taking averages of velocities. We still went with this approach because in practice it works. We have tried the suggested approach (taking average of the time it takes to travel track segments), we found that it is also problematic in some ways (the data collected is too sparse). We will keep monitoring the performance of our approach and update our dynamic caliberation algorithm if it starts to display performance issues.
    - This function also updates the sensor trigger time prediction on the right side of the terminal. With the esitmated velocity calculated, it estimates the times it takes to get to next sensor (we calculate the distance to next sensor using last sensor the train just triggered). It also compares the last estimation with the actual time this sensor is triggered and update the display on the right section of the terminal with this difference.
    - Right now, sensor attribution is done in a crude way. The `tran_control` task keeps track of the current active train, by setting a `active_train_num` variable every time user calls a command with a particular train number. Thus, every time a sensor is triggered, `train_control` task calls this method of the `Train` object of current active train.
- `Location estimate_current_location(unsigned int cur_time_mm)` returns the current location of this train. We know the last sensor this train has triggered. We also know the time at which this method is called, as well as the estimated velocity, thus we can calculate the distance travelled from this sensor. So we can express the location as last sensor + offset. We also normalize the location so that the offset is minimal.
#### Track and track_node/edge
- Track is defined in `track.h`, it is our model or the train tracks.
- At system initialization, we will ask the user which track will be using and initialize the corresponding track by calling `init_tracka()` or `init_trackb()` provided by `track_data`.
- There are broken switches, to deal with them, we call `fix_tracka` or `fix_trackb` that changes the switches to a `NODE_MERGE` node with only one direction. An alternative may be asking the user which tracks are broken, but that might be too much overhead for each time we use our kernel.
- In addition to all the properties in the given `track_data` file, we also keep tracks of the direction of each branch. This is done by calling `set_branch()` every time we send a switch command, this method will update the `current_dir` member variable of a branch to the direction it is pointing to. It also changes the `last_sensor` of that train to the reverse node of `last_sensor` since our path finding algorithm is "direction" sensitive for now.
- The most important method that `Track` class provides is `unsigned int path_planning(Location& src, const Location& dest, unsigned int dist[TRACK_MAX], int parent[TRACK_MAX])`. It basically calls dijkstra from `src` to `dest` node and returns the path. See [path planning](#path-finding-new-in-tc1) part for more details.
- `track_node` and `track_edge` are based on the properties of a node and edge in the given `track_data` file. We added some helper functions like functions that returns the next node or next edge of this node on the track. Each track node can also be reserved, we are not untilizing this functionality at this moment. But it will be useful in TC2.
#### Queue
- Defined in `q.h`
- This is an implementation of a FIFO queue data structure. It is currently used to make the ready queues and the slab allocator.  
- The queue is implemented using a singly linked list, where each element (a node) contains a pointer to the next element. The queue keeps tracks of the head and tail. `push()` adds element after the tail, `pop()` removes and returns the element from the head. 

#### Ready Queues for scheduling with priorities
- Defined in `global.h` (`ready_queue_by_priority`).
- In each ready queue (implemented using `Queue`), each element is a task descriptor. We have one ready queue for each priority (we have 4 in total for now). The ready queues are stored in the array called `ready_queue_by_priority`, where `ready_queue_by_priority[i]` is the ready queue of all tasks of priority `i`. So to find the highest priority task that is ready, we just iterate from high to low priority and check if the corresponding ready queue has any element. If there is any, we pop the element (a task descriptor) from the head so it strictly follows FIFO. 

#### Slab Allocator
- Defined in `slab.h`. It is used to assign slabs to each tasks (i.e., used in creation of tasks).
- At the beginning of the program, we initialized a `SlabAllocator`, which keeps track a queue of free memory slabs. When someone requests a slab, the allocator will pop a slab from its queue.  
- To allow different objects to be put on the queue, we need to use type casting: as long as the first word in your object is a pointer to the next object, you can use this queue!

#### Task descriptor
- Defined in `td.h`. It stores some generic information about the task, including tid, parent tid, priority and state. Note that in our td, we have a memeber variable called `sp`, this is *not* referring to the saved stack pointer value of the task. It is simply a variable we use to keep track of the address where we store user task context.

#### Send-Receive-Reply structs
- Defined in `task_lib.h` (`SendParams`, `ReceiveParams`, `ReplyParams`). These structs are used to pass parameters from a user-level Send-Receive-Reply function to the kernel-level Send-Receive-Reply function.

#### Name server mapping
- A struct is defined in `task_lib.h` (`NameTidMapping`). It is used to save mappings in a name server. This struct has only two fields, the task id and its corresponding name (alias).
- A name server will initialize a buffer of `NameTidMapping`. With this buffer, the name server will be able to register task ids with different names and lookup task ids for different name.

#### Ordered link list for the clock server
- We want to put tasks with their desired awaken time (in ticks) into an ordered data structure. So that we can always have O(1) time to check if we need to awake any tasks because we only need to check the first element in the data structure. As a result, we created an ordered link-list for this purpose.
- The struct is defined in `task_lib.h` (`DelayedTask`). We implemented our ordered queue based on the existing datastructure of `Queue` . We provided special `push` and `pop` functions for the clock server so it's guaranteed that every `push` into the queue, the queue is ordered. The `pop` function is easy, it just check the head of the queue and pop the elements that should be awaken.

#### Ring Buffer
- Defined in `RBuf.h`
- This is an implementation of a FIFO buffer data structure.
- The buffer is implemented using a given array for memory access, where each element in the buffer is a specified (template) class / struct. The buffer keeps tracks of the start and end. `push_rbuf_item()` adds element at the `end`, `get_rbuf_item()` removes and returns the element from the `start`. 


### Critical system parameters and limitations
These parameters are all macro, which makes it very easily to modify. <u>But **remember**</u>, if we are going to modify the code, then please make sure it's under the limitations. Or at least change these limitations so the program can run properly.
- Maximal number of slabs:
    - Variable name (Macro): `SLAB_AMOUNT`
    - Value: 40
    - Variable location: `rtos-train/include/memconf.h`
    - Explanation:
        - We have at most 29 tasks right now so we need >= 29 slabs, 40 seems to be a fair number
        - We need to increase this value if we need more than 40 slabs (tasks)
- Size of a slab (size of memory for each task):
    - Variable name (Macro): `SLAB_SIZE`
    - Value: 250000 (~250kB)
    - Variable location: `rtos-train/include/memconf.h`
    - Explanation:
        - Currently we are giving ~250kB for each slab so it should be enough for everything we do in our tasks
- Number of clients (players):
    - Variable name (Macro): `NUM_CLIENTS`
    - Value: 6 + 12 = 18
    - Variable location: `rtos-train/include/global.h`
    - Explanation: 
        - This is a special value for deliverables, currently in tc2 (train control), there are exactly 6 clients + 12 train workers
        - If wish to modify the number of clients, then just modify this value will do. But be aware of the limitation of "Maximal number of tasks" (`NUM_TASK`)
- Maximal number of tasks:
    - Variable name (Macro): `NUM_TASK`
    - Value: 11 + NUM_CLIENTS = 29
    - Variable location: `rtos-train/include/global.h`
    - Explanation: 
        - idle task + first task + name server + clock server + clock notifier + UART_server + 5 uart notifiers + NUM_CLIENTS = 11 + 18 = 29
        - We need to modify this value if we add tasks
        - **Important!**: this value should not exceed the number of slabs (`SLAB_AMOUNT`) since we don't have reallocate system for slabs yet.
        - It's fine as long as the value is greater than the expecetd number of tasks
- Number of priorities:
    - Variable name (Macro): `NUM_PRIORITIES`
    - Value: 7
    - Variable location: `rtos-train/include/task_lib.h`
    - Explanation: 
        - We currently have 7 priorities: Initializer, Highest, High, Medium, Low, Lowest, and Idle. 
        - Initializer is the de facto highest priority. But it's only used for tasks that will initialize other tasks. This priority made sure that they won't be preempted before all other tasks are created. That being said, this priority can only be used in kernel since only kernel can create initializers.
        - The priority `Idle` is another special priority which should not be assigned to any user task. It is only used by the idle task.
- Maximal length of the name used in a name server:
    - Variable name (Macro): `TASK_NAME_LENGTH`
    - Value: 20
    - Variable location: `rtos-train/include/task_lib.h`
    - Explanation: 
        - The maximal length of a user task name that can be registered in the name server
        - longer names will be truncated
        - Note that, the actual length is `TASK_NAME_LENGTH - 1`, which is currently 19
- Maximal number of registered names:
    - Variable name (Macro): `NAME_SERVER_BUFFER_SIZE`
    - Value: 64
    - Variable location: `rtos-train/include/task_lib.h`
    - Explanation: 
        - The size of the buffer used in a name server. I.e., there can only be this many mappings from a name to a task id stored in one name server.
- Maximal size of train control system input:
    - Variable name (Macro): `TC_INPUT_BUF_SIZE`
    - Value: 30
    - Variable location: `rtos-train/include/trainControl.h`
    - Explanation: 
        - The input in the CLI provided by the train control system only allows input with this maximal length
- Maximal number of delayed commands for the train control system:
    - Variable name (Macro): `TC_DELAYED_COMMAND_BUF_SIZE`
    - Value: 30
    - Variable location: `rtos-train/include/trainControl.h`
    - Explanation: 
        - There can only be this many delayed command stored in the buffer at the same time
- Expected size of a groupd of commands for the train control system:
    - Variable name (Macro): `COMMAND_SIZE`
    - Value: 2
    - Variable location: `rtos-train/include/trainControl.h`
    - Explanation: 
        - Commands / instructions to the train are usually with at most this many bytes
- Interval to measure the sensor response time:
    - Variable name (Macro): `SENSOR_INTERVAL`
    - Value: 100
    - Variable location: `rtos-train/include/trainControl.h`
    - Explanation: 
        - Will measure and print the sensor response time every 100 ticks (1 second)
- Maximal number of sensor data stored to be printed:
    - Variable name (Macro): `SENSOR_BUF_SIZE`
    - Value: 10
    - Variable location: `rtos-train/include/trainControl.h`
    - Explanation: 
        - The number of triggered sensors information that will be stored and printed
- Minimal length for a valid path in tc1:
    - Variable name (Macro): `TC_DT_SAFE_DISTANCE`
    - Value: 400
    - Variable location: `rtos-train/include/trainControl.h`
    - Explanation: 
        - We don't have the measurements for accelerations yet, but we want to make sure the train is running in a constant speed, so we will always plan a path longer than a constant length so that the train will always reach a constant speed before reaching the target location.
- Minimal length for a path buffer for each train:
    - Variable name (Macro): `MAX_PATH_CAPACITY`
    - Value: 100
    - Variable location: `rtos-train/include/train.h`
    - Explanation: 
        - This is a buffer for `PathNode`s, basically a track node (only sensor and branch) with some other information. We have 80 sensors and 20 branches (with 2-4 broken switches), we will have at most 100 nodes in a path without a loop. Since our path finding algorithm guarantees that there are no loops in the output path, 100 seems to be a fair upper limit.

### other
- Kernel API:
    - Since user tasks should not be able to directly access kernel functions or variables, we created some user-level functions for user-level tasks to call. Basically, each user-level function will trigger interrupts and access the kernel-version of the same function and store the result in a safe location on the stack of the corresponding user task. E.g., a user task will call `MyTid()` first. Inside `MyTid()` it will trigger interrupt to the kernel mode. Then the kernel will call `k_MyTid()` and store the results for the user task.
- Assembly code in C:
    - Most of the code that helps to trigger and interruption and the code that handles the interruption (`svc_handler`) are written as assembly code blocks. This made sure that we take the whole control of how the system works underlying without worrying about the compiler.

## Performance measurement

The measurement results is in `performance.txt`.
### Method

Basically we created a receiver and a sender to conduct the measurements. Note that we will not parse the messages, i.e., as long as the process of send-receive-reply finshed properly, we would call it a round. This case, we will not count the time to do variable assignments.

The total time cost is taken as the amount of time for the sender to finish sending all the messages. We asked it to send `NUM_CYCLE` times, which is defined in `rtos-train/include/tasks.h` and is around 70000 - 80000. 

Note that, we also calculate a term call `error` so the final duration can cancel out with this `error` to make the measurement more accurate. The `error` is the amount of time to read the timer plus the amount of time to just run `NUM_CYCLE` cycles. Notice that the compiler optimization might optimize an empty for loop, so we added one line of assembly code to the loops so the loop used for measuring error won't be removed by the compiler optimizer. 

Then we can just simply measure the time for the receiver and sender to send-receive-reply for `NUM_CYCLE` times. (The final time measured minus the error we calculated) divided by `NUM_CYCLE` will be the average time cost for one send-receive-reply process. Since `NUM_CYCLE` is large enough (at least 70000) and the execution will usually take in between 2 s and 30 s, our final estimation (measurement) for one send-receive-reply process should be fair enough.

### Analysis

Comparisons
    - `noopt nocache` vs. `opt nocache`: compiler optimization will increase the speed exponentially
    - `noopt nocache` vs. `noopt cache`: cache will increase speed exponentially as well
    - `opt nocache` vs. `noopt cache`: the benefit of using cache is way more than using the compiler optimization
    - `noopt nocache` vs. `opt nocache`: using compiler optimization and using cache don't conflict, the result is the best among all
    - `Receiver first` vs. `Sender first`: Receiver first is faster than Sender first, this is reasonable since there are extra steps to do if sender sends before the receiver is ready

### Conclusion

Since the difference between `noopt nocache` and `noopt cache` is the largest, a reasonable conclusion is that the memory access (read/write) costs the most time in our program. This is reasonable since the memory access in our measurement program are always to the same locations. As a result, the cache will definitely save a lot of time in this situation.


## Calibration information collection and processing
We need to collect some data manually in order to stop the train at a certain location accurately. For example, we need the stopping distance and speed of each train, because the actual speed of the train varies at the same speed level. For this purpose, we collected two types of data:
- Stopping distance of each train (10 train types in total) at each speed level (11 functional speed levels)
- Actual speed of each train at each speed level (including different sub-cases)

Note: The speed of a train in the same speed level can also vary depending on whether the train speed is switched from a higher speed level or a lower speed level. This is what I mean by sub-cases.   
For example, train A has a speed level of 10 and then it switches to speed level 5. Its actual speed will be different from train A running at speed level 0 and then switching to speed level 5.

We also allow dynamic speed calibration so the speed data will be more accurate while the program and the trains are running. For dynamic calibration:

- For velocity, we use the data we manually gathered to seed the initial values for velocities when the program starts. The velocity then is calculated dynamically for each running train on the track.
- For stopping distance, we also use the data we manually gathered to seed the initial values. While we were testing our trains by commanding some trains to stop at random locations on the track using `dt` command, a few velocities of some trains have a consistent error, so we re-caliberated these speed levels specifically by discounting the error in our stopping-distance cliberations.

The raw caliberation data can be found in the measurements folder.

## Output explanation of the unit-tests of the kernel

### k3

Execute the program following the descriptions in [run program](#operate-the-executable). Note that, you will need to modify `main.cpp` to make sure that only the deliverable part for `k3` is uncommented. I.e., uncomment this part
```
g_idle_task_tid = k_Create(ETP_Idle, task_idle);
k_Create(ETP_Initializer, task_first_task_k3);
```

Note that, everytime a task exits, we will print the percentage of cpu idle time so far.  

Output:
```
Idle: 0%
Tid: 5. Delay: 10. Completed: 1 delays.
Tid: 5. Delay: 10. Completed: 2 delays.
Tid: 6. Delay: 23. Completed: 1 delays.
Tid: 5. Delay: 10. Completed: 3 delays.
Tid: 7. Delay: 33. Completed: 1 delays.
Tid: 5. Delay: 10. Completed: 4 delays.
Tid: 6. Delay: 23. Completed: 2 delays.
Tid: 5. Delay: 10. Completed: 5 delays.
Tid: 5. Delay: 10. Completed: 6 delays.
Tid: 7. Delay: 33. Completed: 2 delays.
Tid: 6. Delay: 23. Completed: 3 delays.
Tid: 5. Delay: 10. Completed: 7 delays.
Tid: 8. Delay: 71. Completed: 1 delays.
Tid: 5. Delay: 10. Completed: 8 delays.
Tid: 5. Delay: 10. Completed: 9 delays.
Tid: 6. Delay: 23. Completed: 4 delays.
Tid: 7. Delay: 33. Completed: 3 delays.
Tid: 5. Delay: 10. Completed: 10 delays.
Tid: 5. Delay: 10. Completed: 11 delays.
Tid: 6. Delay: 23. Completed: 5 delays.
Tid: 5. Delay: 10. Completed: 12 delays.
Tid: 5. Delay: 10. Completed: 13 delays.
Tid: 7. Delay: 33. Completed: 4 delays.
Tid: 6. Delay: 23. Completed: 6 delays.
Tid: 5. Delay: 10. Completed: 14 delays.
Tid: 8. Delay: 71. Completed: 2 delays.
Tid: 5. Delay: 10. Completed: 15 delays.
Tid: 5. Delay: 10. Completed: 16 delays.
Tid: 6. Delay: 23. Completed: 7 delays.
Tid: 7. Delay: 33. Completed: 5 delays.
Tid: 5. Delay: 10. Completed: 17 delays.
Tid: 5. Delay: 10. Completed: 18 delays.
Tid: 6. Delay: 23. Completed: 8 delays.
Tid: 5. Delay: 10. Completed: 19 delays.
Tid: 7. Delay: 33. Completed: 6 delays.
Idle: 93%
Tid: 5. Delay: 10. Completed: 20 delays.
Idle: 93%
Tid: 6. Delay: 23. Completed: 9 delays.
Idle: 93%
Tid: 8. Delay: 71. Completed: 3 delays.
Idle: 93%
```

Idle time explanation:
- We can see that the first idle percentage is 0. This is when the first user task quits, since it's never blocked by anything, this percentage is 0 is making sense. The first user task has created all other tasks (except the idle task). As a result, after that line of `Idle: 0%`, the client tasks are going to print their own lines. 
The last few idle metrics are `Idle: 93%`, this is the idle time summed up until each client task exits. This is a good number proving that our cpu spent most of its time doing nothing, which is expected for this deliverable.  

Task id explanation
- The first client task has a tid of 5 is because we have other tasks created before it:
    1. The idle task (not exactly a regular user task)
    2. The first user task
    3. The name server
    4. The clock server

    Hence, the four clients are having the tids of 5,6,7, and 8. 

Proving the output is correct:
- To understand if the output is correct, we can label each line to find out how many ticks all the client tasks have been waited so far (at this line).  
The way to label it is by multiplying the `delay interval` and the `number of delays`. If we get a list of increasing numbers, then it means the sequence is in the correct order.
    ```
    Tid: 5. Delay: 10. Completed: 1 delays.     // 10 ticks
    Tid: 5. Delay: 10. Completed: 2 delays.     // 20 ticks
    Tid: 6. Delay: 23. Completed: 1 delays.     // 23 ticks
    Tid: 5. Delay: 10. Completed: 3 delays.     // 30 ticks
    Tid: 7. Delay: 33. Completed: 1 delays.     // 33 ticks
    Tid: 5. Delay: 10. Completed: 4 delays.     // 40 ticks
    Tid: 6. Delay: 23. Completed: 2 delays.     // 46 ticks
    Tid: 5. Delay: 10. Completed: 5 delays.     // 50 ticks
    Tid: 5. Delay: 10. Completed: 6 delays.     // 60 ticks
    Tid: 7. Delay: 33. Completed: 2 delays.     // 66 ticks
    Tid: 6. Delay: 23. Completed: 3 delays.     // 69 ticks
    Tid: 5. Delay: 10. Completed: 7 delays.     // 70 ticks
    Tid: 8. Delay: 71. Completed: 1 delays.     // 71 ticks
    Tid: 5. Delay: 10. Completed: 8 delays.     // 80 ticks
    Tid: 5. Delay: 10. Completed: 9 delays.     // 90 ticks
    Tid: 6. Delay: 23. Completed: 4 delays.     // 92 ticks
    Tid: 7. Delay: 33. Completed: 3 delays.     // 99 ticks
    Tid: 5. Delay: 10. Completed: 10 delays.    // 100 ticks
    Tid: 5. Delay: 10. Completed: 11 delays.    // 110 ticks
    Tid: 6. Delay: 23. Completed: 5 delays.     // 115 ticks
    Tid: 5. Delay: 10. Completed: 12 delays.    // 120 ticks
    Tid: 5. Delay: 10. Completed: 13 delays.    // 130 ticks
    Tid: 7. Delay: 33. Completed: 4 delays.     // 132 ticks
    Tid: 6. Delay: 23. Completed: 6 delays.     // 138 ticks
    Tid: 5. Delay: 10. Completed: 14 delays.    // 140 ticks
    Tid: 8. Delay: 71. Completed: 2 delays.     // 142 ticks
    Tid: 5. Delay: 10. Completed: 15 delays.    // 150 ticks
    Tid: 5. Delay: 10. Completed: 16 delays.    // 160 ticks
    Tid: 6. Delay: 23. Completed: 7 delays.     // 161 ticks
    Tid: 7. Delay: 33. Completed: 5 delays.     // 165 ticks
    Tid: 5. Delay: 10. Completed: 17 delays.    // 170 ticks
    Tid: 5. Delay: 10. Completed: 18 delays.    // 180 ticks
    Tid: 6. Delay: 23. Completed: 8 delays.     // 184 ticks
    Tid: 5. Delay: 10. Completed: 19 delays.    // 190 ticks
    Tid: 7. Delay: 33. Completed: 6 delays.     // 198 ticks
    Tid: 5. Delay: 10. Completed: 20 delays.    // 200 ticks
    Tid: 6. Delay: 23. Completed: 9 delays.     // 207 ticks
    Tid: 8. Delay: 71. Completed: 3 delays.     // 213 ticks
    ```
- We can be sure that the output sequence is correct since the list is indeed increasing.  
Additionally, from the result, we can also see that each clients has printed the desired number of lines. As a result, we know the Send-Receive-Reply transaction have successfully passed the arguments to the clients.


### k2
Execute the program following the descriptions in [run program](#operate-the-executable). Note that, you will need to modify `main.cpp` to make sure that only the deliverable part for `k2` is uncommented. I.e., uncomment this part
```
k_Create(ETP_Medium, task_first_task_k2);
```

Before showing the output of the execution for k2, I would like to introduce some basic information:
1. We currently decided to have <u>*6*</u> clients. Note that you can change this number by changing the macro for <u>*Number of clients*</u> described in [system parameters](#critical-system-parameters-and-limitations). 
2. Each player will play a different number of turns, the number is a random number in [1,10]. So it's possible for some players to quit before their opponents does. In this case, the player didn't quit will quit right away once it found out its opponent quitted (i.e., as soon as the processor is given to it after its opponent quitted).
3. Every choice a player made is generated as a random number. In other words, it's not hard-coded.

Therefore, <u>**the output may be different each time it is executed**</u>. So this part is only going to explain some of the potential outcomes.

General process for a player:
1. Player signs up.
    - The next player that signs up will be paired up with this player
2. Player play random amount of games.
    - If the other player paired up with this player (opponent) quitted, then this player will quit right away (when it's its turn to use the processor)
3. Player quits.
    - Player does not sign up again.

Execute the program following the descriptions in [run program](#operate-the-executable).
We will see the following:
```
FirstUserTask: exiting

[Press any key to continue]
```
We can see that the first task had already exited. This means all the other tasks have already been created. This includes 1 task for the Name server, 1 task for the RPS server, and 6 client tasks that play the RPS game.  
There is a pause afterwards in the command line and it's waiting for user to press a key to continue. This is asked from the requirements of `k2` so the user can clearly see what's happening.

Once the user pressed anything, it's possible to see the following output:
```
FirstUserTask: exiting

Client 4 played Rock. Reply: Lose 
Client 5 played Paper. Reply: Win 

[Press any key to continue]
```
The choices players made are displayed with their task ids. The `Reply` part means whether this player won, lost, or it's a tie.  
Keep pressing for a few times, we could probably see:
```
Client 6 played Scissor. Reply: Tie 
Client 7 played Scissor. Reply: Tie 

Client 8 played Rock. Reply: Lose 
Client 9 played Paper. Reply: Win 

Client 5 played Paper. Reply: Lose 
Client 4 played Scissor. Reply: Win 

Client 7 played Rock. Reply: Tie 
Client 6 played Rock. Reply: Tie 

Client 9 played Scissor. Reply: Lose 
Client 8 played Rock. Reply: Win 
Client 5 quitted before its opponent, its opponent is 4.
Client 7 quitted before its opponent, its opponent is 6.
```
So we can see that the players have played for several rounds now, and there are 3 pairs of players that are playing concurrently. (4 and 5, 6 and 7, 8 and 9)  
The results are straightforward. Scissor vs. Scissor is a tie. Scissor vs. Rock is a lose...  
Note that player 5 and 7 quitted at the end and their opponents are also printed. This means, when task 4 and 6 got their turns to act, then they will quit as well.  
Keep pressing keys:
```
Client 8 played Scissor. Reply: Win 
Client 9 played Paper. Reply: Lose 
Client 4 quitted after its opponent.
Client 6 quitted after its opponent.
```
We can see that player 8 and 9 are still playing. Player 4 and 6 quitted after their opponents, which are 5 and 7 respectively.  
Keep pressing:
```
Client 9 played Rock. Reply: Win 
Client 8 played Scissor. Reply: Lose 
Client 8 quitted before its opponent, its opponent is 9.
Client 9 quitted after its opponent.
```
Now, player 8 and 9 also quitted and the program ends here.  
From the output, we can have some trivial analysis:
- player 4 and 5 have played 2 rounds
- player 6 and 7 have played 2 rounds
- player 8 and 9 have played 4 rounds
- player 5 and 7 only wished to play 2 rounds
- player 8 wished to play 4 rounds
- player 4, 6, 9 quitted because their opponents "surrendered"

---
### k1
Execute the program following the descriptions in [run program](#operate-the-executable). Note that, you will need to modify `main.cpp` to make sure that only the deliverable part for `k1` is uncommented. I.e., uncomment this part
```
k_Create(ETP_Medium, task_first_task_k1);
```
We should see the following output after executing the program:
```
Created: 2
Created: 3
Task id: 4, Parent tid: 1
Task id: 4, Parent tid: 1
Created: 4
Task id: 5, Parent tid: 1
Task id: 5, Parent tid: 1
Created: 5
FirstUserTask: exiting
Task id: 2, Parent tid: 1
Task id: 3, Parent tid: 1
Task id: 2, Parent tid: 1
Task id: 3, Parent tid: 1
```

This part will explain the output line by line:  
1. Line 1: The first user task, `FirstUserTask`, will create two tasks with lower priorities. As a result, it will first create a task with a tid of 2. Note that, we assign task ids consequently, so 2 here is making sense since the first user task with id of 1 is the `FirstUserTask` itself.  
The current running task is not switched to other tasks is because this task with tid of 2 has a lower priority than the `FirstUserTask`. Hence, even if the system re-scheduled tasks, the current task will remain to be the `FirstUserTask` (tid 1).
2. Line 2: Same as above. Only difference is that the task id should be 3 now.
3. Line 3: The first user task, `FirstUserTask`, will create two tasks with higher priorities. At this point, a task with higher priority should have been created. However, right after it's created, the processor will be given to that task. In other words, the current running task will be switched to this new task. 
The tid of this new task should be 4 since we already have tasks with tid of 1, 2 and 3 at this point.  
It printed the task information as all other tasks should (task id and parent task id). Since it's created by the `FirstUserTask`, which has a tid of 1, its parent tid should be 1.
4. Line 4: We can see that the same information is printed twice. this is because at this moment, task 4 has the highest priority and it's the only task with that priority. As a result, only task 4 can run.
5. Line 5: Task 4 has finished its execution and task 1 gets to run next. Now the system is just returned from the function `Create()`, so we should print the information about the task it created, i.e., task 4.
6. Line 6-8: Same as Line 3-5. Only difference is that the task id should be 5 now.
7. Line 9: Now the processor is given back to the `FirstUserTask` since all tasks with higher priorities finished executions. The job of the `FirstUserTask` is to create 4 tasks, so it has also done its execution. Hence, it printed the last string, i.e., "FirstUserTask: exiting".
8. Line 10: Now only task 2 and 3 with the same priorities are in the ready queue. Since task 2 is created before task 3, it will be at the head of the ready queue. As a result, the processor will be given to task 2.
9. Line 11: The second instruction in task 2 is to give up the processor. This will move task 2 to the back of the ready queue. As a result, the processor will be given to task 3 now.
10. Line 12-13: Task 3 will also give up the processor, so these two lines are just repeating what happened in line 10-11.
11. End: Task 2 exited on line 12 and task 3 exited on line 13. So there is nothing more to print now and the program exited.


## Citation
* Our `.gitignore` file is copied from a standard gitignore file for c: https://github.com/github/gitignore/blob/main/C.gitignore  
* Our simple queue implementation (q.h and q.cpp) is based on a similar queue implementation we wrote for se350.  
* For physics engine simulation, we need to use some math liberies like square root, finding root of quadratic formula, they are included in `src/math_lib.cpp` and the citations are included in this file as well.
