#ifndef MATH_LIB_H
#define MATH_LIB_H

namespace math_lib
{
    int integer_sqrt_iter(int n);
    int solve_quadratic_formula(int a, int b, int c);
}

#endif