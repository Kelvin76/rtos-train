#ifndef INTERRUPT_H
#define INTERRUPT_H

void interrupt_init();
void enable_uart_2_timeout();
void disable_uart_2_timeout();
void enable_uart_tx_interrupt(int channel);
void disable_uart_tx_interrupt(int channel);
void enable_uart_1_CTS_interrupt();
void disable_uart_1_CTS_interrupt();
int get_cts_status();
void confirm_cts_interrupt();
bool is_uart_1_CTS_interrupt_enabled();
void enable_uart_1_rx_interrupt();
void disable_uart_1_rx_interrupt();
void halt();
extern "C" {
    void enable_halt();
}
#endif