/*
 * io_lib.h, needs the creation of uart server first, so this can be functional
 */


#ifndef IOLIB_H
#define IOLIB_H

extern "C"
{
    // import c libs here
    #include "bwio.h"
}

int k_setfifo( int channel, int state );
int k_setspeed( int channel, int speed );


// ---- wrapper / helper functions for Putc and Putstr (support formating) ----

// printf used by user tasks, support multiple concurrent tasks
void printf( int channel, char *format, ... );
void va_printf( int channel, char *format, va_list args );

// only supports single-thread mode (does not work for concurrent tasks)
// i.e., if we only have one "writer" to uart, it's ok
void cprintf( int channel, char *format, ... );
void cprintf( int uart_tid, int channel, char *fmt, ... );
void va_cprintf( int channel, char *format, va_list args );

#endif
