// #ifndef DEBUG_MEM    // all mem/slab allocation related printouts
// #define DEBUG_MEM
// #endif

// #ifndef DEBUG_TASK   // all task related printouts
// #define DEBUG_TASK
// #endif

// #ifndef DEBUG_TASK_AE   // all user tasks printouts
// #define DEBUG_TASK_AE
// #endif

// #ifndef DEBUG_TASK_NS   // name server stuff
// #define DEBUG_TASK_NS
// #endif

// #ifndef DEBUG_TASK_IDLE // idle time in k1 - k3
// #define DEBUG_TASK_IDLE
// #endif


// LOG macros for Train control system

// #ifndef DEBUG_ROUTING_PLAN // print route planned
// #define DEBUG_ROUTING_PLAN
// #endif

// #ifndef DEBUG_ROUTING // print route planned
// #define DEBUG_ROUTING
// #endif

// #ifndef DEBUG_LOCATION_ESTIMATION // print out estimated location
// #define DEBUG_LOCATION_ESTIMATION
// #endif

// #ifndef DEBUG_SOLVE_ROOT
// #define DEBUG_SOLVE_ROOT
// #endif

// #ifndef DEBUG_TRAIN_COMMAND
// #define DEBUG_TRAIN_COMMAND
// #endif

// #ifndef DEBUG_RESERVATION
// #define DEBUG_RESERVATION
// #endif

// #ifndef DEBUG_SENSOR_ATTRIBUTION
// #define DEBUG_SENSOR_ATTRIBUTION
// #endif

// #ifndef DEBUG_DESTINATION
// #define DEBUG_DESTINATION
// #endif

// #ifndef DEBUG_UART_BUF
// #define DEBUG_UART_BUF
// #endif


// Some extra information

// #ifndef DT_INFO
// #define DT_INFO
// #endif

#ifndef SENSOR_MEASUREMENT
#define SENSOR_MEASUREMENT          // used to print sensor measurement info
#endif

#ifndef TRAIN_SYSTEM_ON             // diff from kernel assignments
#define TRAIN_SYSTEM_ON          
#endif

#ifndef GLOBAL_H
#define GLOBAL_H

#include "slab.h"
#include "td.h"
#include <cstdint> // uintptr_t
#include "config.h"
#include "task_lib.h"

extern SlabAllocator slab_allocator;

extern char __bss_start, __bss_end;                    // defined in linker script
extern uintptr_t __init_array_start, __init_array_end; // defined in linker script
typedef void (*funcvoid0_t)();

#define inRange(value, low, high) (value >= low && value <= high) 

// deliverable - k2
// #define NUM_CLIENTS 6   // k2 has 6 clients
// #define NUM_CLIENTS 4   // k3 has 4 clients
#define NUM_DELAY_WORKER 2*6 // each train needs delay reverse and stopping, that's two functionalities

#define NUM_CLIENTS 6 + NUM_DELAY_WORKER   // k4 (train control) has 6 tasks

// ---- tasks related ----

// Exception stack
#define SP_OFFSET 15    // offset between mem loc for PC and the value of SP

// TD management
extern TD *g_cur_task_td;
extern u32 *g_cur_space;

/*
idle task + first task + name server + clock server + clock notifier + UART_server + 5 uart notifiers + clients
*/
#define NUM_TASK 11 + NUM_CLIENTS   // need to be smaller than SLAB_AMOUNT - 1 (39) since train_control will have double the space
extern TD *g_task_pool[NUM_TASK];
extern int g_task_pool_size;       // the size of the task pool
extern int tid_counter;

extern Queue ready_queue_by_priority[NUM_PRIORITIES];

// kernel variables
extern int *g_kernel_sp;
extern void* redboot_return_addr;

#define TIMER_MAX 4294967295
/*
508.4689 kHz = 508468.9 per second = 5084.689 per 10ms
*/
#define TIMER_10MS_TICKS 5085

extern "C" {
    void _my_exit();
}

void assert(bool a, char* str = (char*)"");

extern int name_server_tid;
extern int g_idle_task_tid;
extern int g_uart_server_tid;
// system measurement helper variables
extern unsigned int g_idle_task_start_at; // start at, in actual hw timer value
extern unsigned int g_idle_task_end_at; // end at, in actual hw timer value
extern unsigned int g_idle_task_running_time; // running time of idle task, in actual hw timer value

extern unsigned int g_ticks;

extern Queue g_event_queue_by_event[NUM_EVENTS];

// limits
#define U_INT_MAX 4294967295

#endif