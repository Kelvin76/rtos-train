// tasks.h includes all the tasks (tasks are functions with their own stack space)

#ifndef TASK_CODE_H
#define TASK_CODE_H

// tasks for testings
void test_task_cs();

void test_receiver_1();
void test_sender_1();

void test_receiver_2();
void test_sender_2();

void test_sender_3();

void test_sender_truncated();
void test_receiver_truncated();

void test_reply_non_existing_task();
void test_reply_non_blocked_task();

void test_multiple_sender_1();
void test_multiple_sender_2();
void test_multiple_sender_receiver();

void test_register_name();
void test_who_is();

void task_test_1();
void task_test_2();

void test_receiver_do_not_receive();
void test_sender_do_not_receive();

void test_receiver_receiver_quitted();
void test_sender_receiver_quitted();

void test_await_event();

// timer test
void test_timer_and_Ticks();

void test_task_time();
void test_task_delay();
void test_task_delayUntil();

void test_task_printf_u();
void test_task_printf_m();

void test_halt();

// -- special task --
void task_idle();

// ---- deliverables for kernel assignments ----

// -- k1 --
void task_first_task_k1();


// -- k2 --
// for the rps games
void task_first_task_k2();

// for measurements
void task_measurement_receiver();
void task_measurement_sender();

// Get result for testing in around [10-20] s

#define MEASUREMENT_BUFFER_SIZE 4
#define NUM_CYCLE 80000             

// #define MEASUREMENT_BUFFER_SIZE 64
// #define NUM_CYCLE 72000

// #define MEASUREMENT_BUFFER_SIZE 256
// #define NUM_CYCLE 70000


// -- k3 --
void task_first_task_k3();

// -- k4 --
void task_first_task_k4();
#endif
