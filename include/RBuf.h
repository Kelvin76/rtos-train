#ifndef RBuf_H
#define RBuf_H
#include "global.h"
// ref: based on the ring buffer I wrote for a0
// why we should define templated class in header file: 
// https://stackoverflow.com/questions/8752837/undefined-reference-to-template-class-constructor
// https://stackoverflow.com/questions/495021/why-can-templates-only-be-implemented-in-the-header-file
template <class T>
class RBuf {
	T* arr;
	int end;
	int start;
	int count;
	int capacity;
public:
	RBuf(T* array, int capacity) : arr{array}, end{0}, start{0}, count{0}, capacity{capacity} {};
	bool push_rbuf_item(T item) { // return operation success
		if (count < capacity) {
			arr[end++] = item;
			end %= capacity;
			count++;
			return true;
		}
		return false;
	};
	bool push_front_rbuf_item(T item) { // return operation success
		if (count < capacity) {
            start = (start > 0)? start - 1 : capacity - 1;
			arr[start] = item;
			count++;
			return true;
		}
		return false;
	};
	bool is_buf_full() {
		return count == capacity;
	};
	bool is_buf_empty() {
		return count == 0;
	};
	void pop(){
		if (count > 0) {
			start = (start+1) % capacity;
			count--;
			return;
		}
		assert(false);
		return;
	}

	T pop_rbuf_item() {
		if (count > 0) {
			T item = arr[start++];
			start %= capacity;
			count--;
			return item;
		}
		assert(false, (char*)"Rbuf empty when popping");
		// since 0 won't make sense for some type and
		// code will never reach here due to the assert
		// return a default value
		// seems like common practice from: https://stackoverflow.com/questions/5300602/template-return-type-with-default-value
		T t;
		return t;
	};

	T pop_back() {
		if (count > 0) {
			T item = arr[end--];
			end %= capacity;
			count--;
			return item;
		}
		assert(false);
		return 0;
	};

	T peak_back() {
		if (count > 0) {
			return arr[end];
		}
		assert(false);
		return 0;
	};

	int size() {
		return count;
	}
	int get_start(){
		return start;
	}
	int get_end(){
		return end;
	}
	void clear() {
		end = 0;
		start = 0;
		count = 0;
	}
    bool get_next_index(int &index){
        index = (index+1) % capacity;
        if(index == end) return true;   // at the end
        return false;
    }
	T& operator[](int ind){ return arr[ind]; }
};

#endif
