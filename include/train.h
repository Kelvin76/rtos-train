#ifndef TRAIN_H
#define TRAIN_H

#include "track_node.h"
#include "track.h"
#include "RBuf.h"
#include "io_lib.h"


// 15 speed levels, two granular speed each. two slots not used. In mm.
#define STOP_DISTANCE_ARR_SIZE 30
#define VELOCITY_ARR_SIZE 30
#define NUMBER_OF_SPEED_LEVELS 15 // 0-14 inclusive
#define LOWEST_FUNCTIONAL_SPEED 7
#define TOP_NORMALIZED_SPEED 14
#define LOW_SPEED 7
#define HI_SPEED 12

#define NUM_RESERVABLE_NODES (NUM_SENSORS + NUM_BRANCH_PLUS_MERGE)

#define MAX_PATH_CAPACITY 100    // at most 80 sensors with the same direction + 20 branches (ignore the broken ones)

#define NUM_SENSOR_LOOK_AHEAD 3 // how many sensors should a train look a head for

#define DEAD_LOCK_CHECK_CYCLE 4         // 4*0.4=1.6s   
#define RANDOM_WAIT_TIME 5              // 5*0.4=2s       SENSOR_LOOP_CYCLE_PERIOD

/*
    This is a class for us to model a train

    In order for the model to be precise, the control of the train must follow some rules:
    1. The train will only move at speed levels 0, 7, 12, this is because mutiple speed levels
    will need exponential number of acceleration measurements. If we focus on 0, 7, 14 only,
    then we only need to measure the aceleration of 
        0 -> 7
        0 -> 12
        7 -> 12
        12 -> 7
    The train have a physical model snapshot that consists of velocity, acceleration, timestamp and location,
    which represents the velocity, acceleration and location of a train at a timestamp.
    The location is caliberated by sensor attribution. Evertime this train hits a sensor, we update the location to be 
    (sensor, 0).

    IMPORTANT: we need to be cautious when predicting distance that will be travelled or time that will 
    be taken to travel some distance using the physical state of the train. The reason being the 
    the physical state only shows a snapshot of the train's state. If we are planning to change 
    the speed level of the train during the prediction timeframe, we need to take that into predication.
*/
class Train {
    // contains status for each individual train
public:
    bool lightOn;
    int reserved_num;
    enum class GranularSpeed {
        low,
        high, // two speed levels within the same speed
        // for speed 0, it is always high
        // for speed 14, it is always low
    };
    struct PhysicalStateSnapshot{
        unsigned int v; // 4 fixed point
        int a; // 8 fixed point
        Location location; // caliberated by attribute_sensor
        unsigned int last_refresh_time_mm;
        PhysicalStateSnapshot();
    };
    // technically, can only send normal commands to the train in stand by
    // however, won't force this because there are so many special case
    // in fact, we will make sure the states are changed back to standby asap when the train can be modified
    enum class State {
        WaitingForOthers,   // blocking on other trains, train is pasued
        StandBy,            // train is stopped
        TravellingToDest,   // train is running to its destination
        RegisteredToStop,   // train will be stopped very soon
        Reversing,          // train is reversing 
        FreeRunning,        // train is running freely (not supported by track reservations)
    };
    track_node* waitingOnNode;     // only useful when state == WaitingForOthers

    struct NextSensorExpection{
        track_node* next_sensor;
        // TODO: add timeout

        NextSensorExpection();
    };

    PhysicalStateSnapshot get_current_physical_state_snapshot(bool refresh = false); // making it public for debugging purpose
    Location get_current_estimated_location();

    enum class EReserveResult{
        No_Collision,
        Collision_Same_Direction,
        Collision_Opposing_Direction
    };

    struct PathNode {
        track_node *node;
        NodeDir dir;
        PathNode(track_node *node = nullptr, NodeDir dir = DIR_AHEAD):node(node), dir(dir){}
        NodeType get_node_type(){return (nullptr == node)? NODE_NULL: node->type;}
        int reserved_by(){return (nullptr == node)? 0: node->reservedBy;}
    };

    Train();
    void set_train_number(int train_number);
    int get_train_number();
    void set_need_wait_time(bool is){need_wait = is;};
    void reset_random_wait_counter(){random_waited_counter = 0;};
    void increment_random_wait_counetr(){random_waited_counter++;};
    bool is_random_wait_counter_enough(){return (!need_wait || (random_waited_counter >= RANDOM_WAIT_TIME));};
    bool is_random_player(){return Track::RandomVersion::na != random_version;};
    void set_random_version(Track::RandomVersion version){ random_version = version;};
    Track::RandomVersion get_random_version() { return random_version;};

    // get/set train properties
    unsigned int set_speed(unsigned int target_speed);  // return time required for reaching constant velocity in ms
    unsigned int get_speed() const;
    unsigned int get_normalized_speed() const;
    GranularSpeed get_granular_speed();
    void set_activation_time(unsigned int activation_time);
    unsigned int get_activation_time();
    void reverse_heading_direction();
    bool get_heading_direction();
    State get_train_state();
    void set_train_state(State s);
    void clear_location();

    // velocity and stop_distance caliberation
    void seed_stop_distance();
    void seed_velocity();
    void seed_accleration();

    // track reserve
    void reserve_tracks(void* ptr, track_node* target_node);
    void clear_reserved_track_on_the_back(track_node* sensor);
    void set_detination(Location &loc);
    void clear_destination();
    void train_stopped(void* tc);
    bool destination_exists(){return destination.exists();};
    EReserveResult front_tracks(void* ptr, track_node *&node);
    void reserve_track_in_the_front(void* tc);
    void clear_path_buf();
    void clear_reserve(Track &track);
    unsigned int time_to_sensor(track_node* target, int &distance, unsigned int speed = U_INT_MAX);
    bool will_slow_down_solve_conflicts(Train* other_train, track_node* target, bool sameDirection);

    // sensor attribution
    bool attribute_sensor(void* tc, track_node* sensor, Track& track, unsigned int time_mm, unsigned int& delay, bool is_register);
    track_node* get_last_sensor();
    unsigned int get_last_sensor_set_time();
    
    // distance/time calculation
    unsigned int get_estimated_time_for_next_sensor();
    unsigned int estimate_time_ms(unsigned int distance_mm, unsigned int speed = U_INT_MAX);
    int estimate_distance_mm(int time_diff_ms);
    int calc_sensor_trigger_estimation_time_diff_with_actual(unsigned int cur_time);
    unsigned int get_stop_distance();
    unsigned int get_stop_distance(unsigned int new_normalized_speed);
    unsigned int get_accelerate_distance(unsigned int new_normalized_speed);
    int is_dynamic_stop_possible(unsigned int d);
    int get_max_speed_for_distance(unsigned int distance);
    void stop_at(Location stop_sensor);

    unsigned int get_path_distance();
    int path_safe_speed(int dist_off);

    // path planning and track reservation
    PathNode path_node_arr[MAX_PATH_CAPACITY];
    RBuf<PathNode> path_node_buf;

    Location destination;

    // delayed command stuff
    int delayed_command_setspeed_worker_tid;
    int delayed_command_setspeed_speed;

    int delayed_command_reverse_worker_tid;
    int delayed_command_reverse_speed;

private:
    int target_speed;
    int clock_tid;
    // train properties -- digital
    int train_number;
    unsigned int speed; // default 0
    unsigned int normalized_speed; // default 0
    GranularSpeed granular_speed; // default high
    unsigned int activation_time; // default 0
    State state;
    bool need_wait;
    Track::RandomVersion random_version;
    int random_waited_counter;

    void set_waiting(void* ptr, track_node* node = nullptr);
    void set_train_moving(void* ptr, int speed);

    // train properties -- physical
    PhysicalStateSnapshot snapshot;
    void refresh_physical_state_snapshot(); // update physical properties
    void update_current_physical_state_snapshot_accleration(unsigned int new_accleration);
    void caliberate_current_physical_state_snapshot_location(track_node* lm, int offset_mm);
    void reverse_current_physical_state_snapshot_location();


    // default true (1), define direction true (1) as the direction pointed by train's number sticker
    bool heading_direction;
    unsigned int stop_distances[STOP_DISTANCE_ARR_SIZE];
    unsigned int velocities[VELOCITY_ARR_SIZE]; // mm/ms, fixpoint of 4, rg 1051 represents 0.1051
    unsigned int get_converged_velocity() const;
    unsigned int get_converged_velocity(unsigned int new_normalized_speed) const;
    Location stop_sensor;

    // sensor attribution status
    unsigned int estimated_time_for_next_sensor;
    void estimate_time_for_next_sensor(unsigned int next_sensor_dist);
    track_node* last_sensor;
    unsigned int last_sensor_set_time;

    // acceleration adta
    struct AccelerationMeasurements {
        // d can be calculated from a during runtime
        // but calculating them here is much more easier
        unsigned int a; // 8 digit fix point
        unsigned int d; // acceleration distance, in mm
        AccelerationMeasurements();
    };

    AccelerationMeasurements zero_to_speed_acceleration[NUMBER_OF_SPEED_LEVELS];   
    int speed_to_zero_acceleration[NUMBER_OF_SPEED_LEVELS];
    AccelerationMeasurements a_7_to_12;
    AccelerationMeasurements a_12_to_7;
    
    void dynamic_caliberation(track_node* last_sensor, track_node* this_sensor, const Track& track, unsigned int time_passed_mm);
};

#endif