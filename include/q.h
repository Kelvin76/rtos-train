#ifndef Q_H
#define Q_H
/*
        head    tail
        |       |
pop<-  [a, b, c] <- push
count = 3
*/

class Queue {
    public:
        class Node {
                Node* next;
            public:
                Node(Node* next = nullptr);
                void set_next(Node* next);
                Node* get_next();
        };
    private:
        unsigned int count;
        Node* head;
        Node* tail;
    public:
        Queue();
        void push(Node* elem);          // push end
        void push_front(Node* elem);    // push front
        Node* pop();                    // pop front
        unsigned int size();
        void increment_size();
        bool is_empty();
        Node* get_head();
        Node* get_tail();
};

#endif