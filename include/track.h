#ifndef TRACK_H
#define TRACK_H

#include "track_node.h"

// The track initialization functions expect an array of this size.
#define TRACK_MAX 144
#define NUM_SENSORS 80

class Track {
    track_node track[TRACK_MAX];
    // TODO: probably need to store switches info
    int min_distance(unsigned int dist[], bool sptSet[]);
    void init_tracka();
    void fix_tracka();
    void init_trackb();
    void fix_trackb();
public:
    enum class RandomVersion{
        na,     // not a random player
        v1,     // run in circles
        v2,     // run with any nodes
    };

    short total_nodes;
    unsigned int random_nodes[NUM_SENSORS];     // stores the index in track[]
    bool is_track_a = true;
    void init();
    unsigned int get_track_node_idx(track_node* n) const;
    track_node* get_random_destination(Track::RandomVersion version);
    track_node* get_branch_from_num(int num);
    track_node* get_sensor_from_num(int num);
    track_node* get_sensor_from_str(const char* str);
    track_node* get_landmark_from_str(const char* str);
    bool set_branch(track_node* branch, NodeDir dir);       // returns true if executed
    bool set_branch(int num, NodeDir dir);
    unsigned int distance_to_next_sensor(track_node* last_sensor, track_node* next_sensor) const;
    unsigned int path_planning(track_node* src, track_node* dest, unsigned int dist[TRACK_MAX], int parent[TRACK_MAX], void* ptr = nullptr);
    track_node& operator[](int ind){ return track[ind]; }
};

#endif
