#ifndef TRACK_NODE_H
#define TRACK_NODE_H

typedef enum NodeType {
    NODE_NULL = 0,
    NODE_SENSOR,
    NODE_BRANCH,
    NODE_ENTER,
    NODE_EXIT,
    NODE_MERGE,
} NodeType;

enum NodeDir{
    DIR_AHEAD = 0,
    DIR_CURVED,
    DIR_STRAIGHT,
};

struct track_node;
struct track_edge;

struct track_edge {
    track_edge* reverse;
    track_node* src;
    track_node* dest;
    unsigned int dist;
    track_edge();
};

struct track_node {
    char const * name;
    NodeType type;
    int num;
    track_node* reverse;
    track_edge edge[DIR_STRAIGHT+1];
    NodeDir current_dir;    // direction of switch, not used if not a branch
    int reservedBy;         // the train number that reserved this track, 0 if none
    track_edge* next_edge(NodeDir dir = DIR_AHEAD);
    track_node* next_node(NodeDir dir = DIR_AHEAD);
    track_node* next_sensor(NodeDir dir = DIR_AHEAD);
    track_node* step_one_edge(unsigned int& distance, NodeDir dir = DIR_AHEAD);
    unsigned int distance_to_next_sensor(NodeDir dir = DIR_AHEAD);
    track_node();

    // these two functions will modify the field of reservedBy for both this and the reverse node 
    void setReservedBy(int train_number);
    void clearReserve();
    // reserved by other trains than the input
    bool reserved(){return reservedBy != 0;};
    // reserved by other trains than the input
    bool reservedAndNotBy(int train_number){return reservedBy && reservedBy != train_number;};
    // reserved by other trains than the input
    bool notReservedBy(int train_number){return reservedBy != train_number;};
    // not reserved or reserved by this train
    bool canReserved(int train_number){return reservedBy == 0 || reservedBy == train_number;};

};

class Location {
    track_node* landmark;
    unsigned int offset_mm;
public:
    Location();
    Location(track_node* landmark, int offset_mm);
    void set_location(track_node* landmark, int offset_mm);
    void set_location(Location &loc);
    track_node* get_landmark() const;
    unsigned int get_offset_mm() const;
    void caliberate();
    Location get_reverse_location();
    unsigned int distance_to_next_sensor();
    bool exists(){return nullptr != landmark;};
    void clear(){landmark = nullptr;};
    bool same_landmark(Location &loc){ return landmark == loc.landmark;}
};
#endif