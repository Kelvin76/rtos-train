// task_lib.h provides the functions to be called by tasks
//      include both kernel functions and user-level functions

#ifndef TASK_H
#define TASK_H

#include "td.h"

enum ESwiNumber
{
    ESN_Interrupt = 1,
    ESN_MyTid,
    ESN_MyParentTid,
    ESN_Schedule,
    ESN_Exit,
    ESN_Destroy,
    ESN_Create,
    ESN_Send,
    ESN_Receive,
    ESN_Reply,
    ESN_Await_Event,
    ESN_Read_Timer,
    ESN_Read_Ticks,
    ESN_Delay_task,
    ESN_Get_Idle_Time,
    ESN_Terminate,
};

enum ETaskPriority
{
    ETP_Initializer = 0,
    ETP_Highest,
    ETP_High,
    ETP_Medium,
    ETP_Low,
    ETP_Lowest,
    ETP_Idle,       // only for the idle task, adding a priority for it makes things easier
};

enum Event // remember up update NUM_EVENTS when you update this!!!
{
    EVT_TIMER_TICK = 0,
    EVT_UART2_TIMEOUT,
    EVT_UART2_TX,
    EVT_UART1_TX,
    EVT_UART1_CTS,
    EVT_UART1_RX,
};

#define NUM_PRIORITIES ETP_Idle + 1
#define NUM_EVENTS EVT_UART1_RX + 1
// name server
#define TASK_NAME_LENGTH 20
#define NAME_SERVER_BUFFER_SIZE 64
#define REGISTER_CHAR 'r'
#define WHO_IS_CHAR 'w'

// clock server
#define TICK_CHAR 'k'
#define TIME_CHAR 't'
#define DELAY_CHAR 'd'
#define DELAYUNTIL_CHAR 'u'
#define CLOCK_SERVER_NAME "ClockServer"

// UART server
enum UARTServerProtocol {
    USP_UART2_RX,
    USP_UART2_GET_C,
    USP_UART2_TX,
    USP_UART2_PUT_C,
    USP_UART2_PUT_STR,
    USP_UART1_TX,
    USP_UART1_CTS,
    USP_UART1_PUT_C,
    USP_UART1_PUT_STR,
    USP_UART1_GET_C,
    USP_UART1_RX,
    USP_TRAIN_FLUSH,        // flush the UART1 TX buf and reply to the caller
};

enum class CTSState {
    READY,
    SENT_NOT_LOWERED,
    SENT_LOWERED,
};

#define UART_SERVER_NAME "UARTServer"
#define UART1_RX_BUF_CAPACITY 50  // uart server buffer sizes
#define UART2_RX_BUF_CAPACITY 50
#define GETC_QUEUE_UART2_CAPACITY 50
#define UART1_TX_BUF_CAPACITY 100
#define UART2_TX_BUF_CAPACITY 600
#define PUTC_QUEUE_UART2_CAPACITY 50
#define PUTC_QUEUE_UART1_CAPACITY 50
#define GETC_QUEUE_UART1_CAPACITY 50

#define UART2_MSG_BUFFER_CAPACITY 60

// ------ kernel ------

// returns the TD of the next scheduled (user) task
TD *schedule();

// main kernel task
extern "C"{
    void k_main_loop(int value1, int value2, bool is_swi);
    void svc_handler();
    // switch registers using stacks sp1 and sp2
    void leave_kernel();
}

// push current task back to ready queue
void k_make_ready(TD* td);

// save return value to r0 on stack
void k_save_return_value(unsigned int val, TD* td = nullptr);

// initialize the task descriptor for a task and return the task id
int k_Create(int priority, void (*function)());

// return task id of the calling task
void k_MyTid();

// return task id of the parent for the calling task, return -1 if parent does not exist
void k_MyParentTid();

// -- send-receive-reply --
struct SendParams {
    int tid;
    const char *msg;
    int msglen;
    char *reply;
    int replylen;
};
void k_Send(int tid, const char *msg, int msglen, char *reply, int replylen);
extern "C" int _Send(SendParams* param);

struct ReceiveParams {
    int* tid;
    char *msg;
    int msglen;
};
void k_Receive(int *tid, char *msg, int msglen);
extern "C" int _Receive(ReceiveParams* param);

struct ReplyParams {
    int tid;
    const char *reply;
    int rplen;
};
void k_Reply(int tid, const char *reply, int rplen);
extern "C" int _Reply(ReplyParams* param);

// cache enabling
extern "C" void disableCache(); // TODO: might wanna delete this add just add some documentation in readme? like what we did with cache and how we enabled it
extern "C" void enableCache();  // TODO: also, should this be counted as kernel functions?

// blocks until the event identified by eventid occurs then returns with volatile data, if any
void k_AwaitEvent(int eventType);

unsigned int k_ReadTimer();

// ------ user level functions (interface for user tasks) ------

// Create a task and returns its task id
extern "C" int Create(int priority, void (*function)());

// trigger software interruption, i.e., execute "swi n"
int enter_kernel(ESwiNumber n = ESwiNumber::ESN_Interrupt);

// return task id of the calling task
int MyTid();

// return task id of the parent for the calling task, return -1 if parent does not exist
int MyParentTid();

// give up the processor
void Yield();

// quit current task
void Exit();

// TODO: delete current task (Exit() + give up mem slab + delete td)
void Destroy();

// read timer
unsigned int ReadTimer();

// read timer in microseconds (us)
unsigned int ReadTimerUS();

// returns a number in [low, high]
unsigned int rand(int low, int high);

// read the number of ticks, one tick is 10 ms
unsigned int ReadTicks();

// -- send-receive-reply --
/**
 * @brief We are using a send-receive-reply model. 
 * In this model, the sender will always be blocked on replies, if the receiver 
 * does not reply, then sender will always be blocked. - Both Sender first and Receiver first
 * Receiver blocks only if there was no sender before it receives. - Only Receiver first
 */

// send messgae to tid with a msg, receiver will reply in *reply
int Send(int tid, const char *msg, int msglen, char *reply, int replylen);

// receives message, sender id will be put in *tid, message will be put in *msg
int Receive(int *tid, char *msg, int msglen);

// replys to a target task with replies provided
int Reply(int tid, const char *reply, int rplen);

// blocks until the event identified by eventid occurs then returns with volatile data, if any
extern "C" int AwaitEvent(int eventType);

void uint2Chars(char* cs, unsigned int num);
unsigned int chars2Uint(char* cs);

// ---- servers ----

// -- name server --
struct NameTidMapping {
    int tid;
    char name[TASK_NAME_LENGTH];
};
void task_name_server();    // name server

// Register a task, name is currently max at 9 characters (TASK_NAME_LENGTH - 1)
/*
    truncate if name exceeds max length
    must include null character
    return -1 when nameserver tid is bad
    return 0 if success
*/
int RegisterAs(const char *name);

// returns the task id of a name
/*
    must include null character
    return -1 nameserver tid does not exist
    return -2 if doesn't exist
    should return tid normally 
*/
int WhoIs(const char *name);


// -- clock server --
struct DelayedTask {
    DelayedTask *next;
    bool used = false;
    unsigned int unblockAtTick;
    int tid;
};
void task_clock_server();    // name server

int pushQueueInOrder(Queue &q, DelayedTask *dt);
DelayedTask* getFreeNode4delayedTask(DelayedTask dt_buffer[]);

// returns the number of ticks since the clock was initialized
/**
Return Value
>-1	time in ticks since the clock server initialized.
-1	tid is not a valid clock server task.
*/
int Time(int tid);

// returns after the given number of ticks has elapsed.
/**
Return Value
>-1	success. The current time returned (as in Time())
-1	tid is not a valid clock server task.
-2	negative delay.
*/
int Delay(int tid,int ticks);

// returns when the time since clock server initialization is greater or equal than the given number of ticks
/**
Return Value
>-1	success. The current time returned (as in Time())
-1	tid is not a valid clock server task.
-2	negative delay.
*/
int DelayUntil(int tid, int ticks);


// -- UART server --
/* UART server
* 		- Handle requests from the wrapper functions
* 			- ds
* 				buffer for uart 1 receive
* 				buffer for uart 2 receive
* 				buffer for uart 1 send
* 				buffer for uart 2 send
* 				flag for uart 2 ready to tx
* 				flag for uart 2 ready to rx
* 			- Receive/handle Getc
* 				pop char from uart x receive
* 			- Receive/handle Putc
* 				push char to uart x send
* 			- Receive/handle notifiers
* 				- do read first
* 				- UART 2 user input ready - caused by timeout, but there is a queue
* 					- drain queue and insert into uart 2 receive buffer
* 					- until read fail
* 					- enable interrupt (should be disabled in kernel)
* 				
* 		- Communication / Serial IO
* 			- need to handle the communication to COM1 - train
* 			- need to handle the communication to COM2 - Redboot
*
* Notifiers
* 		- possibly a naive notifier like what we have for clock server
* 		- what interrupts?
* 			- first, consider uart 2
* 				- should we enable buffer?
* 					- yes, reduce interrupt load, burst write
* 					- cannot receive interrupt right away -> use timeout interrupt (~360us), user cannot type this fast
* 				- when to transmit?
* 					- transmit to PC, no need to consider receiver speed
* 					- is limited by transmit buffer
* 					- TX transmit fifo half empty interrupt
* 				- need to disable/enable interrupt in user task?
* 			- then uart 1
* 				- for input, disable fifo recommanded
*
*  For uart 0:
* 		- CTS status: 
* 			ready_to_send (can send a char now), (default)
* 			sent_not_lowered (after sending a char and confirming the interrupt),
* 			sent_lowered(CTS is off when in sent_not_lowered), 
* 			if a new CTS interrupt happens, goto ready_to_send
* 		- user calls putc
* 		- putc puts char in input buffer
* 		
* 		- tx interrupt
* 		- disable tx
* 		- tx notifier notifies tx up
* 		- tx_up = true
* 		
* 		- CTS interrupt
* 		- disable CTS
* 		- cts notifier
* 		- if high
* 			- CTS status = from sent_lowered to ready_to_send
* 		- if low (have to happen after write)
* 			- from sent_not_lowered to sent_lowered
* 
* 		- if tx_up and ready_to_send:
* 			write char (no fifo, no need for while loop)
* 			confirms rts
* 			ready_to_send -> sent_not_lowered
* 
* 			enable TX interrupt
* 			enable CTS
*/

void task_uart_server();    // UART server

void task_uart2_timeout_notifier();
void task_uart2_tx_notifier();
void task_uart1_tx_notifier();
void task_uart1_cts_notifier();
void task_uart1_rx_notifier();

/**
return -1 if tid is not a valid uart server task. 
*/
int Getc(int tid, int uart);

/**
Return Value
0	success.
-1	tid is not a valid uart server task. 
*/
int Putc(int tid, int uart, char ch);

// put a whole string altogether 
int Putstr(int tid, int uart, char* str);

// put a wholse string altogether with a defined size for the string (so it's not depending of '\0', i.e., we can actually send '\0' in this way)
int Putstr(int tid, int uart, char* str, int size);

// Currently not used (see this as a debug tool), avoid using this if possible
// put a whole string into separate "packets", each with a size of partition
int PutstrPartition(int tid, int uart, char* str, int partition);

#endif
