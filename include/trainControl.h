/**
    trainControl.h declares tasks and functions related to train controls, including the terminal user interface
*/

#ifndef TC_H
#define TC_H

#include "track_node.h"
#include "track.h"
#include "train.h"

/**
 * @brief Train control system will have one main task and severel notifiers
 * Main task (Entry point used when creating):
 *  1. task_train_control 
 *      - This function will start the train control system
 *      - All other tasksed will be initialized in this function/task
 *  Servers:
 *      2. task_delayed_command_server
 *          - used to send delayed commands
 *          - basically, the main task will send commands with an activation time to this server
 *          - this task checks whether the activation time is reached for a command, if so, it send command to uart com 1
 *          - TODO: could actually send the commands back to the main task and let the main task send to the uart com1, so we only have one task that is interacting with sending commands
 *          - however, this should not cause any problem, since all commands sent to COM1 are using Putstr (unless it's just one character), so no sequence of commands will be interrupted by other commands
 *  notifiers:
 *      3. task_delayed_command_notifier
 *          - send to `task_delayed_command_server` every 1 tick, so `task_delayed_command_server` will be awaken every 1 tick to check if any delayed_command should be sent
 *      4. task_wall_clock_notifier (could potentially be merged with the above notifier)
 *          - send to `task_train_control` every 10 ticks, so `task_delayed_command_server` will be awaken every 10 tick to print the wall clock
 *      5. task_input_notifier
 *          - get input from COM2 and send the input to `task_train_control` character by character
 *      6. task_sensor_notifier  
 *          - keep requesting for sensor data and send information about triggered data to the main task if there is any
 *          - will also send sensor response time to the main task if the macro `SENSOR_MEASUREMENT` is defined
 *              - the response time is measured every `SENSOR_INTERVAL` ticks (i.e., 1 second for now)
 */

// some command constants
#define TRAIN_SPEED_STOP 0
#define TRAIN_SPEED_REVERSE 15
#define TRAIN_SPEED_LIGHT_OFFSET 16
#define COMMAND_SOLENOID_OFF 32
#define COMMAND_TURNOUT_STRAIGHT 33
#define COMMAND_TURNOUT_CURVE 34
#define TRAIN_SYSTEM_START 96
#define TRAIN_SYSTEM_STOP 97

// track system constants
#define SENSOR_MEM_RESET 192
#define SENSOR_DATA_REQUEST 133
#define SENSOR_GET_BUF_SIZE 10      // train will send 10 bytes data for sensors

#define SENSOR_BUF_SIZE 10          // we can store 10 recent triggered sensors
#define SENSOR_INTERVAL 100         // measure the sensor response every 100 ticks - 1 s

#define NUM_TRAINS 6
#define NUM_SENSORS 80
#define NUM_BRANCH_PLUS_MERGE 44
#define NUM_TURNOUTS 22
#define COMMAND_SIZE 2              // a conventional size for commands

// customized waiting time
#define TRAIN_STOP_TICK_BASE 6                  // 60 ms
#define MULTIPLIER_FOR_TRAIN_STOP_TICK 21       // 21 times

#define TC_INPUT_BUF_SIZE 30                    // size of the input buffer
#define TC_DELAYED_COMMAND_BUF_SIZE 30          // max number of delayed commands stored

#define TC_REQUEST_BUF_SIZE 10

#define DELAY_COMMAND_2_CHAR 'd'
#define DELAY_COMMAND_1_CHAR 't'

#define SPECIAL_TRAIN_EXTRA_DELAY 20    // 200ms

#define TC_DT_DEFAULT_SPEED 12

// TODO: do not use this in tc2
#define TC_DT_SAFE_DISTANCE 3000        // 3m
#define RANDOM_PATH_MIN_LENGTH 1200     // 1.2m
#define MIN_SAFE_DISTANCE_TO_MOVE 700   // 0.7m

#define SENSOR_LOOP_CYCLE_PERIOD 4      // 0.4s

#define TRAIN_LENGTH 300        // mm

enum EExecutionResult{
    EER_Quit = 0,           // execution is quit
    EER_OK,           // execution is done
    EER_UnknownFormat,      // format is unknown
    EER_InvalidInstruction, // instruction is invalid
    EER_InvalidValues,      // values are not allowed
    EER_Empty,              // no instruction provided
    EER_DestinationSameAsSource,    // destination is the same as the source
    EER_TrainInProcess,     // the locomotive is in a sequence of commands
    EER_TrainNotStarted,
    EER_PathNotFound,       // no feasible path
    EER_TrainNotFound,      // Train does not exist
    EER_TrainDead,          // Train is at an dead end, has to reverse
    EER_TrainInDT,          // DT command is not finished
    EER_SensorReserved,     // sensor is reserved by other train
    EER_ReverseSensorReserved,
    EER_TrainAlreadyDelayedReversing,  // train is already in reversing process
};

enum ETrainRequests{
    ETR_ClockUpdate = 0,    // should update the clock
    ETR_SensorUpdate,
    ETR_InputReceived,
    ETR_SensorMeasurement1,
    ETR_SensorMeasurement2,
    ETR_WorkerRequestDelay,
    ETR_WorkerDelayDone
};

class TrainSystem{
private:
    struct SensorPrintData { // print sensor and train attributed
        char sensor;
        int train_num;
    };
    unsigned int curTicks;
    unsigned int switchCounter;     // everytime switchCounter = 10, turnoff the switches
    bool is_switch_on;
    Train trains[NUM_TRAINS];
    Track track;

    struct ReservedSensorData {
        const char *name;
        int train_num;
    };

    // current active train, works for TC1, need to change for TC2
    // set by dt
    // unsigned int active_train_num;

    // server tids
    short uart_tid;
    short clock_tid;
    short delayed_command_task_tid;
    // sub tasks
    short wall_clock_notifier_tid;
    short sensor_notifier_tid;
    short input_notifier_tid;
    int delay_work_arr[NUM_DELAY_WORKER];
    RBuf<int> delay_workers;

    EExecutionResult assert_train_state(Train*& tr, const int train_number, const bool check_activation_time = true);
    void turnOffSwitchNow();

public: 
    Train* get_train(int train_number);
    void delayedStopTrain(Train& tr, int stop_delay = -1, int total_dist = -1);

    // helper functions for switch a turnout
    bool setSwitch(char switchN, char direction, bool shouldUpdate);
    void turnOffSwitch();

    int setTrain(const int train_number, const int train_speed_level);
    int setTrain(Train &tr, int train_speed_level);
    EExecutionResult train_path_planning(Train* tr, Location& dest, bool reverse_start = false, unsigned int min_distance = 0);
    int stopTrain(Train &tr);

    void reverseTrain(const int train_number);
    void reverseTrain(Train &tr);
    void sendCommand(char* const commands, const int size);
    void sendDelayedCommandDouble(char* const commands, const unsigned int activationTime);
    void sendDelayedCommandSingle(char* const commands, const unsigned int activationTime);
    // set train property with a delay
    void delayed_set_speed_async(Train& tr, int speed, int delay_until);
    void delayed_reverse_async(Train& tr, int speed, int delay_until);

    // --- functions for the instructions ---
    // Use these function to switch a turnout / reverse the train / set speed if needed
    bool switchTurnout(int switchN, const char direction);
    EExecutionResult reverseTrainAndRun(const int train_number, bool check_activation_time = true, bool is_stop = false);
    EExecutionResult reverseTrainAndStop(const int train_number, bool check_activation_time = true);
    EExecutionResult trSetTrainSpeed(const int train_number, const int train_speed_level, bool check_activation_time = true);
    
    int set_train_path(Train& tr, track_node* dijkstra_start_node, Location& src, Location& dest, unsigned int dist[TRACK_MAX], int parent[TRACK_MAX], unsigned int total_dist);    

private:
    // parser and executor for instructions, called in start() to handle all inputs
    EExecutionResult executeCommands(const char* input, const int size);

    // initialize train speed and set switch directions
    void initTrainSystem();
    void flushCommands();        // block the caller until UART1_TX_BUF in uart_server is empty

    void resetMem();

    // printing
    /*
      col: 123456789012345678901234567890
    row 1: mm:ss:x   idle:dd%
    row 2: ---Switch Table---     --Recently triggered Sensors (higher is newer)--     --Sensor Prediction Time & Distance Diff--
            ...
    row 14:
    row 15: 
            ...
    row 20: Input>
    row 21: command execution result
    row 22: debug msg
    */
    void initUI();                      // print the frame of tables (switch & sensor title)
    void refreshCli();                  // clean and reset line 20 (the input line)
    void printPrompt(char* input_str);  // print on line 21 for feedback on the input
    void printSensorTriggerPrediction(int time_diff, int distance_diff); // print on line 3 for sensor prediction results
    void printTrainTable();             // train table on line 5, row 78
    void updateTrainStatus(int row, Train* tr, RBuf<ReservedSensorData> &buf); // print train information on line 4-9
    void clearPrompt();                 // clean the prompt on line 21
    void updateSwitchTable(unsigned short switchN, char direction);
    void updateSensorTable();           // print triggered sensor from new to late
public:
    TrainSystem();

    void start();   // main entry - called in task_train_control
};

// print debug info on line 22 (add '\n' in "input_str" to switch to the next line like line 23 or 24)
void debugprintf(char* input_str, ...); 

// The entry point: train control is a user task that includes a user interface
void task_train_control();  

#endif
