#ifndef SLAB_H
#define SLAB_H

#include "q.h"

class SlabAllocator {
        Queue free_list;
    public:
        SlabAllocator();
        void* get_slab();
};

#endif