#ifndef MEMCONF_H
#define MEMCONF_H


#define MEMORY_END          0x2000000
#define K_STACK_END          0x1F00000 // give kernel ~1mb memory
#define MEMORY_START        0x0100000 // coded in linker script

////// slab

/*
low address

0 next (probably need multiple of these)


 stack
 TD
hi address
*/
#define SLAB_AMOUNT         40 // 0x989680 bytes, should have 0x1576980 left
#define SLAB_SIZE           250000    // give task 250 kb memory => maybe less than 100 tasks?
#define slab_addr(i) (K_STACK_END - (i + 1) * SLAB_SIZE)

#define NEXT_POINTER_OFFSET 0


#endif