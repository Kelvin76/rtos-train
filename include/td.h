#ifndef TD_H
#define TD_H

#include "config.h"
#include "q.h"

enum EState
{
	ES_Unknown,
	ES_Active,
	ES_Ready,
	ES_Zombie,
	ES_SendBlocked,
	ES_ReceiveBlocked,
	ES_ReplyBlocked,
	ES_EventBlocked
};

class TD
{
public:
	// per class, a TD will only be in one of (ready, blocked) state at any given time
	// so we only need one "next" pointer
	// this is for when we add td to the ready queue, we need to cast
	// this to a queue node
	TD *next;
	EState state;
	int tid;
	TD *parent;
	int priority;
	u32 *sp;
	Queue senders_blocked_on_Receive;
	TD(int tid, TD *parent, int priority, u32 *sp);
};

/*
 * @brief: Exception Stack:

		  +-------------------------+ High Address
		  |          ...            | <--- td mem end
		  |-------------------------|
		  |          PSR            |
		  |-------------------------|
		  |  PC (R15) = func addr   | <--- sp position before init
		  |-------------------------|
		  |         LR (R14) = 0    |
		  |-------------------------|
		  |         SP (R13) = 0    |
		  |-------------------------|
		  |         R12 = 0         |
		  |-------------------------|
		  |         R11 = 0         |
		  |-------------------------|
		  |         R10 = 0         |
		  |-------------------------|
		  |         R9 = 0          |
		  |-------------------------|
		  |           ...           |
		  |-------------------------|
		  |         R1 = 0          |
		  |-------------------------|
		  |         R0 = 0          | <--- sp position
		  +-------------------------+ Low Address
*/

#endif